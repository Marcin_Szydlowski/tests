﻿using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Fakes;
using Workshop05.Fakes;

namespace Workshop05.Tests
{
	[TestClass]
	public class PersonTests
	{
		[TestMethod]
		public void Case1_ShouldPublicBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.AllInstances.Case1 = delegate
				{
					Debug.WriteLine("Fake Case1");
				};
				var person = new Person();

				// Act
				person.Case1();

				// Assert

			}
		}

		[TestMethod]
		public void Case2_ShouldPrivateBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.AllInstances.Case2 = delegate
				{
					Debug.WriteLine("Fake Case2");
				};
				var person = new Person();
				PrivateObject privatePerson = new PrivateObject(person);

				// Act
				privatePerson.Invoke("Case2");

				// Assert

			}
		}

		[TestMethod]
		public void Case3_ShouldPrivateStaticBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.Case3 = delegate
				{
					Debug.WriteLine("Fake Case3");
				};
				PrivateType privatePersonType = new PrivateType(typeof(Person));

				// Act
				privatePersonType.InvokeStatic("Case3");

				// Assert

			}
		}

		[TestMethod]
		public void Case1_ShouldPublicWithReturnBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.AllInstances.Case1WithReturn = value =>
				{
					Debug.WriteLine("Fake Case1WithReturn");
					return 10;
				};
				var person = new Person();

				// Act
				int result = person.Case1WithReturn();

				// Assert
				Assert.AreEqual(10, result);
			}
		}

		[TestMethod]
		public void Case2_ShouldPrivateWithReturnBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.AllInstances.Case2WithReturn = value =>
				{
					Debug.WriteLine("Fake Case2WithReturn");
					return 20;
				};
				var person = new Person();
				PrivateObject privatePerson = new PrivateObject(person);

				// Act
				int result = (int)privatePerson.Invoke("Case2WithReturn");

				// Assert
				Assert.AreEqual(20, result);
			}
		}

		[TestMethod]
		public void Case3_ShouldPrivateStaticWithReturnBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.Case3WithReturn = () =>
				{
					Debug.WriteLine("Fake Case3WithReturn");
					return 30;
				};
				PrivateType privatePersonType = new PrivateType(typeof(Person));

				// Act
				int result = (int)privatePersonType.InvokeStatic("Case3WithReturn");

				// Assert
				Assert.AreEqual(30, result);
			}
		}

		[TestMethod]
		public void Case1_ShouldPublicWithReturnAndParameterBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.AllInstances.Case1WithReturnAndParameterInt32 = (value, param) =>
				{
					Debug.WriteLine("Fake Case1WithReturnAndParameter");
					return 100;
				};
				var person = new Person();

				// Act
				int result = person.Case1WithReturnAndParameter(5);

				// Assert
				Assert.AreEqual(100, result);
			}
		}

		[TestMethod]
		public void Case2_ShouldPrivateWithReturnAndParameterBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.AllInstances.Case2WithReturnAndParameterInt32 = (value, param) =>
				{
					Debug.WriteLine("Fake Case2WithReturnAndParameter");
					return 200;
				};
				var person = new Person();
				PrivateObject privatePerson = new PrivateObject(person);

				// Act
				int result = (int)privatePerson.Invoke("Case2WithReturnAndParameter", 5);

				// Assert
				Assert.AreEqual(200, result);
			}
		}

		[TestMethod]
		public void Case3_ShouldPrivateStaticWithReturnAndParameterBeMocked_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimPerson.Case3WithReturnAndParameterInt32 = value =>
				{
					Debug.WriteLine("Fake Case3WithReturnAndParameter");
					return 300;
				};
				PrivateType privatePersonType = new PrivateType(typeof(Person));

				// Act
				int result = (int)privatePersonType.InvokeStatic("Case3WithReturnAndParameter", 5);

				// Assert
				Assert.AreEqual(300, result);
			}
		}

		[TestMethod]
		public void GetRandom_ShouldAlwaysReturn3_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange

				// 1st approach - change logic
				//ShimPerson.AllInstances.GetRandom = value =>
				//	{
				//		StubRandom stubRandom = new StubRandom();
				//		stubRandom.Next01 = () =>
				//		{
				//			return 3;
				//		};

				//		return stubRandom.Next();
				//	};

				// 2nd approach - without change logic
				ShimRandom.AllInstances.Next = value => 3;

				var person = new Person();

				// Act
				int result = person.GetRandom();

				// Assert
				Assert.AreEqual(3, result);
			}
		}

		[TestMethod]
		public void GetDate_ShouldAlwaysReturn201509281200_Success()
		{
			using (ShimsContext.Create())
			{
				// Arrange
				ShimDateTime.NowGet = () => new DateTime(2015, 9, 28, 12, 0, 0);
				var person = new Person();

				// Act
				DateTime now = person.GetNow();
				bool areEqual = now.Equals(new DateTime(2015, 9, 28, 12, 0, 0));

				// Assert
				Assert.IsTrue(areEqual);
			}
		}
	}
}