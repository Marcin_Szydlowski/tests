﻿using FakeItEasy;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Workshop05.Fakes;
using Assert = NUnit.Framework.Assert;

namespace Workshop05.Tests
{
	[TestClass]
	public class PersonTestsFakeItEasy
	{
		[TestMethod]
		public void Ctor_ShouldNotBeFaked_Success()
		{
			// Arrange

			// Act
			// Not possible to mock parameterless constructor -> look at documentation

			using (ShimsContext.Create())
			{
				ShimPerson.Constructor = param => { };
				var person = new Person();
			}

			// Assert
			Assert.Pass();
		}

		[Test]
		public void Case4_ShouldAlwaysReturn1_Success()
		{
			// Arrange
			var fakePerson = A.Fake<Person>();
			A.CallTo(() => fakePerson.Case4()).Returns(1);

			// Act
			int result = fakePerson.Case4();

			// Assert
			Assert.AreEqual(1, result);
		}

		[Test]
		public void Case5_ShouldAlwaysReturn2_Success()
		{
			// Arrange
			var fakePerson = A.Fake<Person>();
			A.CallTo(() => fakePerson.Case5()).Returns(2);

			// Act
			int result = fakePerson.Case5();

			// Assert
			Assert.AreEqual(2, result);
		}
	}
}