﻿using Moq;
using NUnit.Framework;

namespace Workshop05.Tests
{
	[TestFixture]
	public class PersonTestsMoQ
	{
		[Test]
		public void Ctor_ShouldNotBeFaked_Success()
		{
			// Arrange

			// Act
			var fakePersonMoq = new Mock<Person>();
			fakePersonMoq.CallBase = false;
			//var result = fakePersonMoq.Object.Case4();
			// Not possible to mock parameterless constructor -> http://stackoverflow.com/questions/19931844/how-to-moq-and-override-constructor "There is no possibility to override or mock constructor using Moq framework."

			// Assert
			Assert.Fail();
		}

		[Test]
		public void Case4_ShouldAlwaysReturn1_Success()
		{
			// Arrange
			var fakePersonMoq = new Mock<Person>();
			fakePersonMoq.Setup(fp => fp.Case4()).Returns(1);

			// Act
			int result = fakePersonMoq.Object.Case4();

			// Assert
			Assert.AreEqual(1, result);
		}

		[Test]
		public void Case5_ShouldAlwaysReturn2_Success()
		{
			// Arrange
			var fakePersonMoq = new Mock<Person>();
			fakePersonMoq.Setup(fp => fp.Case5()).Returns(2);

			// Act
			int result = fakePersonMoq.Object.Case5();

			// Assert
			Assert.AreEqual(2, result);
		}
	}
}