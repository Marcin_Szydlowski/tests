﻿using NSubstitute;
using NUnit.Framework;

namespace Workshop05.Tests
{
	[TestFixture]
	public class PersonTestsNSubstitute
	{
		[Test]
		public void Ctor_ShouldNotBeFaked_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		public void Case4_ShouldAlwaysReturn1_Success()
		{
			// Arrange
			var fakePersonSubstitute = Substitute.For<Person>();
			fakePersonSubstitute.Case4().Returns(1);

			// Act
			int result = fakePersonSubstitute.Case4();

			// Assert
			Assert.AreEqual(1, result);
		}

		[Test]
		public void Case5_ShouldAlwaysReturn2_Success()
		{
			// Arrange
			var fakePersonSubstitute = Substitute.For<Person>();
			fakePersonSubstitute.Case5().Returns(2);

			// Act
			int result = fakePersonSubstitute.Case5();

			// Assert
			Assert.AreEqual(2, result);
		}
	}
}