﻿namespace Knowledge.Repository.Entities
{
	public class BookEntity : BaseEntity
	{
		public string Title { get; set; }
		public string Author { get; set; }
		public ushort Year { get; set; }

		public virtual LibraryEntity Library { get; set; }
		public virtual UserEntity Borrower { get; set; }
	}
}