﻿using System.Collections.Generic;

namespace Knowledge.Repository.Entities
{
	public class LibraryEntity : BaseEntity
	{
		public string Name { get; set; }

		public virtual ICollection<BookEntity> Books { get; set; }
		public virtual ICollection<UserEntity> Users { get; set; }
	}
}