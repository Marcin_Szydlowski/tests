﻿using System.Collections.Generic;

namespace Knowledge.Repository.Entities
{
	public class UserEntity : BaseEntity
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }

		public virtual LibraryEntity Library { get; set; }            // Assumption: user borrows books from only one library - no middle table
		public virtual ICollection<BookEntity> Books { get; set; }
	}
}