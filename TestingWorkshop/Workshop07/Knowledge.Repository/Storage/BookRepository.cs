﻿using Knowledge.Repository.Entities;
using MC.CommonUtils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Knowledge.Repository.Storage
{
	public static class BookRepository
	{
		public static BookEntity GetBook(int id)
		{
			id.EnsurePositive(nameof(id));

			return Database.Books.Single(b => b.Id == id);
		}

		public static IEnumerable<BookEntity> GetBooks()
		{
			return Database.Books;
		}

		public static void AddBook(BookEntity book)
		{
			book.EnsureNotNull(nameof(book));

			Database.Books.Add(book);
		}

		public static void DeleteBook(int id)
		{
			throw new NotImplementedException();
		}

		public static void UpdateBook(BookEntity book)
		{
			... you are here right now
			throw new NotImplementedException();
		}
	}
}