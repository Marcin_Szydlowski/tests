﻿using Knowledge.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Knowledge.Repository.Storage
{
	public static class LibraryRepository
	{
		public static LibraryEntity GetLibrary(int id)
		{
			return Database.Libraries.Single(lib => lib.Id == id);
		}

		public static IEnumerable<LibraryEntity> GetLibraries()
		{
			throw new NotImplementedException();
		}

		public static void AddLibrary(LibraryEntity library)
		{
			throw new NotImplementedException();
		}

		public static void DeleteLibrary(int id)
		{
			throw new NotImplementedException();
		}

		public static void UpdateLibrary(LibraryEntity library)
		{
			throw new NotImplementedException();
		}
	}
}