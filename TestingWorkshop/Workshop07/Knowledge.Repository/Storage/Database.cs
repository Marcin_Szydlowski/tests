﻿using Knowledge.Repository.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Knowledge.Repository.Storage
{
	internal static class Database
	{
		internal static List<LibraryEntity> Libraries;
		internal static List<BookEntity> Books;
		internal static List<UserEntity> Users;

		static Database()
		{
			PrepareData();
		}

		private static void PrepareData()
		{
			PrepareLibraries();
			PrepareBooks();
			PrepareUsers();

			MakeRelations();
		}

		private static void PrepareLibraries()
		{
			Libraries = new List<LibraryEntity>
			{
				new LibraryEntity {
					Id=1,
					Name = "Library of Sulęcin"
				}
			};
		}

		private static void PrepareBooks()
		{
			Books = new List<BookEntity>
			{
				new BookEntity
				{
					Id=1,
					Title="One Day Without Internet",
					Author="Nigel Donnahy",
					Year = 2013
				},
				new BookEntity
				{
					Id=2,
					Title="Next Step To Heaven",
					Author="Deborah Sheldon",
					Year=2012
				}
			};
		}

		private static void PrepareUsers()
		{
			Users = new List<UserEntity>
			{
				new UserEntity
				{
					Id=1,
					FirstName="Franz",
					LastName="Kropka"
				}
			};
		}

		private static void MakeRelations()
		{
			var library = Libraries.First();

			library.Books = Books;
			library.Users = Users;

			var book = Books.First();
			var user = Users.First();

			user.Library = library;
			user.Books = new List<BookEntity> { book };

			Books.ForEach(b =>
			{
				b.Library = library;
			});

			book.Borrower = user;
		}
	}
}