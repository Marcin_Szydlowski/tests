﻿//using Knowledge.Building;
//using Knowledge.Human;
//using Knowledge.Paper;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Linq;

//namespace Knowledge.Tests.Functional
//{
//	[TestClass]
//	public class UserFunctionalTests
//	{
//		[TestMethod]
//		[TestCategory("User tests")]
//		public void User_ShouldOrderBook_Success()
//		{
//			// Arrange
//			IUser user = new Human.User("Gill", "Bates");
//			ILibrary library = new Library();
//			IBook book = new Book("Code of Destruction", "M€", 2015);
//			user.OrderBook(library, book);

//			// Act
//			int booksCount = user.BooksCount;

//			// Assert
//			Assert.AreEqual(1, booksCount);
//		}

//		[TestMethod]
//		public void User_ShouldReturnBook_Success()
//		{
//			// Arrange
//			IUser user = new Human.User("Helga", "Wurst");
//			ILibrary library = new Library();
//			IBook book = library.FindBookByTitle("Jack Refactor - True Story").FirstOrDefault();
//			user.OrderBook(library, book);

//			// Act
//			user.ReturnBook(library, book);
//			bool bookIsOrdered = book.IsBorrowed;
//			IUser purchaser = book.Borrower;
//			int availableBooksCount = library.GetAvailableBooks().Count();

//			// Assert
//			Assert.IsFalse(bookIsOrdered, "Book should not be ordered");
//			Assert.IsNull(purchaser, "Purchaser should be null");
//			Assert.AreEqual(5, availableBooksCount, "All books should be available");
//		}
//	}
//}