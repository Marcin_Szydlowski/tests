﻿//using Knowledge.Building;
//using Knowledge.Human;
//using Knowledge.Paper;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Linq;

//namespace Knowledge.Tests.Functional
//{
//	[TestClass]
//	public class LibraryFunctionalTests
//	{
//		[TestMethod]
//		public void Library_ShouldAssignUserToBook_Success()
//		{
//			// Arrange
//			ILibrary library = new Library();
//			IUser user = new Human.User("Kurd", "Coalman");
//			IBook book = library.FindBookByTitle("Dev Ops - Fear Factory").FirstOrDefault();     // TODO: MSZ: add test when no results

//			// Act
//			library.Order(user, book);
//			bool bookIsOrdered = book.IsBorrowed;
//			int availableBooksCount = library.GetAvailableBooks().Count;

//			// Assert
//			Assert.IsTrue(bookIsOrdered, "Book should be ordered");
//			Assert.AreEqual(4, availableBooksCount, "Available books is the same as before oreder");
//		}
//	}
//}