﻿//using Knowledge.Building;
//using Knowledge.Human;
//using Knowledge.Paper;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Linq;

//namespace Knowledge.Tests.Functional
//{
//	[TestClass]
//	public class BookFunctionalTests
//	{
//		[TestMethod]
//		public void Book_ShouldHavePurchaser_Success()
//		{
//			// Arrange
//			ILibrary library = new Library();
//			IUser user = new Human.User("Slumbody", "Gotkilled");
//			IBook book = library.FindBookByTitle("Dev Ops - Fear Factory").FirstOrDefault();    // TODO: MSZ: add test when no results
//			library.Order(user, book);

//			// Act
//			IUser purchaser = book.Borrower;

//			// Assert
//			Assert.AreEqual(user, purchaser);
//		}
//	}
//}