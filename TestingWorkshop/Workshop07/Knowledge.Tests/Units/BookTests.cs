﻿using Knowledge.Building;
using Knowledge.Human;
using Knowledge.Paper;
using NSubstitute;
using NUnit.Framework;
using System;

namespace Knowledge.Tests.Units
{
	[TestFixture]
	public class BookUnitsTests
	{
		private static readonly object[] InvalidValues =
		{
			new object[] { null, "M€", Substitute.For<ILibraryModel>() },
			new object[] { "", "M€", Substitute.For<ILibraryModel>() },
			new object[] { "No Bugs, No Cry", null, Substitute.For<ILibraryModel>() },
			new object[] { "No Bugs, No Cry", "", Substitute.For<ILibraryModel>() },
			new object[] { "No Bugs, No Cry", "M€", null }
		};

		private static readonly object[] InvalidParameters =
		{
			new object[] { null, "M€" },
			new object[] { "", "M€" },
			new object[] { "No Bugs, No Cry", null },
			new object[] { "No Bugs, No Cry", "" }
		};

		private static readonly object[] UnmatchedBooksValues =
		{
			new object[] { "Wizards of Code", "Jay Jones", (ushort)2013 },
			new object[] { "ABC of Coding", "Garry Wisdom", (ushort)2013 },
			new object[] { "ABC of Coding", "Jay Jones", (ushort)1999 }
		};

		private static readonly object[] OtherTypeObjects =
		{
			new object[] { null },
			new object[] { new object() }
		};

		[Test]
		[Category("Book unit success")]
		public void Book_ShouldInitialzeWithDefaultValues_Success()
		{
			// Arrange
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = new BookModel
			{
				Title = "No Bugs, No Cry",
				Author = "M€",
				Year = 2015,
				Library = library
			};

			// Act
			string title = book.Title;
			string author = book.Author;
			ushort year = book.Year;
			bool isBorrowed = book.IsBorrowed;
			IUserModel borrower = book.Borrower;
			ILibraryModel libraryOfBook = book.Library;

			// Assert
			Assert.AreEqual("No Bugs, No Cry", title, "Titles mismatched");
			Assert.AreEqual("M€", author, "Authors mismatched");
			Assert.AreEqual(2015, year, "Years mismatched");
			Assert.IsFalse(isBorrowed, "Book should not be borrowed");
			Assert.IsNull(borrower, "Book should not have a borrower");
			Assert.IsNotNull(libraryOfBook, "Book should be owned by the library");
		}

		[Test]
		[Category("Book unit fail")]
		[TestCaseSource("InvalidValues")]
		public void Book_ShouldNotBeInitializedWithInvalidValues_Fail(string title, string author, ILibraryModel library)
		{
			// Arrange

			// Act
			try
			{
				//IBookModel book = new BookModel(title, author, 2015, library);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Book unit success")]
		public void Book_ShouldBeEqual_Success()
		{
			// Arrange
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book1 = null;//new BookModel("ABC of Coding", "Jay Jones", 2013, library);
			IBookModel book2 = null;//ew BookModel("ABC of Coding", "Jay Jones", 2013, library);

			// Act
			bool result = book1.Equals(book2);

			// Assert
			Assert.True(result);
		}

		[Test]
		[Category("Book unit success")]
		[TestCaseSource("UnmatchedBooksValues")]
		public void Book_ShouldNotBeEqual_Success(string title, string author, ushort year)
		{
			// Arrange
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = null;// new BookModel("Someday, somewhere", "M€", 2015, library);
			IBookModel otherBook = null;// new BookModel(title, author, year, library);

			// Act
			bool result = book.Equals(otherBook);

			// Assert
			Assert.False(result);
		}

		[Test]
		[Category("Book unit success")]
		[TestCaseSource("OtherTypeObjects")]
		public void Book_ShouldNotBeEqualWithOtherTypes_Success(object obj)
		{
			// Arrange
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = null;// new BookModel("Someday, somewhere", "M€", 2015, library);

			// Act
			bool result = book.Equals(obj);

			// Assert
			Assert.False(result);
		}

		[Test]
		[Category("Book unit success")]
		public void Book_ShouldReturnHashCode_Success()
		{
			// Arrange
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = null;//new BookModel("Someday, somewhere", "M€", 2015, library);

			// Act
			int hashCode = book.GetHashCode();
			bool result = hashCode == ("Someday, somewhere".GetHashCode() ^ "M€".GetHashCode() ^ ((ushort)2015).GetHashCode() ^ library.GetHashCode());

			// Assert
			Assert.True(result);
		}

		[Test]
		[Category("Book unit success")]
		public void Book_ShouldUpdateData_Success()
		{
			// Arrange
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = null;//new BookModel("Someday, somewhere", "M€", 2015, library);

			// Act
			//book.UpdateData("Sometime, someday", "M€Tool", 2014);
			string newTitle = book.Title;
			string newAuthor = book.Author;
			ushort newYear = book.Year;

			// Assert
			Assert.AreEqual("Sometime, someday", newTitle, "Book's new title is different than expected");
			Assert.AreEqual("M€Tool", newAuthor, "Book's new author is different than expected");
			Assert.AreEqual(2014, newYear, "Book's new year is different than expected");
		}

		[Test]
		[Category("Book unit success")]
		[TestCaseSource("InvalidParameters")]
		public void Book_ShouldNotUpdateDataInvalidParameters_Fail(string title, string author)
		{
			// Arrange
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = null;// new BookModel("Call of Method", "Dexter Programmer", 2015, library);

			// Act
			try
			{
				//	book.UpdateData(title, author, 2014);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}
	}
}