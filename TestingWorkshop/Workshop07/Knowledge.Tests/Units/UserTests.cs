﻿using Knowledge.Building;
using Knowledge.Human;
using Knowledge.Paper;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Knowledge.Tests.Units
{
	[TestFixture]
	public class UserUnitsTests
	{
		private static readonly object[] InvalidValues =
		{
			new object[] { null, "Kundel" },
			new object[] { "", "Kundel" },
			new object[] { "Harold", null },
			new object[] { "Harold", "" }
		};

		private static readonly object[] UnmatchedUsersValues =
		{
			new object[] { "Brigitte", "Mefisto" },
			new object[] { "Jason", "Devil" }
		};

		private static readonly object[] OtherTypeObjects =
		{
			new object[] { null },
			new object[] { new object() }
		};

		[Test]
		[Category("User unit success")]
		public void User_ShouldInitializeWithDefaultValues_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Marcel", "Weaver");

			// Act
			string firstName = user.FirstName;
			string lastName = user.LastName;
			uint booksCount = user.BooksCount;
			int borrowedBooksCount = user.GetBorrowedBooks().Count;
			int librariesCount = user.GetLibraries().Count;

			// Assert
			Assert.AreEqual("Marcel", firstName, "First names aren't matched");
			Assert.AreEqual("Weaver", lastName, "Last names aren't matched");
			Assert.AreEqual(0, booksCount, "User should not have ordered books");
			Assert.AreEqual(0, borrowedBooksCount, "Borrowed books' list is not empty");
			Assert.AreEqual(0, librariesCount, "Libraries' list is not empty");
		}

		[Test]
		[Category("User unit fail")]
		[TestCaseSource("InvalidValues")]
		public void User_ShouldNotBeInitializedWithInvalidValues_Fail(string firstName, string lastName)
		{
			// Arrange

			// Act
			try
			{
				IUserModel user = new UserModel(firstName, lastName);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldBorrowBook_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Anke", "Kreuz");
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = Substitute.For<IBookModel>();
			book.Title.Returns("Dev Ops: Full Metal Cover");
			book.Author.Returns("M€");
			book.Year.Returns((ushort)2013);
			book.Library.Returns(library);
			library.GetAvailableBooks().Returns(new List<IBookModel> { book });

			// Act
			user.BorrowBook(book);
			uint borrowedBooksCount = user.BooksCount;

			// Assert
			Assert.AreEqual(1, borrowedBooksCount, "User should have only one book");
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldNotBorrowBookBookIsNull_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Maria", "Cruz");

			// Act
			try
			{
				user.BorrowBook(null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldNotBorrowBookBookIsBorrowed_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Maria", "Cruz");
			IBookModel book = Substitute.For<IBookModel>();
			book.Title.Returns("One Day of Coding");
			book.Author.Returns("Brian Coacher");
			book.Year.Returns((ushort)2010);
			book.IsBorrowed.Returns(true);

			// Act
			try
			{
				user.BorrowBook(book);
			}
			catch (InvalidOperationException ex)
			{
				Assert.AreEqual("This book is already borrowed", ex.Message);
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldReturnBook_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Hanke", "Kreuz");
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = Substitute.For<IBookModel>();
			book.Title.Returns("Dev Ops: Full Metal Cover");
			book.Author.Returns("M€");
			book.Year.Returns((ushort)2013);
			book.Library.Returns(library);
			library.GetAvailableBooks().Returns(new List<IBookModel> { book });
			user.BorrowBook(book);

			// Act
			user.ReturnBook(book);
			uint borrowedBooksCount = user.BooksCount;

			// Assert
			Assert.AreEqual(0, borrowedBooksCount, "User should not have any books");
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldNotReturnBookBookIsNull_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Brenda", "Salmon");

			// Act
			try
			{
				user.ReturnBook(null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldNotReturnBookHasNotHaveIt_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Brandon", "Octopus");
			ILibraryModel library = Substitute.For<ILibraryModel>();
			IBookModel book = Substitute.For<IBookModel>();
			book.Title.Returns("One Day of Coding");
			book.Author.Returns("Brian Coacher");
			book.Year.Returns((ushort)2010);
			book.Library.Returns(library);

			// Act
			try
			{
				user.ReturnBook(book);
			}
			catch (InvalidOperationException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldBeEqual_Success()
		{
			// Arrange
			IUserModel user1 = new UserModel("Brandon", "Orca");
			IUserModel user2 = new UserModel("Brandon", "Orca");

			// Act
			bool result = user1.Equals(user2);

			// Assert
			Assert.IsTrue(result);
		}

		[Test]
		[Category("User unit success")]
		[TestCaseSource("UnmatchedUsersValues")]
		public void User_ShouldNotBeEqual_Success(string firstName, string lastName)
		{
			// Arrange
			IUserModel user = new UserModel("Jason", "Mefisto");
			IUserModel otherUser = new UserModel(firstName, lastName);

			// Act
			bool result = user.Equals(otherUser);

			// Assert
			Assert.False(result);
		}

		[Test]
		[Category("User unit success")]
		[TestCaseSource("OtherTypeObjects")]
		public void User_ShouldNotBeEqualWithOtherTypes_Success(object obj)
		{
			// Arrange
			IUserModel user = new UserModel("Barbara", "Rabarbar");

			// Act
			bool result = user.Equals(obj);

			// Assert
			Assert.False(result);
		}

		[Test]
		[Category("User unit success")]
		public void User_ShouldReturnHashCode_Success()
		{
			// Arrange
			IUserModel user = new UserModel("Kamila", "Klemensowa");

			// Act
			int hashCode = user.GetHashCode();
			bool result = hashCode == ("Kamila".GetHashCode() ^ "Klemensowa".GetHashCode());

			// Assert
			Assert.True(result);
		}
	}
}