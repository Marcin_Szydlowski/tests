﻿using Knowledge.Building;
using Knowledge.Human;
using Knowledge.Paper;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Knowledge.Tests.Units
{
	[TestFixture]
	public class LibraryUnitsTests
	{
		private static readonly object[] OtherTypeObjects =
		{
			new object[] { null },
			new object[] { new object() }
		};

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldBeInitializedByDefaultValues_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Library in Zielona Góra");

			// Act
			string name = library.Name;
			int availableBooksCount = library.GetAvailableBooks().Count;
			int usersCount = library.GetUsers().Count;

			// Assert
			Assert.AreEqual("Library in Zielona Góra", name, "Library has different name than expected");
			Assert.AreEqual(5, availableBooksCount, "Library should have 5 books now");
			Assert.AreEqual(0, usersCount, "Library should not have any users yet");
		}

		[Test]
		[Category("Library unit fail")]
		[TestCase(null)]
		[TestCase("")]
		public void Library_ShouldNotBeInitializedByInvalidValues_Fail(string name)
		{
			// Arrange

			// Act
			try
			{
				ILibraryModel library = new LibraryModel(name);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldLendBook_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Kamil Norwid's Library in Zielona Góra");
			IBookModel firstBook = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			user.FirstName.Returns("Michalina");

			// Act
			library.LendBook(user, firstBook);
			int availableBooksCount = library.GetAvailableBooks().Count;
			int usersCount = library.GetUsers().Count;
			bool bookIsLent = firstBook.IsBorrowed;
			string userFirstName = firstBook.Borrower.FirstName;

			// Assert
			Assert.AreEqual(4, availableBooksCount, "Library should have only 4 available books now");
			Assert.AreEqual(1, usersCount, "Library should have 1 user now");
			Assert.True(bookIsLent, "Book should be lent now");
			Assert.AreEqual("Michalina", userFirstName, "User name is different than expected");
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotLendBookParametersAreNull_Fail()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Kamil Norwid's Library in Warsaw");
			IBookModel firstBook = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			user.FirstName.Returns("Helga");

			// Act
			try
			{
				library.LendBook(null, firstBook);
			}
			catch (ArgumentNullException)
			{

			}
			catch
			{
				Assert.Fail();
			}

			try
			{
				library.LendBook(user, null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotLendBookBookAlreadyBorrowed_Fail()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Kamil Norwid's Library in Cracov");
			IBookModel firstBook = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			user.FirstName.Returns("Regina");
			firstBook.Borrower = user;

			// Act
			try
			{
				library.LendBook(user, firstBook);
			}
			catch (InvalidOperationException ex)
			{
				Assert.AreEqual("This book is already borrowed", ex.Message);
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldTakeBookBack_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Kamil Norwid's Library in Bielsko Biała");
			IBookModel firstBook = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			user.FirstName.Returns("Grażyna");
			library.LendBook(user, firstBook);

			// Act
			library.TakeBookBack(firstBook);
			int availableBooksCount = library.GetAvailableBooks().Count;
			int usersCount = library.GetUsers().Count;
			bool bookIsLent = firstBook.IsBorrowed;

			// Assert
			Assert.AreEqual(5, availableBooksCount, "Library should have all books available now");
			Assert.AreEqual(0, usersCount, "Library should not have any users now");
			Assert.False(bookIsLent, "Book should not be lent now");
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotTakeBookBackBookIsNull_Fail()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Kamil Norwid's Library in Gdańsk");
			IBookModel firstBook = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			user.FirstName.Returns("Władysława");
			library.LendBook(user, firstBook);

			// Act
			try
			{
				library.TakeBookBack(null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotTakeBookBackBookIsNotBorrowed_Fail()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Kamil Norwid's Library in Gorzów Wielkopolski");
			IBookModel firstBook = library.GetAvailableBooks().First();

			// Act
			try
			{
				library.TakeBookBack(firstBook);
			}
			catch (InvalidOperationException ex)
			{
				Assert.AreEqual("This book is not borrowed", ex.Message);
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotTakeBookBackBookDoesNotBelongToTheLibrary_Fail()
		{
			// Arrange
			ILibraryModel library1 = new LibraryModel("Kamil Norwid's Library in Wrocław");
			ILibraryModel library2 = new LibraryModel("Forgotten library");
			IBookModel book = null;//new BookModel("Grendel Snowman", "Fu Manchu", 1999, library1);
			IUserModel user = Substitute.For<IUserModel>();
			book.Borrower = user;

			// Act
			try
			{
				library2.TakeBookBack(book);
			}
			catch (InvalidOperationException ex)
			{
				Assert.AreEqual("This book does not belong to this library", ex.Message);
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldAddBook_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");
			IBookModel book = null;// new BookModel("Debug Hard: Yakatomi Plaza", "Michael Wayne", 2008, library);

			// Act
			library.AddBook(book);
			int availableBooksCount = library.GetAvailableBooks().Count;

			// Assert
			Assert.AreEqual(6, availableBooksCount);
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotAddBookBookIsNull_Fail()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			try
			{
				library.AddBook(null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldRemoveBook_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");
			IBookModel book = library.GetAvailableBooks().First();

			// Act
			library.RemoveBook(book);
			int availableBooksCount = library.GetAvailableBooks().Count;

			// Assert
			Assert.AreEqual(4, availableBooksCount);
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotRemoveBookBookIsNull_Fail()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			try
			{
				library.RemoveBook(null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotRemoveBookBookDoesNotBelongToTheLibrary_Fail()
		{
			// Arrange
			ILibraryModel library1 = new LibraryModel("George Wisdomer National Library");
			ILibraryModel library2 = new LibraryModel("Another Library");
			IBookModel book = null;//new BookModel("So Far To Stop Process", "Jeffrey Nano", 2014, library2);

			// Act
			try
			{
				library1.RemoveBook(book);
			}
			catch (InvalidOperationException ex)
			{
				Assert.AreEqual("This book does not belong to this library", ex.Message);
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldUpdateBook_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");
			IBookModel book = library.GetAvailableBooks().First();
			//book.UpdateData("New title", "New author", 1000);

			// Act
			library.UpdateBook(book);
			IBookModel changedBook = library.GetAvailableBooks().First();
			string newTitle = changedBook.Title;
			string newAuthor = changedBook.Author;
			ushort newYear = changedBook.Year;

			// Assert
			Assert.AreEqual("New title", newTitle, "Book's new title is different than expected");
			Assert.AreEqual("New author", newAuthor, "Book's new author is different than expected");
			Assert.AreEqual(1000, newYear, "Book's new year is different than expected");
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotUpdateBookBookIsNull_Fail()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			try
			{
				library.UpdateBook(null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit fail")]
		public void Library_ShouldNotUpdateBookBookDoesNotBelongToTheLibrary_Fail()
		{
			// Arrange
			ILibraryModel library1 = new LibraryModel("George Wisdomer National Library");
			ILibraryModel library2 = new LibraryModel("Another Library");
			IBookModel book = library1.GetAvailableBooks().First();

			// Act
			try
			{
				library2.UpdateBook(book);
			}
			catch (InvalidOperationException ex)
			{
				Assert.AreEqual("This book does not belong to this library", ex.Message);
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldSearchBooksByTitle_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			List<IBookModel> matchedBooks = library.SearchByTitle("Jack Refactor - True Story");
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(1, matchedBooksCount);
		}

		[Test]
		[Category("Library unit fail")]
		[TestCase(null)]
		[TestCase("")]
		public void Library_ShouldNotSearchBooksByTitleInvalidTitle_Fail(string title)
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			try
			{
				List<IBookModel> matchedBooks = library.SearchByTitle(title);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotSearchBooksByTitleBooksBorrowed_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");
			IBookModel book = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			book.Borrower = user;

			// Act		
			List<IBookModel> matchedBooks = library.SearchByTitle(book.Title);
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(0, matchedBooksCount);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotSearchBooksByTitleNoSuchTitle_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act		
			List<IBookModel> matchedBooks = library.SearchByTitle("Impossible title");
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(0, matchedBooksCount);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldSearchBooksByAuthor_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			List<IBookModel> matchedBooks = library.SearchByAuthor("Papa Coach");
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(1, matchedBooksCount);
		}

		[Test]
		[Category("Library unit fail")]
		[TestCase(null)]
		[TestCase("")]
		public void Library_ShouldNotSearchBooksByAuthorInvalidAuthor_Fail(string author)
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			try
			{
				List<IBookModel> matchedBooks = library.SearchByAuthor(author);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotSearchBooksByAuthorBooksBorrowed_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");
			IBookModel book = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			book.Borrower = user;

			// Act		
			List<IBookModel> matchedBooks = library.SearchByAuthor(book.Author);
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(0, matchedBooksCount);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotSearchBooksByAuthorNoSuchAuthor_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act		
			List<IBookModel> matchedBooks = library.SearchByAuthor("Impossible author");
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(0, matchedBooksCount);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldSearchBooksByYear_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act
			List<IBookModel> matchedBooks = library.SearchByYear(2011);
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(2, matchedBooksCount);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotSearchBooksByYearBooksBorrowed_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");
			IBookModel book = library.GetAvailableBooks().First();
			IUserModel user = Substitute.For<IUserModel>();
			book.Borrower = user;

			// Act		
			List<IBookModel> matchedBooks = library.SearchByYear(book.Year);
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(1, matchedBooksCount);      // Because first and the last book both have the same year
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotSearchBooksByYearNoSuchYear_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("George Wisdomer National Library");

			// Act		
			List<IBookModel> matchedBooks = library.SearchByYear(1);
			int matchedBooksCount = matchedBooks.Count;

			// Assert
			Assert.AreEqual(0, matchedBooksCount);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldBeEqual_Success()
		{
			// Arrange
			ILibraryModel library1 = new LibraryModel("An old abandoned library");
			ILibraryModel library2 = new LibraryModel("An old abandoned library");

			// Act
			bool result = library1.Equals(library2);

			// Assert
			Assert.True(result);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotBeEqualToTheOther_Success()
		{
			// Arrange
			ILibraryModel library1 = new LibraryModel("An old abandoned library");
			ILibraryModel library2 = new LibraryModel("A newly built library");

			// Act
			bool result = library1.Equals(library2);

			// Assert
			Assert.False(result);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldNotBeEqualToNull_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("An old abandoned library");

			// Act
			bool result = library.Equals(null);

			// Assert
			Assert.False(result);
		}

		[Test]
		[Category("Library unit success")]
		[TestCaseSource("OtherTypeObjects")]
		public void Library_ShouldNotBeEqualToOtherTypes_Success(object obj)
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Another old abandoned library");

			// Act
			bool result = library.Equals(obj);

			// Assert
			Assert.False(result);
		}

		[Test]
		[Category("Library unit success")]
		public void Library_ShouldReturnHashCode_Success()
		{
			// Arrange
			ILibraryModel library = new LibraryModel("Lost in big city library");

			// Act
			int hashCode = library.GetHashCode();
			bool result = "Lost in big city library".GetHashCode() == hashCode;

			// Assert
			Assert.True(result);
		}
	}
}