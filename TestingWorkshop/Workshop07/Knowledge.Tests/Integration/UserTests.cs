﻿using NUnit.Framework;

namespace Knowledge.Tests.Integration
{
	[TestFixture]
	public class UserTests
	{
		[Test]
		[Category("User integration success")]
		public void User_ShouldGetListOfUsers_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User integration success")]
		public void User_ShouldAddNewUser_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User integration success")]
		public void User_ShouldRemoveUser_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("User integration success")]
		public void User_ShouldUpdateUser_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}
	}
}