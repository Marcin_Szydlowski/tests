﻿using NUnit.Framework;

namespace Knowledge.Tests.Integration
{
	[TestFixture]
	public class LibraryTests
	{
		[Test]
		[Category("Library integration success")]
		public void Library_ShouldGetListOfLibraries_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library integration success")]
		public void Library_ShouldAddNewLibrary_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library integration success")]
		public void Library_ShouldRemoveLibrary_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Library integration success")]
		public void Library_ShouldUpdateLibrary_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}
	}
}