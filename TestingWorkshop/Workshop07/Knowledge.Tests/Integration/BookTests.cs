﻿using Knowledge.Building;
using Knowledge.Extensions;
using Knowledge.Paper;
using Knowledge.Repository.Entities;
using Knowledge.Repository.Storage;
using NUnit.Framework;
using System.Linq;

namespace Knowledge.Tests.Integration
{
	[TestFixture]
	public class BookTests
	{
		[Test]
		[Category("Book integration success")]
		public void Book_ShouldGetBookById_Success()
		{
			// Arrange
			IBookModel book = new BookModel { Id = 1 };

			// Act
			book.FetchData();
			string title = book.Title;
			string author = book.Author;
			ushort year = book.Year;
			bool isBorrowed = book.IsBorrowed;
			string borrowerFirstName = book.Borrower.FirstName;
			string borrowerLastName = book.Borrower.LastName;
			string libraryName = book.Library.Name;

			// Assert
			Assert.AreEqual("One Day Without Internet", title, "Book's title is different than expected");
			Assert.AreEqual("Nigel Donnahy", author, "Book's author is different than expected");
			Assert.AreEqual(2013, year, "Book's year is different than expected");
			Assert.True(isBorrowed, "Book should be borrowed");
			Assert.AreEqual("Franz", borrowerFirstName, "Book's borrower first name is different than expected");
			Assert.AreEqual("Kropka", borrowerLastName, "Book's borrower first name is different than expected");
			Assert.AreEqual("Library of Sulęcin", libraryName, "Library's name is different than expected");
		}

		[Test]
		[Category("Book integration success")]
		public void Book_ShouldAddNewBook_Success()
		{
			// Arrange
			ILibraryModel library = LibraryRepository.GetLibrary(1).Map();
			IBookModel newBook = new BookModel
			{
				Title = "Sizzle And Drizzle On The Frying Pan",
				Author = "Jason Dougnut",
				Year = 2005,
				Library = library
			};
			BookEntity bookEntity = newBook.Map();

			// Act
			BookRepository.AddBook(bookEntity);
			int booksCount = BookRepository.GetBooks().Count();

			// Assert
			Assert.AreEqual(3, booksCount);
		}

		[Test]
		[Category("Book integration success")]
		public void Book_ShouldRemoveBook_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Book integration success")]
		public void Book_ShouldUpdateBook_Success()
		{
			// Arrange

			// Act

			// Assert
			Assert.Fail();
		}
	}
}