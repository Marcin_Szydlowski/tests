﻿using Knowledge.Building;
using Knowledge.Human;
using Knowledge.Paper;
using Knowledge.Repository.Entities;
using MC.CommonUtils.Extensions;

namespace Knowledge.Extensions
{
	public static class Mapper
	{
		public static IBookModel Map(this BookEntity bookEntity)
		{
			bookEntity.EnsureNotNull(nameof(bookEntity));

			ILibraryModel library = bookEntity.Library.Map();

			IBookModel book = new BookModel
			{
				Id = bookEntity.Id,
				Title = bookEntity.Title,
				Author = bookEntity.Author,
				Year = bookEntity.Year
			};

			if (bookEntity.Borrower != null)
			{
				book.Borrower = bookEntity.Borrower.Map();
			}

			return book;
		}

		public static BookEntity Map(this IBookModel bookModel)
		{
			bookModel.EnsureNotNull(nameof(bookModel));

			BookEntity book = new BookEntity
			{
				Id = bookModel.Id,
				Title = bookModel.Title,
				Author = bookModel.Author,
				Year = bookModel.Year
			};

			LibraryEntity library = bookModel.Library.Map();

			if (bookModel.IsBorrowed)
			{
				book.Borrower = bookModel.Borrower.Map();
			}

			return book;
		}

		public static IUserModel Map(this UserEntity userEntity)
		{
			userEntity.EnsureNotNull(nameof(userEntity));

			IUserModel user = new UserModel(userEntity.FirstName, userEntity.LastName)
			{
				Id = userEntity.Id
			};

			return user;
		}

		public static UserEntity Map(this IUserModel userModel)
		{
			userModel.EnsureNotNull(nameof(userModel));

			UserEntity user = new UserEntity
			{
				Id = userModel.Id,
				FirstName = userModel.FirstName,
				LastName = userModel.LastName
			};

			return user;
		}

		public static ILibraryModel Map(this LibraryEntity libraryEntity)
		{
			libraryEntity.EnsureNotNull(nameof(libraryEntity));

			ILibraryModel library = new LibraryModel(libraryEntity.Name)
			{
				Id = libraryEntity.Id
			};

			return library;
		}

		public static LibraryEntity Map(this ILibraryModel libraryModel)
		{
			libraryModel.EnsureNotNull(nameof(libraryModel));

			LibraryEntity library = new LibraryEntity
			{
				Id = libraryModel.Id,
				Name = libraryModel.Name
			};

			return library;
		}
	}
}