﻿namespace Knowledge.Common
{
	public interface IBaseModel
	{
		int Id { get; set; }
		void FetchData();
		void UpdateData();
	}
}