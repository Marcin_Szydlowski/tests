﻿using Knowledge.Building;
using Knowledge.Common;
using Knowledge.Paper;
using System.Collections.Generic;

namespace Knowledge.Human
{
	public interface IUserModel : IBaseModel
	{
		string FirstName { get; set; }
		string LastName { get; set; }
		uint BooksCount { get; }
		void BorrowBook(IBookModel book);
		void ReturnBook(IBookModel book);
		List<IBookModel> GetBorrowedBooks();
		List<ILibraryModel> GetLibraries();
	}
}