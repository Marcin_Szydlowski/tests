﻿using Knowledge.Building;
using Knowledge.Extensions;
using Knowledge.Paper;
using Knowledge.Repository.Entities;
using Knowledge.Repository.Storage;
using MC.CommonUtils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Knowledge.Human
{
	public class UserModel : IUserModel, IEquatable<IUserModel>
	{
		private List<IBookModel> borrowedBooks = new List<IBookModel>();
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public uint BooksCount
		{
			get
			{
				return (uint)borrowedBooks.Count;
			}
		}
		public int Id { get; set; }

		public UserModel(string firstName, string lastName)
		{
			firstName.EnsureNotNullOrEmpty(nameof(firstName));
			lastName.EnsureNotNullOrEmpty(nameof(lastName));

			FirstName = firstName;
			LastName = lastName;
		}

		public void FetchData()
		{
			UserEntity userEntity = UserRepository.GetUser(Id);

			Id = userEntity.Id;
			FirstName = userEntity.FirstName;
			LastName = userEntity.LastName;

			borrowedBooks.Clear();

			List<BookEntity> relatedBooks = BookRepository.GetBooks()
				.Where(b => b.Borrower.Id == Id)
				.ToList();

			relatedBooks.ForEach(b =>
			{
				IBookModel book = b.Map();
				borrowedBooks.Add(book);
			});
		}

		public void UpdateData()
		{
			UserEntity userEntity = this.Map();

			UserRepository.UpdateUser(userEntity);
		}

		public void BorrowBook(IBookModel book)
		{
			// TODO: MSZ: to check
			book.EnsureNotNull(nameof(book));

			if (book.IsBorrowed)
			{
				throw new InvalidOperationException("This book is already borrowed");
			}

			book.Library.LendBook(this, book);
			borrowedBooks.Add(book);
		}

		public void ReturnBook(IBookModel book)
		{// TODO: MSZ: to check
			book.EnsureNotNull(nameof(book));

			if (!borrowedBooks.Contains(book))
			{
				throw new InvalidOperationException("User doesn't have this book");
			}

			book.Library.TakeBookBack(book);
			borrowedBooks.Remove(book);
		}

		public List<IBookModel> GetBorrowedBooks()
		{
			return borrowedBooks;
		}

		public List<ILibraryModel> GetLibraries()
		{
			return borrowedBooks
				.Select(b => b.Library)
				.Distinct()
				.ToList();
		}

		public bool Equals(IUserModel other)
		{
			if (other != null)
			{
				return FirstName == other.FirstName &&
					LastName == other.LastName;
			}

			return false;
		}

		public override bool Equals(object obj)
		{
			IUserModel user = obj as IUserModel;

			if (user != null)
			{
				return Equals(user);
			}

			return false;
		}

		public override int GetHashCode()
		{
			return FirstName.GetHashCode() ^
				LastName.GetHashCode();
		}
	}
}