﻿using Knowledge.Building;
using Knowledge.Extensions;
using Knowledge.Human;
using Knowledge.Repository.Entities;
using Knowledge.Repository.Storage;
using System;

namespace Knowledge.Paper
{
	public class BookModel : IBookModel, IEquatable<IBookModel>
	{
		public string Title { get; set; }
		public string Author { get; set; }
		public ushort Year { get; set; }
		public bool IsBorrowed
		{
			get
			{
				return Borrower != null;
			}
		}
		public IUserModel Borrower { get; set; }
		public ILibraryModel Library { get; set; }
		public int Id { get; set; }

		//public BookModel(string title, string author, ushort year, ILibraryModel library)
		//{
		//	title.EnsureNotNullOrEmpty(nameof(title));
		//	author.EnsureNotNullOrEmpty(nameof(author));
		//	library.EnsureNotNull(nameof(library));

		//	Title = title;
		//	Author = author;
		//	Year = year;
		//	Library = library;
		//}

		public void FetchData()
		{
			BookEntity bookEntity = BookRepository.GetBook(Id);

			Id = bookEntity.Id;
			Title = bookEntity.Title;
			Author = bookEntity.Author;
			Year = bookEntity.Year;

			if (bookEntity.Borrower != null)
			{
				Borrower = bookEntity.Borrower.Map();
			}

			Library = bookEntity.Library.Map();
		}

		public void UpdateData()
		{
			BookEntity bookEntity = this.Map();

			BookRepository.UpdateBook(bookEntity);
		}

		public bool Equals(IBookModel other)
		{
			if (other != null)
			{
				return Title == other.Title &&
					Author == other.Author &&
					Year == other.Year;
			}

			return false;
		}

		public override bool Equals(object obj)
		{
			IBookModel book = obj as IBookModel;

			if (book != null)
			{
				return Equals(book);
			}

			return false;
		}

		public override int GetHashCode()
		{
			return Title.GetHashCode() ^
				Author.GetHashCode() ^
				Year.GetHashCode() ^
				Library.GetHashCode();
		}
	}
}