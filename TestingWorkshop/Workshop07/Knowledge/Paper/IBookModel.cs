﻿using Knowledge.Building;
using Knowledge.Common;
using Knowledge.Human;

namespace Knowledge.Paper
{
	public interface IBookModel : IBaseModel
	{
		string Title { get; }
		string Author { get; }
		ushort Year { get; }
		bool IsBorrowed { get; }
		IUserModel Borrower { get; set; }
		ILibraryModel Library { get; }
	}
}