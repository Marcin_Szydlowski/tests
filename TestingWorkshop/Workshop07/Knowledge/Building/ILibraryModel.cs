﻿using Knowledge.Common;
using Knowledge.Human;
using Knowledge.Paper;
using System.Collections.Generic;

namespace Knowledge.Building
{
	public interface ILibraryModel : IBaseModel
	{
		string Name { get; }
		List<IBookModel> GetAvailableBooks();
		List<IUserModel> GetUsers();
		void LendBook(IUserModel user, IBookModel book);
		void TakeBookBack(IBookModel book);
		void AddBook(IBookModel book);
		void RemoveBook(IBookModel book);
		void UpdateBook(IBookModel book);
		List<IBookModel> SearchByTitle(string title);
		List<IBookModel> SearchByAuthor(string author);
		List<IBookModel> SearchByYear(ushort year);
	}
}