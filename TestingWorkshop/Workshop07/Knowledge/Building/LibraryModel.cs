﻿using Knowledge.Human;
using Knowledge.Paper;
using MC.CommonUtils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Knowledge.Building
{
	public class LibraryModel : ILibraryModel, IEquatable<ILibraryModel>
	{
		private List<IBookModel> books = new List<IBookModel>();
		public string Name { get; private set; }
		public int Id { get; set; }

		public LibraryModel(string name)
		{
			name.EnsureNotNullOrEmpty(nameof(name));

			Name = name;
			InitializeRepository();
		}

		public List<IBookModel> GetAvailableBooks()
		{
			return books
				.Where(b => b.IsBorrowed == false)
				.ToList();
		}

		public List<IUserModel> GetUsers()
		{
			return books
				.Where(b => b.IsBorrowed)
				.Select(b => b.Borrower)
				.Distinct()
				.ToList();
		}

		public void LendBook(IUserModel user, IBookModel book)
		{
			user.EnsureNotNull(nameof(user));
			book.EnsureNotNull(nameof(book));

			if (book.IsBorrowed)
			{
				throw new InvalidOperationException("This book is already borrowed");
			}

			book.Borrower = user;
		}

		public void TakeBookBack(IBookModel book)
		{
			book.EnsureNotNull(nameof(book));

			if (!book.IsBorrowed)
			{
				throw new InvalidOperationException("This book is not borrowed");
			}

			if (book.Library.Name != Name)
			{
				throw new InvalidOperationException("This book does not belong to this library");
			}

			book.Borrower = null;
		}

		public void AddBook(IBookModel book)
		{
			book.EnsureNotNull(nameof(book));

			books.Add(book);
		}

		public void RemoveBook(IBookModel book)
		{
			book.EnsureNotNull(nameof(book));

			if (book.Library.Name != Name)
			{
				throw new InvalidOperationException("This book does not belong to this library");
			}

			books.Remove(book);
		}

		public void UpdateBook(IBookModel book)
		{
			book.EnsureNotNull(nameof(book));

			if (book.Library.Name != Name)
			{
				throw new InvalidOperationException("This book does not belong to this library");
			}

			IBookModel bookToChange = books.First(b => b.Id == book.Id);
			//bookToChange.UpdateData(book.Title, book.Author, book.Year);
		}

		public List<IBookModel> SearchByTitle(string title)
		{
			title.EnsureNotNullOrEmpty(nameof(title));

			return books.
				Where(b => b.IsBorrowed == false && b.Title == title)
				.ToList();
		}

		public List<IBookModel> SearchByAuthor(string author)
		{
			author.EnsureNotNullOrEmpty(nameof(author));

			return books.
				Where(b => b.IsBorrowed == false && b.Author == author)
				.ToList();
		}

		public List<IBookModel> SearchByYear(ushort year)
		{
			return books.
				Where(b => b.IsBorrowed == false && b.Year == year)
				.ToList();
		}

		public bool Equals(ILibraryModel other)
		{
			if (other != null)
			{
				return Name.Equals(other.Name);
			}

			return false;
		}

		public override bool Equals(object obj)
		{
			ILibraryModel library = obj as ILibraryModel;

			if (library != null)
			{
				return Equals(library);
			}

			return false;
		}

		public override int GetHashCode()
		{
			return Name.GetHashCode();
		}

		private void InitializeRepository()
		{
			books = new List<IBookModel>
			{
				//new BookModel("Compiled Too Fast", "Dandy Warwick", 2011, this),
				//new BookModel("Dev Ops - Fear Factory", "Jason Build", 2014, this),
				//new BookModel("Jack Refactor - True Story", "Jenny Kendrix", 2009, this),
				//new BookModel("Full Metal Cover", "Fake Muppet", 1999, this),
				//new BookModel("Lost In Solution - Message From Black Box", "Papa Coach", 2011, this)
			};
		}

		public void FetchData()
		{
			throw new NotImplementedException();
		}

		public void UpdateData()
		{
			throw new NotImplementedException();
		}
	}
}