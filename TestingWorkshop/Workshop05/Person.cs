﻿using System;
using System.Diagnostics;

namespace Workshop05
{
	public class Person
	{
		public Person()
		{
			Debug.Fail("Because");
		}

		#region Standard
		public void Case1()
		{

		}

		public int Case1WithReturn()
		{
			return 0;
		}

		public int Case1WithReturnAndParameter(int param)
		{
			return param << 1;
		}

		private void Case2()
		{

		}

		private int Case2WithReturn()
		{
			return 0;
		}

		private int Case2WithReturnAndParameter(int param)
		{
			return param << 1;
		}

		private static void Case3()
		{

		}

		private static int Case3WithReturn()
		{
			return 0;
		}

		private static int Case3WithReturnAndParameter(int param)
		{
			return param << 1;
		}

		public void CaseNested()
		{
			// TODO: MSZ: tests needed
			Case1();
			Case2();
		}

		#endregion

		#region System

		public int GetRandom()      // Always 3
		{
			Random randomizer = new Random(DateTime.Now.Second);

			return randomizer.Next();
		}

		public DateTime GetNow()    // Always 28-09-2015 12:00
		{
			return DateTime.Now;
		}

		#endregion

		#region FakeItEasy, MoQ, NSubstitute

		public virtual int Case4()      // Always 1
		{
			return 11;
		}

		internal virtual int Case5()    // Always 2
		{
			return 12;
		}

		#endregion
	}
}