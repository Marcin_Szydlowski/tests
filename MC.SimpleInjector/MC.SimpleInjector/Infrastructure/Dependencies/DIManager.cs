﻿using SimpleInjector;

namespace MC.SimpleInjector.Infrastructure.Dependencies
{
	internal sealed class DIManager
	{
		private static volatile DIManager instance;
		private static object syncToken = new object();

		private static Container container;

		public static DIManager Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncToken)
					{
						if (instance == null)
						{
							instance = new DIManager();
						}
					}
				}

				return instance;
			}
		}

		private DIManager()
		{
			PrepareDIContaner();
		}

		public T GetObject<T>() where T : class
		{
			return container.GetInstance<T>();
		}

		private void PrepareDIContaner()
		{
			container = new Container();
			container.Register<IDisplay>(() => new Table
			{
				HorizontalResolution = 3000,
				VerticalResolution = 2500,
				ModelDescription = "AV 3x2.5"
			});
			container.Verify();
		}
	}
}
