﻿namespace MC.SimpleInjector.Infrastructure
{
	public interface IDisplay
	{
		uint HorizontalResolution { get; set; }
		uint VerticalResolution { get; set; }
		string ModelDescription { get; set; }

		void ShowInfo();
	}
}
