﻿using System;

namespace MC.SimpleInjector.Infrastructure
{
	internal class Table : IDisplay
	{
		public uint HorizontalResolution { get; set; }
		public uint VerticalResolution { get; set; }
		public string ModelDescription { get; set; }

		public void ShowInfo()
		{
			Console.WriteLine("Table \"{0}\"\n\tHR={1}\n\tVR={2}", ModelDescription, HorizontalResolution, VerticalResolution);
		}
	}
}
