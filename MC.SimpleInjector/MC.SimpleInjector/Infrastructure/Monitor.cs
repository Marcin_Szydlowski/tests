﻿using System;

namespace MC.SimpleInjector.Infrastructure
{
	internal class Monitor
	{
		private IDisplay displayDevice;

		public Monitor(IDisplay display)
		{
			if (display == null)
			{
				throw new ArgumentNullException("display");
			}

			displayDevice = display;
		}

		public void HealthCheck()
		{
			if (displayDevice == null)
			{
				throw new ArgumentNullException("displayDevice");
			}

			displayDevice.ShowInfo();
		}
	}
}
