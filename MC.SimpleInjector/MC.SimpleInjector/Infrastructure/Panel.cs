﻿using System;

namespace MC.SimpleInjector.Infrastructure
{
	internal class Panel : IDisplay
	{
		public uint HorizontalResolution { get; set; }
		public uint VerticalResolution { get; set; }
		public string ModelDescription { get; set; }

		public void ShowInfo()
		{
			Console.WriteLine("HR={0}\nVR={1}MD={2}", HorizontalResolution, VerticalResolution, ModelDescription);
		}
	}
}
