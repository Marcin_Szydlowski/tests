﻿using MC.SimpleInjector.Infrastructure;
using MC.SimpleInjector.Infrastructure.Dependencies;
using System;

namespace MC.SimpleInjector
{
	class Program
	{
		static void Main(string[] args)
		{
			var displayDevice = new Monitor(DIManager.Instance.GetObject<IDisplay>());
			displayDevice.HealthCheck();

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}
