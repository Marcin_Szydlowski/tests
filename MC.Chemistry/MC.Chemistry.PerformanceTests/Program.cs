﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MC.Chemistry.PerformanceTests
{
	class Program
	{
		private static readonly byte IterationsCount = 10;

		private static PerformanceCounter calculationMassCounter;
		private static PerformanceCounter calculationVolumeCounter;
		private static PerformanceCounter calculationDensityCounter;

		private static void Main(string[] args)
		{
			Security.SignAndVerify();

			Console.Write("Please wait, initialization of counters... ");
			InitializeCounters();
			Console.WriteLine("done");

			Console.Write("Starting calculations... ");
			Task[] massTasks = new Task[IterationsCount];
			Task[] volumeTasks = new Task[IterationsCount];
			Task[] densityTasks = new Task[IterationsCount];
			for (int i = 0; i < IterationsCount; i++)
			{
				massTasks[i] = Task.Factory.StartNew(() => ProcessMasses());
				volumeTasks[i] = Task.Factory.StartNew(() => ProcessVolumes());
				densityTasks[i] = Task.Factory.StartNew(() => ProcessDensities());
			}

			Task.WaitAll(massTasks);
			Task.WaitAll(volumeTasks);
			Task.WaitAll(densityTasks);

			Console.WriteLine("done");
			CleanUp();
			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		private static void ProcessDensities()
		{
			double result = Properties.Density(1, 2);
			calculationDensityCounter.Increment();
		}

		private static void ProcessVolumes()
		{
			double result = Properties.Volume(1, 2);
			calculationVolumeCounter.Increment();
		}

		private static void ProcessMasses()
		{
			double result = Properties.Mass(1, 2);
			calculationMassCounter.Increment();
		}

		private static void InitializeCounters()
		{
			InitializePerformanceCounter(ref calculationMassCounter, "MassCalculator", "Calculation mass category", "Mass counter", "Counter for calculation mass", PerformanceCounterType.NumberOfItems32);
			InitializePerformanceCounter(ref calculationVolumeCounter, "VolumeCalculator", "Calculation volume category", "Volume counter", "Counter for calculation volume", PerformanceCounterType.NumberOfItems32);
			InitializePerformanceCounter(ref calculationDensityCounter, "DensityCalculator", "Calculation density category", "Density counter", "Counter for calculation density", PerformanceCounterType.NumberOfItems32);
		}

		private static void InitializePerformanceCounter(ref PerformanceCounter performanceCounter, string categoryName, string categoryHelp, string counterName, string counterHelp, PerformanceCounterType counterType)
		{
			if (!PerformanceCounterCategory.Exists(categoryName))
			{
				var counters = new CounterCreationDataCollection
				{
					new CounterCreationData(counterName,counterHelp,counterType)
				};
				PerformanceCounterCategory.Create(categoryName, categoryHelp, counters);
			}

			performanceCounter = new PerformanceCounter
			{
				CategoryName = categoryName,
				CounterName = counterName,
				MachineName = ".",
				ReadOnly = false
			};
		}

		private static void CleanUp()
		{
			if (calculationMassCounter != null)
			{
				calculationMassCounter.Dispose();
			}

			if (calculationVolumeCounter != null)
			{
				calculationVolumeCounter.Dispose();
			}

			if (calculationDensityCounter != null)
			{
				calculationDensityCounter.Dispose();
			}
		}
	}
}
