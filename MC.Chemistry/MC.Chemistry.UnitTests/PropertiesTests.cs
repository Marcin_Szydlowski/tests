﻿using NUnit.Framework;
using System;

namespace MC.Chemistry.UnitTests
{
	[TestFixture]
	[Category("Units")]
	public class PropertiesTests
	{
		[Test]
		[TestCase(1, 1)]
		[TestCase(1, 2)]
		[TestCase(1, 3)]
		[TestCase(1, 4)]
		[TestCase(5, 1)]
		[TestCase(4, 1)]
		[TestCase(3, 1)]
		[TestCase(2, 1)]
		public void Properties_Density_VolumeAndMassValid(double mass, double volume)
		{
			// arrange
			double expected = mass / volume;

			// act
			double actual = Properties.Density(mass, volume);

			// assert
			Assert.AreEqual(expected, actual);
		}

		[Test]
		[TestCase(double.NaN)]
		[TestCase(double.PositiveInfinity)]
		[TestCase(double.NegativeInfinity)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Density_MassInfinityOrNotANumber(double mass)
		{
			double actual = Properties.Density(mass, 1);
		}

		[Test]
		[TestCase(double.NaN)]
		[TestCase(double.PositiveInfinity)]
		[TestCase(double.NegativeInfinity)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Density_VolumeInfinityOrNotANumber(double volume)
		{
			double actual = Properties.Density(1, volume);
		}

		[Test]
		[TestCase(-1)]
		[TestCase(-10)]
		[TestCase(-100)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Density_MassLessThanZero(double mass)
		{
			double actual = Properties.Density(mass, 1);
		}

		[Test]
		[TestCase(0)]
		[TestCase(-1)]
		[TestCase(-10)]
		[TestCase(-100)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Density_VolumeLessOrEqualsZero(double volume)
		{
			double actual = Properties.Density(1, volume);
		}

		[Test]
		[TestCase(1, 1)]
		[TestCase(1, 2)]
		[TestCase(1, 3)]
		[TestCase(1, 4)]
		[TestCase(5, 1)]
		[TestCase(4, 1)]
		[TestCase(3, 1)]
		[TestCase(2, 1)]
		public void Properties_Volume_DensityAndMassValid(double mass, double density)
		{
			// arrange
			double expected = mass / density;

			// act
			double actual = Properties.Volume(mass, density);

			// assert
			Assert.AreEqual(expected, actual);
		}

		[Test]
		[TestCase(double.NaN)]
		[TestCase(double.PositiveInfinity)]
		[TestCase(double.NegativeInfinity)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Volume_MassInfinityOrNotANumber(double mass)
		{
			double actual = Properties.Volume(mass, 1);
		}

		[Test]
		[TestCase(double.NaN)]
		[TestCase(double.PositiveInfinity)]
		[TestCase(double.NegativeInfinity)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Volume_DensityInfinityOrNotANumber(double density)
		{
			double actual = Properties.Volume(1, density);
		}

		[Test]
		[TestCase(-1)]
		[TestCase(-10)]
		[TestCase(-100)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Volume_MassLessThanZero(double mass)
		{
			double actual = Properties.Volume(mass, 1);
		}

		[Test]
		[TestCase(0)]
		[TestCase(-1)]
		[TestCase(-10)]
		[TestCase(-100)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Volume_DensityLessOrEqualsZero(double density)
		{
			double actual = Properties.Volume(1, density);
		}

		[Test]
		[TestCase(1, 1)]
		[TestCase(1, 2)]
		[TestCase(1, 3)]
		[TestCase(1, 4)]
		[TestCase(5, 1)]
		[TestCase(4, 1)]
		[TestCase(3, 1)]
		[TestCase(2, 1)]
		public void Properties_Mass_VolumeAndDensityValid(double volume, double density)
		{
			// arrange
			double expected = volume * density;

			// act
			double actual = Properties.Mass(volume, density);

			// assert
			Assert.AreEqual(expected, actual);
		}

		[Test]
		[TestCase(double.NaN)]
		[TestCase(double.PositiveInfinity)]
		[TestCase(double.NegativeInfinity)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Mass_VolumeInfinityOrNotANumber(double volume)
		{
			double actual = Properties.Mass(volume, 1);
		}

		[Test]
		[TestCase(double.NaN)]
		[TestCase(double.PositiveInfinity)]
		[TestCase(double.NegativeInfinity)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Mass_DensityInfinityOrNotANumber(double density)
		{
			double actual = Properties.Mass(1, density);
		}

		[Test]
		[TestCase(-1)]
		[TestCase(-10)]
		[TestCase(-100)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Mass_VolumeLessThanZero(double volume)
		{
			double actual = Properties.Mass(volume, 1);
		}

		[Test]
		[TestCase(-1)]
		[TestCase(-10)]
		[TestCase(-100)]
		[ExpectedException(typeof(ArgumentException))]
		public void Properties_Mass_DensityLessThanZero(double density)
		{
			double actual = Properties.Mass(1, density);
		}
	}
}
