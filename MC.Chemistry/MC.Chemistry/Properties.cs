﻿using System;

namespace MC.Chemistry
{
	/// <summary>
	/// Class provides methods which can be used in chemistry and physics.
	/// </summary>
	public static class Properties
	{
		/// <summary>
		/// Calculate density of an object.
		/// </summary>
		/// <param name="mass">Mass of an object</param>
		/// <param name="volume">Volume of an object</param>
		/// <returns>Density as a division mass by volume.</returns>
		/// <example>For mass = 5kg and volume = 2m3 density will be 2.5kg/m3</example>
		/// <exception cref="ArgumentException">Throws when mass or volume are: not a numbers, infinity, mass is less than zero, volume is less or equal zero.</exception>
		public static double Density(double mass, double volume)
		{
			if (Double.IsInfinity(mass) || Double.IsNaN(mass))
			{
				throw new ArgumentException("Mass cannot be an infinity or not-a-number", "mass");
			}

			if (Double.IsInfinity(volume) || Double.IsNaN(volume))
			{
				throw new ArgumentException("Volume cannot be an infinity or not-a-number", "volume");
			}

			if (mass < 0)
			{
				throw new ArgumentException("Mass cannot be less than zero", "mass");
			}

			if (volume <= 0)
			{
				throw new ArgumentException("Volume must be greater than zero", "volume");
			}

			double result = mass / volume;
			return result;
		}

		/// <summary>
		/// Calculate volume of an object.
		/// </summary>
		/// <param name="mass">Mass of an object</param>
		/// <param name="density">Density of an object</param>
		/// <returns>Volume as a division mass by density.</returns>
		/// <example>For mass = 5kg and density = 2.5 volume will be 2m3</example>
		/// <exception cref="ArgumentException">Throws when mass or density are: not a numbers, infinity, mass is less than zero, density is less or equal zero.</exception>
		public static double Volume(double mass, double density)
		{
			if (Double.IsInfinity(mass) || Double.IsNaN(mass))
			{
				throw new ArgumentException("Mass cannot be an infinity or not-a-number", "mass");
			}

			if (Double.IsInfinity(density) || Double.IsNaN(density))
			{
				throw new ArgumentException("Density cannot be an infinity or not-a-number", "density");
			}

			if (mass < 0)
			{
				throw new ArgumentException("Mass cannot be less than zero", "mass");
			}

			if (density <= 0)
			{
				throw new ArgumentException("Density must be greater than zero", "density");
			}

			double result = mass / density;
			return result;
		}

		/// <summary>
		/// Calculate mass of an object.
		/// </summary>
		/// <param name="volume">Volume of an object</param>
		/// <param name="density">Density of an object</param>
		/// <returns>Mass as a multiplication volume and density.</returns>
		/// <example>For volume = 2m3 and density = 2.5kg/m3 mass will be 5kg</example>
		/// <exception cref="ArgumentException">Throws when volume or density are: not a numbers, infinity, less than zero.</exception>
		public static double Mass(double volume, double density)
		{
			if (Double.IsInfinity(volume) || Double.IsNaN(volume))
			{
				throw new ArgumentException("Volume cannot be an infinity or not-a-number", "volume");
			}

			if (Double.IsInfinity(density) || Double.IsNaN(density))
			{
				throw new ArgumentException("Density cannot be an infinity or not-a-number", "density");
			}

			if (volume < 0)
			{
				throw new ArgumentException("Volume cannot be less than zero", "volume");
			}

			if (density < 0)
			{
				throw new ArgumentException("Density cannot be less than zero", "density");
			}

			double result = volume * density;
			return result;
		}
	}
}
