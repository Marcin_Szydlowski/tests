﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MC.Chemistry
{
	public static class Security
	{
		private static readonly string AlgorithmName = "SHA1";
		private static readonly string SubjectName = "CN=Marcin Szydłowski 1984";
		private static readonly string StoreName = "mcWebStore";

		public static void SignAndVerify()
		{
			var textToSign = "Test paragraph";
			byte[] signature = Sign(textToSign, SubjectName, StoreName);
			bool verificationResult = Verify(textToSign, SubjectName, StoreName, signature);
		}

		private static byte[] Sign(string text, string subjectName, string storeName)
		{
			Trace.Assert(!String.IsNullOrWhiteSpace(text));
			Trace.Assert(!String.IsNullOrWhiteSpace(subjectName));
			Trace.Assert(!String.IsNullOrWhiteSpace(storeName));
			var certificate = GetCertificate(subjectName, storeName);

			using (var cryptoServiceProvider = (RSACryptoServiceProvider)certificate.PrivateKey)
			{
				byte[] hash = HashData(text);
				var signature = cryptoServiceProvider.SignHash(hash, CryptoConfig.MapNameToOID(AlgorithmName));
				return signature;
			}
		}

		private static bool Verify(string text, string subjectName, string storeName, byte[] signature)
		{
			Trace.Assert(!String.IsNullOrWhiteSpace(text));
			Trace.Assert(!String.IsNullOrWhiteSpace(subjectName));
			Trace.Assert(!String.IsNullOrWhiteSpace(storeName));
			Trace.Assert(signature != null && signature.Length > 0);
			var certificate = GetCertificate(subjectName, storeName);
			using (var cryptoServiceProvider = (RSACryptoServiceProvider)certificate.PublicKey.Key)
			{
				byte[] hash = HashData(text);
				bool verificationResult = cryptoServiceProvider.VerifyHash(hash, CryptoConfig.MapNameToOID(AlgorithmName), signature);
				return verificationResult;
			}
		}

		private static byte[] HashData(string text)
		{
			Trace.Assert(!String.IsNullOrWhiteSpace(text));
			var hashAlgorithm = new SHA1Managed();
			var encoding = new UnicodeEncoding();
			var data = encoding.GetBytes(text);
			var hash = hashAlgorithm.ComputeHash(data);
			return hash;
		}

		private static X509Certificate2 GetCertificate(string subjectName, string storeName)
		{
			Trace.Assert(!String.IsNullOrWhiteSpace(subjectName));
			Trace.Assert(!String.IsNullOrWhiteSpace(storeName));
			var store = new X509Store(storeName, StoreLocation.CurrentUser);
			store.Open(OpenFlags.ReadOnly);
			var certificate = new X509Certificate2("mcWebStore.pfx", "pass");
			return certificate;







			var certificates = store.Certificates.Find(X509FindType.FindBySubjectName, subjectName.Substring(3), false);
			Trace.Assert(certificates != null);

			if (certificates.Count > 0)
			{
				return certificates[certificates.Count - 1];
			}

			return null;
		}
	}
}
