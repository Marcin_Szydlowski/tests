﻿using System.Collections.Generic;

namespace MC.Company.Repository.Infrastructure
{
	public static class PeopleRepository
	{
		private static List<Person> people = new List<Person>
		{
			new Person
			{
				Id=1,
				FirstName="Jane",
				LastName="Yankovich",
				Age=38,
				Salary=5600m,
				DepartmentId=1
			},
			new Person
			{
				Id=2,
				FirstName="Jason",
				LastName="Gunman",
				Age=25,
				Salary=4200m,
				DepartmentId=2,
			},
			new Person
			{
				Id=3,
				FirstName="Mike",
				LastName="Wizard",
				Age=63,
				Salary=7800m,
				DepartmentId=3,
			},
			new Person
			{
				Id=4,
				FirstName="Mea",
				LastName="Mauko",
				Age=31,
				Salary=4350m,
				ManagerId=3,
				DepartmentId=3
			}
		};

		public static IEnumerable<Person> GetPeople()
		{
			return people;
		}
	}
}