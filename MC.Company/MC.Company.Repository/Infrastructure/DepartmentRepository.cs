﻿using MC.Company.Repository.Models;
using System.Collections.Generic;

namespace MC.Company.Repository.Infrastructure
{
	public static class DepartmentRepository
	{
		private static List<Department> departments = new List<Department>
		{
			new Department
			{
				Id=1,
				Name="HR"
			},
			new Department
			{
				Id=2,
				Name="Sales"
			},
			new Department
			{
				Id=3,
				Name="Production"
			}
		};

		public static IEnumerable<Department> GetDepartments()
		{
			return departments;
		}
	}
}