﻿namespace MC.Company.Repository.Models
{
	public class Department : IDepartment
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}