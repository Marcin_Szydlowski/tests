﻿namespace MC.Company.Repository.Models
{
	public interface IPerson : IBaseEntity
	{
		string FirstName { get; set; }
		string LastName { get; set; }
		byte Age { get; set; }
		decimal Salary { get; set; }

		int? ManagerId { get; set; }
		int DepartmentId { get; set; }
	}
}