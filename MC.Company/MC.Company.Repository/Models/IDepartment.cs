﻿namespace MC.Company.Repository.Models
{
	public interface IDepartment : IBaseEntity
	{
		string Name { get; set; }
	}
}