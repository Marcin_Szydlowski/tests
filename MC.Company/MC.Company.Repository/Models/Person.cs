﻿using MC.Company.Repository.Models;

namespace MC.Company.Repository
{
	public class Person : IPerson
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public byte Age { get; set; }
		public decimal Salary { get; set; }

		public int? ManagerId { get; set; }
		public int DepartmentId { get; set; }
	}
}