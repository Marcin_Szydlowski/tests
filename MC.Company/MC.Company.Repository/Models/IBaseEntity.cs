﻿namespace MC.Company.Repository.Models
{
	public interface IBaseEntity
	{
		int Id { get; set; }
	}
}