﻿using MC.Company.CommonUtils.Extensions;
using MC.Company.Repository.Models;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Linq.Expressions;

namespace MC.Company.CommonUtils.Tests
{
	[TestFixture]
	[Category("Contracts")]
	public class ContractHelperTests
	{
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void EnsureNotNull_ShouldFailWhenExpressionNull()
		{
			// Arrange
			Expression<Func<string>> expression = null;

			// Act
			ContractHelper.EnsureNotNull(expression);

			// Assert
			// should not pass here
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void EnsureNotNull_ShouldFailWhenValueNull_string()
		{
			// Arrange
			string text = null;

			// Act
			ContractHelper.EnsureNotNull(() => text);

			// Assert
			// should not pass here
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void EnsureNotNull_ShouldFailWhenValueNull_object()
		{
			// Arrange
			IPerson person = null;

			// Act
			ContractHelper.EnsureNotNull(() => person);

			// Assert
			// should not pass here
		}

		[Test]
		[TestCase("")]
		[TestCase("Message about system's state")]
		public void EnsureNotNull_ShouldPassWhenValueNotNull_string(string value)
		{
			// Arrange

			// Act
			ContractHelper.EnsureNotNull(() => value);

			// Assert
			Assert.False(value == null);
		}

		[Test]
		public void EnsureNotNull_ShouldPassWhenValueNotNull_object()
		{
			// Arrange
			var person = Substitute.For<IPerson>();
			person.Age.Returns((byte)37);
			person.FirstName.Returns("Jason");
			person.LastName.Returns("Yankovich");

			// Act
			ContractHelper.EnsureNotNull(() => person);

			// Assert
			Assert.False(person == null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void EnsureNotNullOrEmpty_ShouldFailWhenExpressionNull()
		{
			// Arrange
			Expression<Func<string>> expression = null;

			// Act
			ContractHelper.EnsureNotNullOrEmpty(expression);

			// Assert
			// should not pass here
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "Cannot cast expression to member")]
		public void EnsureNotNullOrEmpty_ShouldFailWhenExpressionConstant_null()
		{
			// Arrange

			// Act
			ContractHelper.EnsureNotNullOrEmpty(() => null);

			// Assert
			// should not pass here
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "Cannot cast expression to member")]
		public void EnsureNotNullOrEmpty_ShouldFailWhenExpressionConstant_empty()
		{
			// Arrange

			// Act
			ContractHelper.EnsureNotNullOrEmpty(() => "");

			// Assert
			// should not pass here
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), ExpectedMessage = "\"value\" is null or empty")]
		[TestCase(null)]
		[TestCase("")]
		public void EnsureNotNullOrEmpty_ShouldFailWhenStringEmptyOrNull(string value)
		{
			// Arrange

			// Act
			ContractHelper.EnsureNotNullOrEmpty(() => value);

			// Assert
			// should not pass here
		}

		[Test]
		[TestCase("    ")]
		[TestCase("Message about tests' execution")]
		public void EnsureNotNullOrEmpty_ShouldPassWhenStringNotNull(string message)
		{
			// Arrange

			// Act
			ContractHelper.EnsureNotNull(() => message);

			// Assert
			Assert.False(string.IsNullOrEmpty(message));
		}
	}
}