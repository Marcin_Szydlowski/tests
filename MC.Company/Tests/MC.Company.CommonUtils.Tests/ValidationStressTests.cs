﻿using MC.CommonUtils.Extensions;
using MC.Company.CommonUtils.Extensions;
using NUnit.Framework;
using System;
using System.Diagnostics;

namespace MC.Company.CommonUtils.Tests
{
	[TestFixture]
	[Category("Stress")]
	public class ValidationStressTests
	{
		private static readonly int TestsCount = 100000;
		private static readonly string[] Texts = { null, "", "abc" };
		private static readonly Random Randomizer = new Random(DateTime.Now.Millisecond);

		[Test]
		public void RunConditions()
		{
			var textsCount = Texts.Length;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int i = 0; i < TestsCount; i++)
			{
				try
				{
					string text = Texts[Randomizer.Next(textsCount)];

					if (string.IsNullOrEmpty(text))
					{
						throw new ArgumentException($"\"{nameof(textsCount)}\" is null or empty");
					}
				}
				catch
				{
					// No handling - loop should continue
				}
			}

			stopwatch.Stop();
			Debug.WriteLine(stopwatch.Elapsed.ToString());
		}

		[Test]
		public void RunExtensions()
		{
			var textsCount = Texts.Length;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int i = 0; i < TestsCount; i++)
			{
				try
				{
					string message = Texts[Randomizer.Next(textsCount)];

					message.EnsureNotNullOrEmpty(nameof(message));
				}
				catch
				{
					// No handling - loop should continue
				}
			}

			stopwatch.Stop();
			Debug.WriteLine(stopwatch.Elapsed.ToString());
		}

		[Test]
		public void RunContracts()
		{
			var textsCount = Texts.Length;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int i = 0; i < TestsCount; i++)
			{
				try
				{
					string text = Texts[Randomizer.Next(textsCount)];

					ContractHelper.EnsureNotNullOrEmpty(() => text);
				}
				catch
				{
					// No handling - loop should continue
				}
			}

			stopwatch.Stop();
			Debug.WriteLine(stopwatch.Elapsed.ToString());
		}

		[Test]
		public void RunAll()
		{
			RunConditions();
			RunExtensions();
			RunContracts();
		}
	}
}