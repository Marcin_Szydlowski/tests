﻿namespace MC.Company.Api.Models
{
	public class PersonDto
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public byte Age { get; set; }
		public decimal Salary { get; set; }
	}
}