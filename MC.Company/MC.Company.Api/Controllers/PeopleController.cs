﻿using MC.Company.Api.Models;
using MC.Company.Repository.Infrastructure;
using Nelibur.ObjectMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace MC.Company.Api.Controllers
{
	[RoutePrefix("api/v1/People")]
	public class PeopleController : ApiController
	{
		[Route("List")]
		[HttpGet]
		public Task<IEnumerable<PersonDto>> GetPeople()
		{
			var peopleDto = PeopleRepository.GetPeople().Select(p =>
			{
				var personDto = TinyMapper.Map<PersonDto>(p);
				return personDto;
			});

			return Task.FromResult(peopleDto);
		}
	}
}