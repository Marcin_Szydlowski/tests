﻿using MC.CommonUtils.Extensions;
using System.ComponentModel;

namespace MC.Company.Models
{
	public class PersonViewModel
	{
		private decimal salary = 0;
		private string firstName = string.Empty;
		private string lastName = string.Empty;

		[Description("First name")]
		public string FirstName
		{
			get
			{
				return firstName;
			}
			set
			{
				value.EnsureNotNullOrEmpty(nameof(value));
				firstName = value;
			}
		}

		[Description("Last name")]
		public string LastName
		{
			get
			{
				return lastName;
			}
			set
			{
				value.EnsureNotNullOrEmpty(nameof(value));
				lastName = value;
			}
		}

		[Description("Age")]
		public byte Age { get; set; }

		[Description("Salary")]
		public decimal Salary
		{
			get
			{
				return salary;
			}

			set
			{
				value.EnsurePositive(nameof(value));

				salary = value;

				if (salary > 5000)
				{
					ToVerify = true;
				}
			}
		}

		[Description("To verify")]
		public bool ToVerify { get; set; }
	}
}