﻿using MC.Company.Api.Models;
using MC.Company.Models;
using Nelibur.ObjectMapper;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MC.Company.Controllers
{
	public class HomeController : Controller
	{
		[HttpGet]
		public ViewResult Index()
		{
			var uri = "http://localhost:62864/api/v1/People/List";
			IEnumerable<PersonViewModel> peopleViewModel;

			using (HttpClient httpClient = new HttpClient())
			{
				Task<string> response = httpClient.GetStringAsync(uri);
				var peopleDto = JsonConvert.DeserializeObject<List<PersonDto>>(response.Result);
				peopleViewModel = peopleDto.Select(p =>
				{
					var personViewModel = TinyMapper.Map<PersonViewModel>(p);
					return personViewModel;
				});
			}

			return View(peopleViewModel);
		}
	}
}