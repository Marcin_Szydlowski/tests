﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace MC.Company.CommonUtils.Extensions
{
	public static class ContractHelper
	{
		public static void EnsureNotNull<TClass>(Expression<Func<TClass>> expression) where TClass : class
		{
			var nameAndValue = GetParameterNameAndValue(expression);

			if (nameAndValue.Value == null)
			{
				throw new ArgumentNullException(nameAndValue.Name);
			}
		}

		public static void EnsureNotNullOrEmpty(Expression<Func<string>> expression)
		{
			var nameAndValue = GetParameterNameAndValue(expression);

			if (string.IsNullOrEmpty(nameAndValue.Value))
			{
				throw new ArgumentException($"\"{nameAndValue.Name}\" is null or empty");
			}
		}

		private static NameValue<T> GetParameterNameAndValue<T>(Expression<Func<T>> expression)
		{
			if (expression == null)
			{
				throw new ArgumentNullException(nameof(expression));
			}

			if (expression.NodeType != ExpressionType.Lambda)
			{
				throw new InvalidOperationException("Type of expression is not lambda");
			}

			var memberExpression = expression.Body as MemberExpression;

			if (memberExpression == null)
			{
				throw new InvalidOperationException("Cannot cast expression to member");
			}

			var constantExpression = (ConstantExpression)memberExpression.Expression;
			T value = (T)((FieldInfo)memberExpression.Member).GetValue(constantExpression.Value);

			return new NameValue<T>(memberExpression.Member.Name, value);
		}

		private struct NameValue<T>
		{
			public string Name;
			public T Value;

			public NameValue(string name, T value)
			{
				Name = name;
				Value = value;
			}
		}
	}
}