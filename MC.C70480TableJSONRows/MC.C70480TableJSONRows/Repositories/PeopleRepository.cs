﻿using MC.C70480TableJSONRows.Models;
using MC.C70480TableJSONRows.Utils.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace MC.C70480TableJSONRows.Repositories
{
	public static class PeopleRepository
	{
		private static readonly IEnumerable<Person> people;

		static PeopleRepository()
		{
			people = PreparePeopleFakeDatabase();
		}

		public static IEnumerable<Person> GetPeople()
		{
			return people;
		}

		public static Person UpdatePersonData(Person person)
		{
			person.ThrowIfNull();

			Person p = people.SingleOrDefault(per => per.Id == person.Id);
			p.Name = person.Name;
			p.Age = person.Age;

			return p;
		}

		private static IEnumerable<Person> PreparePeopleFakeDatabase()
		{
			var people = new List<Person>
			{
				new Person
				{
					Id=1,
					Name="Marcel",
					Age=30,
					Salary=2800m
				},
				new Person
				{
					Id=2,
					Name="Jana",
					Age=43,
					Salary=3500m
				},
				new Person
				{
					Id=34,
					Name="George",
					Age=53,
					Salary=6300m
				},
				new Person
				{
					Id=4,
					Name="Anushka",
					Age=22,
					Salary=1800m
				}
			};

			return people;
		}
	}
}