﻿using MC.C70480TableJSONRows.Models;
using MC.C70480TableJSONRows.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MC.C70480TableJSONRows.Repositories
{
	public static class SimulationStateRepository
	{
		private static readonly byte MaxStatesCount = 5;
		private static readonly Queue<State> states;

		static SimulationStateRepository()
		{
			states = PrepareSimulationStatesFakeRepository();
		}

		public static IEnumerable<State> GetStates()
		{
			return states;
		}

		public static State AddState(string state)
		{
			state.ThrowIfNull();
			int newId = states.Any() ? states.Last().Id + 1 : 1;

			if (states.Count == MaxStatesCount)
			{
				states.Dequeue();
			}

			var newState = new State
			{
				Id = newId,
				StateJSON = state,
				Name = $"{DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")}"
			};

			states.Enqueue(newState);

			return newState;
		}

		public static State GetState(int id)
		{
			State s = states.SingleOrDefault(st => st.Id == id);
			return s;
		}

		private static Queue<State> PrepareSimulationStatesFakeRepository()
		{
			var states = new List<State>
			{
				new State
				{
					Id = 1,
					StateJSON = "[35,55,34,55,34,56,34,57,35,57,35,58]",
					Name = "2015-09-14-06-11-43"
				}
			};

			return new Queue<State>(states);
		}
	}
}