﻿namespace MC.C70480TableJSONRows.Models
{
	public class State : BaseEntity
	{
		public string StateJSON { get; set; }
		public string Name { get; set; }
	}
}