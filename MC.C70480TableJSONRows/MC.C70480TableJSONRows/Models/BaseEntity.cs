﻿namespace MC.C70480TableJSONRows.Models
{
	public abstract class BaseEntity
	{
		public int Id { get; set; }
	}
}