﻿namespace MC.C70480TableJSONRows.Models
{
	public class Person : BaseEntity
	{
		public string Name { get; set; }
		public byte Age { get; set; }
		public decimal Salary { get; set; }
	}
}