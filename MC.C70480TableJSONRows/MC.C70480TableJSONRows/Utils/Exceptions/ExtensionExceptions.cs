﻿using MC.C70480TableJSONRows.Models;
using System;

namespace MC.C70480TableJSONRows.Utils.Exceptions
{
	public static class ExtensionExceptions
	{
		public static void ThrowIfNull(this string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				throw new ArgumentException($"\"{nameof(text)}\" is null or empty");
			}
		}

		public static void ThrowIfNull(this BaseEntity entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException(nameof(entity));
			}
		}
	}
}