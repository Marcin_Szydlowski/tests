﻿$(function () {
	var labelGeolocationSupportedSelector = '#labelGeolocationSupported';
	var labelLatitudeSelector = '#labelLatitude';
	var labelLongitudeSelector = '#labelLongitude';

	function isGeolocationSupported() {
		return 'geolocation' in navigator;
	}

	function showPosition(position) {
		$(labelLatitudeSelector).text(position.coords.latitude);
		$(labelLongitudeSelector).text(position.coords.longitude);
	}

	function showError(error) {
		switch (error.code) {
			case error.PERMISSION_DENIED:
				alert('User denied geolocation access request.');
				break;

			case error.POSITION_UNAVAILABLE:
				alert('Location information unavailable.');
				break;

			case error.TIMEOUT:
				alert('Get user location request timed out.');
				break;

			case error.UNKNOWN_ERROR:
				alert('An unknown error occured.');
				break;
		}
	}

	function setControls() {
		var geolocationSupports = false;//isGeolocationSupported();
		var text = '---';

		$(labelLatitudeSelector).text(text);
		$(labelLongitudeSelector).text(text);

		if (geolocationSupports) {
			text = 'yes';
			var options = {
				enableHighAccuracy: true,
				timeout: 3000,
				maximumAge: 20000
			};
			navigator.geolocation.getCurrentPosition(showPosition, showError, options);
		}
		else {
			text = 'no';
		}

		$(labelGeolocationSupportedSelector).text(text);
	}

	function setEvents() {
		$('#inner').click(function (e) {
			e.stopPropagation();

			alert('inner clicked');
		});

		$('#outer').click(function (e) {			
			alert('outer clicked');
		});
	}

	function init() {
		setControls();
		setEvents();
	}

	init();
});