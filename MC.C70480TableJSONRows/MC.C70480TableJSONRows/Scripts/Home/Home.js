﻿$(function () {
	var tablePeopleSelector = '#tablePeople';

	function clearTableData() {
		$(tablePeopleSelector + ' tbody').remove();
	}

	function fillFormFields(row) {
		$('#personId').val($(row).children().eq(0).text());
		$('#personSalary').val($(row).children().eq(3).text());
		$('#personName').val($(row).children().eq(1).text());
		$('#personAge').val($(row).children().eq(2).text());
	}

	function addDataToTable(data) {
		for (var i = 0; i < data.length; i++) {
			var person = data[i];
			$('<tr><td>' + person.Id + '</td><td>' + person.Name + '</td><td>' + person.Age + '</td><td hidden="hidden">' + person.Salary + '</td></tr>', {
				'id': 'rid_' + (i + 1)
			}).appendTo(tablePeopleSelector);
		}

		$(tablePeopleSelector + ' tbody tr').click(function () {
			fillFormFields(this);
		});
	}

	function setEvents() {
		$('#btnRefreshData').click(function () {
			$.ajax({
				url: 'http://localhost:49468/Home/RefreshPeopleData',
				method: 'GET',
				dataType: 'JSON'
			}).success(function (data) {
				clearTableData();
				addDataToTable(data);
			}).error(function (err) {
				alert('Error: ' + err.statusText);
			});
		});

		$(tablePeopleSelector + ' tbody tr').click(function () {
			fillFormFields(this);
		});
	}

	function init() {
		setEvents();
	}

	init();
});