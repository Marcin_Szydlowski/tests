﻿$(function () {
	var btnStartStopSelector = '#btnSimulationStartStop';
	var btnSimulationLoadSelector = '#btnSimulationLoad';
	var btnSimulationSave = '#btnSimulationSave';
	var lblStateIdSelector = '#lblStateId';
	var lblStateNameSelector = '#lblStateName';
	var intervalSliderSelector = '#stepIntervalSlider';
	var tblSimulationStatesSelector = '#tableStates';
	var zoomUpDownSelector = '#zoomUpDown';
	var colorPickerSelector = '#colorPicker';

	var simulation;
	var simulationIsOn = false;
	var drawingContext;
	var simulationEngineId;
	var stepInterval;
	var zoom;
	var cellsColor;

	function drawSimulation() {
		drawingContext.clearRect(0, 0, 300, 300);

		$.each(simulation.Cells, function (i, c) {
			drawingContext.fillStyle = cellsColor;
			drawingContext.fillRect(c.Position.X * zoom, c.Position.Y * zoom, zoom * 0.8, zoom * 0.8);
		});
	}

	function setSimulation() {
		simulation = new MCCellularAutomata.Simulation('70-480', 'Exam incoming');
		simulation.RandomCells(30, 30, 10, 10, 40);
	}

	function setDrawingContext() {
		drawingContext = $('#simulationCanvas')[0].getContext('2d');
	}

	function setSimulationEngine() {
		window.clearInterval(simulationEngineId);

		simulationEngineId = setInterval(function () {
			if (simulationIsOn) {
				drawSimulation();
				simulation.Next();
			}
		}, stepInterval);
	}

	function setIntervalText() {
		stepInterval = $(intervalSliderSelector).val();
		$('#stepIntervalText').text(stepInterval + ' ms');
		setSimulationEngine();
	}

	function setZoom() {
		zoom = $(zoomUpDownSelector).val();
	}

	function setCellsColor() {
		cellsColor = $(colorPickerSelector).val();
	}

	function setLoadBtnState() {
		var stateChosen = $(lblStateNameSelector).text() !== '' ? '' : 'disabled';

		$(btnSimulationLoadSelector).prop('disabled', stateChosen);
	}

	function setControls() {
		setIntervalText();
		setZoom();
		setCellsColor();
		setLoadBtnState();
	}

	function loadStates() {
		var httpRequest = new XMLHttpRequest();
		httpRequest.open('GET', 'http://localhost:49468/Home/GetStates', true);
		httpRequest.onreadystatechange = function () {
			if (httpRequest.readyState === 4 && httpRequest.status === 200) {
				$(tblSimulationStatesSelector + ' tbody').remove();

				var states = JSON.parse(httpRequest.response);

				for (var i = 0; i < states.length; i++) {
					var state = states[i];
					$('<tr><td>' + state.Id + '</td><td>' + state.Name + '</td></tr>').appendTo($(tblSimulationStatesSelector));
				}

				setTableRowEvents();
			}
		}

		httpRequest.send();
	}

	function setTableRowEvents() {
		$(tblSimulationStatesSelector + ' tbody tr').click(function () {
			$(lblStateIdSelector).text($(this).children().eq(0).text());
			$(lblStateNameSelector).text($(this).children().eq(1).text());
			setLoadBtnState();
		});
	}

	function setEvents() {
		$(btnStartStopSelector).click(function () {
			simulationIsOn = !simulationIsOn;

			var btnText = simulationIsOn ? 'Stop' : 'Start';
			$(btnStartStopSelector).text(btnText);
			$(btnStartStopSelector).toggleClass('btnSimulationOn btnSimulationOff');
		});

		$(btnSimulationSave).click(function () {
			var state = simulation.SaveState();

			if (state) {
				var httpRequest = new XMLHttpRequest();
				httpRequest.open('POST', 'http://localhost:49468/Home/SaveState?state=' + state, true);
				httpRequest.onreadystatechange = function () {
					if (httpRequest.readyState === 4 && httpRequest.status === 200) {
						loadStates();
					}
				};

				httpRequest.send();
			}
		});

		$(btnSimulationLoadSelector).click(function () {
			var httpRequest = new XMLHttpRequest();
			httpRequest.open('GET', 'http://localhost:49468/Home/GetState?id=' + $(lblStateIdSelector).text(), true);
			httpRequest.onreadystatechange = function () {
				if (httpRequest.readyState === 4 && httpRequest.status === 200) {
					simulation.RemoveCells();
					var s = simulation.LoadState(httpRequest.response);
					drawSimulation();
				}
			}

			httpRequest.send();
		});

		$(intervalSliderSelector).on('input', function () {
			setIntervalText();
		});

		$(zoomUpDownSelector).on('change', function () {
			setZoom();
			drawSimulation();
		});

		$(colorPickerSelector).on('change', function () {
			setCellsColor();
			drawSimulation();
		});

		setTableRowEvents();
	}

	function init() {
		setDrawingContext();

		setControls();

		setSimulation();
		drawSimulation();
		setSimulationEngine();
		loadStates();

		setEvents();
	}

	init();
});