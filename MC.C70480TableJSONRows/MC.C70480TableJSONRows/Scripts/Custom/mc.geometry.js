/// <reference path='..\_references.js' />

var MCGeometry;

(function (mcgn) {
	// Position as a pair of two numbers
	mcgn.Position = (function () {
		function PositionCtor(x, y) {
			if (x === void 0) {
				x = 0;
			} else {
				MCUtils.Validators.CheckNumberParam(x, 'x');
			}

			if (y === void 0) {
				y = 0;
			} else {
				MCUtils.Validators.CheckNumberParam(y, 'y');
			}

			this._x = x;
			this._y = y;
		}

		Object.defineProperty(PositionCtor.prototype, 'X', {
			get: function () {
				return this._x;
			},
			set: function (value) {
				MCUtils.Validators.CheckNumberParam(value, 'value');

				this._x = value;
			}
		});

		Object.defineProperty(PositionCtor.prototype, 'Y', {
			get: function () {
				return this._y;
			},
			set: function (value) {
				MCUtils.Validators.CheckNumberParam(value, 'value');

				this._y = value;
			}
		});

		PositionCtor.prototype.Clone = function () {
			var position = new PositionCtor();

			position.X = this._x;
			position.Y = this._y;

			return position;
		};

		PositionCtor.prototype.Equals = function (target) {
			MCUtils.Validators.CheckCustomTypeParam(target, 'target', 'MCGeometry.Position');

			return this._x === target.X && this._y === target.Y;
		};

		PositionCtor.prototype.ToString = function (decimalPlacesCount) {
			if (decimalPlacesCount === void 0) {
				return '(' + this._x + '; ' + this._y + ')';
			}

			MCUtils.Validators.CheckNumberParam(decimalPlacesCount);

			if (decimalPlacesCount < 0) {
				throw new Error('"decimalPlacesCount" is less than zero');
			}

			return '(' + this._x.toFixed(decimalPlacesCount) + '; ' + this._y.toFixed(decimalPlacesCount) + ')';
		};

		return PositionCtor;
	})();

	// Position as a pair of two integer numbers (coordinates will be rounded)
	mcgn.PositionInt = (function (base) {
		PositionIntCtor.prototype = new mcgn.Position();
		PositionIntCtor.prototype.constructor = PositionIntCtor;

		function PositionIntCtor(x, y) {
			base.call(this, x, y)

			this._x = Math.round(this._x);
			this._y = Math.round(this._y);
		}

		PositionIntCtor.prototype.ToString = function () {
			return '(' + this._x + '; ' + this._y + ')';
		};

		return PositionIntCtor;
	})(mcgn.Position);

	// 2-dimensional vector
	mcgn.Vector = (function (base) {
		VectorCtor.prototype = new mcgn.Position();
		VectorCtor.prototype.constructor = VectorCtor;

		function VectorCtor(x, y) {
			base.call(this, x, y);
		}

		VectorCtor.prototype.Add = function (vector) {
			MCUtils.Validators.CheckCustomTypeParam(vector, 'vector', 'MCGeometry.Vector');

			return new VectorCtor(this._x + vector._x, this._y + vector._y);
		};

		VectorCtor.prototype.Multiply = function (value) {
			MCUtils.Validators.CheckNumberParam(value, 'value');

			return new VectorCtor(this._x * value, this._y * value);
		};

		VectorCtor.prototype.IsZero = function () {
			return this._x === 0 && this._y === 0;
		};

		VectorCtor.prototype.Length = function () {
			if (this.IsZero()) {
				return 0;
			}

			return Math.sqrt(this._x * this._x + this._y * this._y);
		};

		VectorCtor.prototype.Unit = function () {
			var length = this.Length();

			if (length === 0) {
				return new VectorCtor();
			}

			return new VectorCtor(this._x / length, this._y / length);
		};

		VectorCtor.prototype.Tangent = function () {
			return new VectorCtor(-this._y, this._x);
		};

		VectorCtor.prototype.FromPoints = function (p1, p2) {
			MCUtils.Validators.CheckCustomTypeParam(p1, 'p1', 'MCGeometry.Position');
			MCUtils.Validators.CheckCustomTypeParam(p2, 'p2', 'MCGeometry.Position');

			return new VectorCtor(p2.X - p1.X, p2.Y - p1.Y);
		};

		VectorCtor.prototype.Clone = function () {
			var vector = new VectorCtor();

			vector.X = this._x;
			vector.Y = this._y;

			return vector;
		};

		VectorCtor.prototype.ToString = function (decimalPlaces) {
			if (decimalPlaces === void 0) {
				decimalPlaces = 4;
			}

			MCUtils.Validators.CheckNumberParam(decimalPlaces, 'decimalPlaces');

			return '[' + this._x.toFixed(decimalPlaces) + '; ' + this._y.toFixed(decimalPlaces) + ']';
		};

		return VectorCtor;
	})(mcgn.Position);

	// Common calculations
	mcgn.Calculations = (function () {
		function CalculationsCtor() {
		};

		CalculationsCtor.prototype.CalcDistance = function (p1, p2) {
			MCUtils.Validators.CheckCustomTypeParam(p1, 'p1', 'MCGeometry.Position');
			MCUtils.Validators.CheckCustomTypeParam(p2, 'p2', 'MCGeometry.Position');

			var diffX = p1.X - p2.X;
			var diffY = p1.Y - p2.Y;

			return Math.sqrt(diffX * diffX + diffY * diffY);
		};

		CalculationsCtor.prototype.CalcMiddlePoint = function (p1, p2) {
			MCUtils.Validators.CheckCustomTypeParam(p1, 'p1', 'MCGeometry.Position');
			MCUtils.Validators.CheckCustomTypeParam(p2, 'p2', 'MCGeometry.Position');

			var middleX = p1.X !== p2.X
				? p2.X + (p1.X - p2.X) / 2
				: p1.X;
			var middleY = p1.Y !== p2.Y
				? p2.Y + (p1.Y - p2.Y) / 2
				: p1.Y;

			return new mcgn.Position(middleX, middleY);
		};

		return CalculationsCtor.prototype;
	})();

})(MCGeometry || (MCGeometry = {}));