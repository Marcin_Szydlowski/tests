/// <reference path='..\_references.js' />

var MCCellularAutomata;

(function (mccan) {
	mccan.NEIGHBOURHOOD_RULE_DIE_LESS = 2;
	mccan.NEIGHBOURHOOD_RULE_DIE_MORE = 4;
	mccan.NEIGHBOURHOOD_RULE_CREATE_NEW = 3;

	mccan.Cell = (function () {
		function CellCtor(position) {
			if (position === void 0) {
				position = new MCGeometry.PositionInt();
			}
			else {
				MCUtils.Validators.CheckCustomTypeParam(position, 'position', 'MCGeometry.PositionInt');
			}

			this._id = MCUtils.Common.GenerateId();
			this._position = position;
			this._neighboursCount = 0;
		}

		Object.defineProperty(CellCtor.prototype, 'Id', {
			get: function () {
				return this._id;
			}
		});

		Object.defineProperty(CellCtor.prototype, 'Position', {
			get: function () {
				return this._position;
			},
			set: function (value) {
				MCUtils.Validators.CheckCustomTypeParam(value, 'value', 'MCGeometry.Position');

				this._position = value;
			}
		});

		Object.defineProperty(CellCtor.prototype, 'NeighboursCount', {
			get: function () {
				return this._neighboursCount;
			}
			,
			set: function (value) {
				MCUtils.Validators.CheckNumberParam(value, 'value');

				if (value < 0) {
					throw new Error('"value" is less than zero: ' + value);
				}

				this._neighboursCount = value;
			}
		});

		CellCtor.prototype.ToString = function () {
			return this._id + ', at ' + this._position.ToString();
		};

		CellCtor.prototype.Equals = function (target) {
			return this._position.Equals(target.Position);
		};

		CellCtor.prototype.GetHash = function () {
			return this._position.X + ':' + this._position.Y;
		};

		CellCtor.prototype.Clone = function () {
			var cell = new CellCtor();

			cell.Position = this._position.Clone();
			cell.NeighboursCount = this._neighboursCount;

			return cell;
		};

		return CellCtor;
	})();

	mccan.Simulation = (function () {
		var DEFAULT_EMPTY_NAME = '<no name>';
		var DEFAULT_EMPTY_DESCRIPTION = '<no description>';

		function SimulationCtor(name, description) {
			if (name === void 0) {
				name = DEFAULT_EMPTY_NAME;
			}
			else {
				MCUtils.Validators.CheckStringParam(name, 'name');
			}

			if (description === void 0) {
				description = DEFAULT_EMPTY_DESCRIPTION;
			}
			else {
				MCUtils.Validators.CheckStringParam(description, 'description');
			}

			this._probablyNeighbours = new Array();
			this._cells = new Array();
			this._activeCells = new Array();
			this._name = name;
			this._description = description;

			this._setNeighbours = function (cell) {
				this._setNeighbour(cell.Position.X - 1, cell.Position.Y - 1);
				this._setNeighbour(cell.Position.X, cell.Position.Y - 1);
				this._setNeighbour(cell.Position.X + 1, cell.Position.Y - 1);
				this._setNeighbour(cell.Position.X - 1, cell.Position.Y);

				this._setNeighbour(cell.Position.X + 1, cell.Position.Y);
				this._setNeighbour(cell.Position.X - 1, cell.Position.Y + 1);
				this._setNeighbour(cell.Position.X, cell.Position.Y + 1);
				this._setNeighbour(cell.Position.X + 1, cell.Position.Y + 1);
			};

			this._setNeighbour = function (x, y) {
				var hash = x + ':' + y;
				var position = new MCGeometry.PositionInt(x, y);
				var isPositionOccupied = this._cells[hash];

				if (isPositionOccupied) {
					this._cells[hash].NeighboursCount++;
				} else {
					isPositionOccupied = this._probablyNeighbours[hash];

					if (isPositionOccupied) {
						this._probablyNeighbours[hash].NeighboursCount++;
					} else {
						var newNeighbour = new mccan.Cell(position);
						newNeighbour.NeighboursCount = 1;
						this._probablyNeighbours[hash] = newNeighbour;
					}
				}
			};
		}

		Object.defineProperty(SimulationCtor.prototype, 'Name', {
			get: function () {
				return this._name;
			}
		});

		Object.defineProperty(SimulationCtor.prototype, 'Description', {
			get: function () {
				return this._description;
			}
		});

		Object.defineProperty(SimulationCtor.prototype, 'Cells', {
			get: function () {
				return this._activeCells;
			}
		});

		SimulationCtor.prototype.AddCell = function (c) {
			MCUtils.Validators.CheckCustomTypeParam(c, 'c', 'MCCellularAutomata.Cell');

			var hash = c.GetHash();
			var positionAvailable = this._cells[hash];

			if (positionAvailable) {
				throw new Error('This position is actually occupied by another cell');
			}

			this._cells[hash] = c;
			this._activeCells.push(c);
		};

		SimulationCtor.prototype.RemoveCells = function () {
			this._cells = new Array();
			this._activeCells = new Array();
		};

		SimulationCtor.prototype.RandomCells = function (x, y, width, height, cellsCount) {
			MCUtils.Validators.CheckNumberParam(x, 'x');
			MCUtils.Validators.CheckNumberParam(y, 'y');
			MCUtils.Validators.CheckNumberParam(width, 'width');
			MCUtils.Validators.CheckNumberParam(height, 'height');

			if (width <= 0 || height <= 0) {
				throw new Error('"width" or "height" is less or equal zero: width = ' + width + ', height = ' + height);
			}

			var maxCellsNumber = width * height;

			if (cellsCount === void 0) {
				cellsCount = Math.round(Math.sqrt(maxCellsNumber));
			}
			else {
				MCUtils.Validators.CheckNumberParam(cellsCount, 'cellsCount');

				if (cellsCount < 1) {
					throw new Error('"cellsCount" is less than one: ' + cellsCount);
				}
			}

			var cellsToCreateNumber = maxCellsNumber < cellsCount ? maxCellsNumber : cellsCount;

			while (cellsToCreateNumber > 0) {
				var newPosition = new MCGeometry.PositionInt(x + Math.floor(Math.random() * width), y + Math.floor(Math.random() * height));
				var hash = newPosition.X + ':' + newPosition.Y;
				var positionAvailable = this._cells[hash];

				if (!positionAvailable) {
					var cell = new mccan.Cell(newPosition);
					this._cells[hash] = cell;
					this._activeCells.push(cell);
					cellsToCreateNumber--;
				}
			}
		};

		SimulationCtor.prototype.Next = function () {
			var newCells = new Array();
			this._probablyNeighbours = new Array();

			// Zero neighbours count for each cell
			for (var hash in this._cells) {
				this._cells[hash].NeighboursCount = 0;
			}

			// Actualize neighbours count for each cell
			for (var hash in this._cells) {
				this._setNeighbours(this._cells[hash]);
			}

			// Save only uncrowded cells
			for (var hash in this._cells) {
				var cell = this._cells[hash];

				if (cell.NeighboursCount >= mccan.NEIGHBOURHOOD_RULE_DIE_LESS && cell.NeighboursCount < mccan.NEIGHBOURHOOD_RULE_DIE_MORE) {
					newCells[hash] = cell;
				}
			}

			// Add satisfied neighbours
			for (var hash in this._probablyNeighbours) {
				var cell = this._probablyNeighbours[hash];

				if (cell.NeighboursCount === mccan.NEIGHBOURHOOD_RULE_CREATE_NEW) {
					newCells[hash] = cell;
				}
			}

			this._cells = newCells;
			this._activeCells = Object.keys(newCells).map(function (k) {
				return newCells[k];
			});
		};

		SimulationCtor.prototype.SaveState = function () {
			var isWebStorageSupported = MCUtils.Common.IsWebStorageSupported();

			if (isWebStorageSupported) {
				var coordinatesArray = new Array(this._activeCells.length * 2);

				for (var i = 0; i < this._activeCells.length; i++) {
					var cell = this._activeCells[i];
					coordinatesArray[i * 2] = cell.Position.X;
					coordinatesArray[i * 2 + 1] = cell.Position.Y;
				}

				var state = JSON.stringify(coordinatesArray);
				var stateKey = GetStateKey(this._name, this._description);
				localStorage[stateKey] = state;

				return state;
			}

			return null;
		};

		SimulationCtor.prototype.LoadState = function (state) {
			var isWebStorageSupported = MCUtils.Common.IsWebStorageSupported();

			if (isWebStorageSupported) {
				if (state !== void 0) {
					MCUtils.Validators.CheckStringParam(state, 'state');
				}
				else {
					var stateKey = GetStateKey(this._name, this._description);
					state = localStorage[stateKey];
				}
				
				var coordinatesArray = JSON.parse(state);

				for (var i = 0; i < coordinatesArray.length; i += 2) {
					var newCell = new MCCellularAutomata.Cell(new MCGeometry.PositionInt(parseInt(coordinatesArray[i]), parseInt(coordinatesArray[i + 1])));
					this.AddCell(newCell);
				}

				return state;
			}

			return null;
		};

		SimulationCtor.prototype.IsPreviousStateExist = function () {
			var isWebStorageSupported = MCUtils.Common.IsWebStorageSupported();

			if (isWebStorageSupported) {
				var stateKey = GetStateKey(this._name, this._description);
				var previousState = localStorage[stateKey];

				if (previousState) {
					return true;
				}
			}

			return false;
		};

		SimulationCtor.prototype.ClearRememberedState = function () {
			var isWebStorageSupported = MCUtils.Common.IsWebStorageSupported();

			if (isWebStorageSupported) {
				var stateKey = GetStateKey(this._name, this._description);
				localStorage.removeItem(stateKey);
			}
		};

		var GetStateKey = function (name, description) {
			return name + '_' + description + '_state';
		};

		return SimulationCtor;
	})();

})(MCCellularAutomata || (MCCellularAutomata = {}));