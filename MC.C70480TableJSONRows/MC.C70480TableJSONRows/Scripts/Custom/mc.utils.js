/// <reference path='..\_references.js' />

var MCUtils;

(function (mcun) {
	mcun.Validators = (function () {
		function ValidatorsCtor() {
		}

		ValidatorsCtor.prototype.CheckNumberParam = function (value, paramName) {
			var valueType = typeof value;

			if (valueType !== 'number') {
				throw new Error(this.GetWrongTypeMessageFor(paramName, valueType, 'number'));
			}
		};

		ValidatorsCtor.prototype.CheckStringParam = function (value, paramName) {
			var valueType = typeof value;

			if (valueType !== 'string') {
				throw new Error(this.GetWrongTypeMessageFor(paramName, valueType, 'string'));
			}
		};

		ValidatorsCtor.prototype.CheckBooleanParam = function (value, paramName) {
			var valueType = typeof value;

			if (valueType !== 'boolean') {
				throw new Error(this.GetWrongTypeMessageFor(paramName, valueType, 'boolean'));
			}
		};

		ValidatorsCtor.prototype.CheckFunctionParam = function (value, paramName) {
			var valueType = typeof value;

			if (valueType !== 'function') {
				throw new Error(this.GetWrongTypeMessageFor(paramName, valueType, 'function'));
			}
		};

		ValidatorsCtor.prototype.CheckCustomTypeParam = function (value, paramName, customType) {
			if (!(value instanceof eval(customType))) {
				throw new Error(this.GetWrongTypeMessageFor(paramName, typeof value, customType));
			}
		}

		ValidatorsCtor.prototype.GetWrongTypeMessageFor = function (paramName, wrongType, expectedType) {
			return 'Wrong type ("' + wrongType + '") of "' + paramName + '" parameter, expected type: "' + expectedType + '"';
		}

		return ValidatorsCtor.prototype;
	})();

	mcun.Common = (function () {
		function CommonCtor() {
		}

		CommonCtor.prototype.GenerateId = function () {
			var d = new Date().getTime();
			var id = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'.replace(/x/g, function (c) {
				var r = (d + Math.random() * 16) % 16 | 0;
				d = Math.floor(d / 16);
				return r.toString(16);
			});

			return id;
		};

		CommonCtor.prototype.IsWebStorageSupported = function () {
			return 'localStorage' in window;
		};

		return CommonCtor.prototype;
	})();

	mcun.Exceptions = (function () {
		function ExceptionsCtor() {
		}

		ExceptionsCtor.prototype.NotImplementedException = function () {
			throw new Error('Not implemented exception');
		}

		ExceptionsCtor.prototype.LessOrEqualZeroException = function (paramName, paramValue) {
			throw new Error('"' + paramName + '" is less or equal zero: ' + paramValue);
		}

		return ExceptionsCtor.prototype;
	})();

})(MCUtils || (MCUtils = {}));