﻿using MC.C70480TableJSONRows.Models;
using MC.C70480TableJSONRows.Repositories;
using System;
using System.Linq;
using System.Web.Mvc;

namespace MC.C70480TableJSONRows.Controllers
{
	public class HomeController : Controller
	{
		[HttpGet]
		public ActionResult Index()
		{
			return View(PeopleRepository.GetPeople());
		}

		[HttpPost]
		public ActionResult Index(Person person)
		{
			try
			{
				Person p = PeopleRepository.UpdatePersonData(person);

				return View(PeopleRepository.GetPeople());
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException("An exception occured during actualize person's data", ex);
			}
		}

		[HttpGet]
		public JsonResult RefreshPeopleData()
		{
			return Json(PeopleRepository.GetPeople(), JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetStates()
		{
			var states = SimulationStateRepository.GetStates().Select(s => new { s.Id, s.Name });
			return Json(states, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public string GetState(int id)
		{
			try
			{
				return SimulationStateRepository.GetState(id).StateJSON;
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException($"An exception occured during getting state for id = {id}", ex);
			}
		}

		[HttpPost]
		public JsonResult SaveState(string state)
		{
			try
			{
				State s = SimulationStateRepository.AddState(state);

				return Json(SimulationStateRepository.GetStates());
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException($"An exception occured during saving state = {state}", ex);
			}
		}

		[HttpGet]
		public ViewResult Simulation()
		{
			return View();
		}

		[HttpGet]
		public ViewResult Geolocation()
		{
			return View();
		}
	}
}