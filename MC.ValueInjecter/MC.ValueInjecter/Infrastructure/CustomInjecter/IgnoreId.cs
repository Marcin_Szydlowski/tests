﻿using Omu.ValueInjecter.Injections;

namespace MC.ValueInjecter.Infrastructure.CustomInjecter
{
	internal class IgnoreId : LoopInjection
	{
		public IgnoreId()
			: this("Id")
		{ }

		public IgnoreId(string idFieldName)
			: base(new[] { idFieldName })
		{ }
	}
}
