﻿using System;

namespace MC.ValueInjecter.Infrastructure
{
	internal class ModelSource : BaseModel
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Salary { get; set; }

		public override string ToString()
		{
			return String.Format("Source:\nId={0}\nName={1}\nDescription={2}\nSalary={3:c}", Id, Name, Description, Salary);
		}
	}
}
