﻿using System;

namespace MC.ValueInjecter.Infrastructure
{
	internal class ModelDestination : BaseModel
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTimeOffset BirthDate { get; set; }

		public override string ToString()
		{
			return String.Format("Destination:\nId={0}\nName={1}\nDescription={2}\nBirthDate={3}", Id, Name, Description, BirthDate.ToUniversalTime());
		}
	}
}
