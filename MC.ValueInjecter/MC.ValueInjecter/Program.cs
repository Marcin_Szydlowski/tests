﻿using MC.ValueInjecter.Infrastructure;
using MC.ValueInjecter.Infrastructure.CustomInjecter;
using Omu.ValueInjecter;
using System;

namespace MC.ValueInjecter
{
	class Program
	{
		static void Main(string[] args)
		{
			IgnoreId();
			IgnoreId2();

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		private static void IgnoreId2()
		{
			ModelSource2 source2 = new ModelSource2
			{
				Name = "Steve",
				Description = "Engineer",
				Salary = 6700m
			};

			ModelDestination2 destination2 = new ModelDestination2
			{
				Id2 = 3,
				BirthDate = DateTimeOffset.Now
			};

			destination2.InjectFrom(new IgnoreId("Id2"), source2);

			Console.WriteLine(source2);
			Console.WriteLine(destination2);
		}

		private static void IgnoreId()
		{
			ModelSource source = new ModelSource
			{
				Name = "Steve",
				Description = "Engineer",
				Salary = 6700m
			};

			ModelDestination destination = new ModelDestination
			{
				Id = 3,
				BirthDate = DateTimeOffset.Now
			};

			destination.InjectFrom<IgnoreId>(source);

			Console.WriteLine(source);
			Console.WriteLine(destination);
		}
	}
}
