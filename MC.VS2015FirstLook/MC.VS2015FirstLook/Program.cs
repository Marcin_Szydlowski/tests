﻿using static System.Console;

namespace MC.VS2015FirstLook
{
	class Program
	{
		static void Main(string[] args)
		{
			var sampleObject = new SampleObject { Text = "abc" };
			//obj? WriteLine(obj.ToString);
			sampleObject?.Text?.WriteLineExt();
			var sampleText = "def";
			var number = -4;
			WriteLine($"{sampleText}, {number}");

			WriteLine("Press any key to exit...");
			ReadKey();
		}
	}

	public static class Extensions
	{
		public static void WriteLineExt(this string text)
		{
			WriteLine(text);
		}
	}
}
