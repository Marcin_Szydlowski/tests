﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace CompareBitmapsVariusTechniques
{
	public static class BitmapComparer
	{
		private static readonly ImageLockMode lockMode = ImageLockMode.ReadOnly;

		[DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern int memcmp(IntPtr bitmap, IntPtr bitmap2, UIntPtr count);

		public static bool CompareBitmapsMemCmp(Bitmap bitmap, Bitmap bitmap2)
		{
			if (bitmap == null && bitmap2 == null)
			{
				return true;
			}

			if (bitmap == null || bitmap2 == null)
			{
				return false;
			}

			if (bitmap.Size != bitmap2.Size)
			{
				return false;
			}

			var areaToLock = new Rectangle(new Point(), bitmap.Size);
			BitmapData bitmapData = bitmap.LockBits(areaToLock, lockMode, bitmap.PixelFormat);
			BitmapData bitmapData2 = bitmap2.LockBits(areaToLock, lockMode, bitmap.PixelFormat);

			try
			{
				int stride = bitmapData.Stride;
				uint len = (uint)(stride * bitmap.Height);

				return memcmp(bitmapData.Scan0, bitmapData2.Scan0, new UIntPtr(len)) == 0;
			}
			finally
			{
				bitmap.UnlockBits(bitmapData);
				bitmap2.UnlockBits(bitmapData2);
			}
		}
	}
}