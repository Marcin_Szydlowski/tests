﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace CompareBitmapsVariusTechniques
{
	class Program
	{
		private static readonly Random randomizer = new Random(DateTime.Now.Millisecond);
		private static readonly Color[] colors = new Color[]
		{
			Color.FromArgb(0xFF,0x81,0x82,0x83),
			Color.FromArgb(0xFF,0xA1,0xA2,0xA3),
			Color.FromArgb(0xFF,0x51,0x52,0x53),
			Color.FromArgb(0xFF,0xC1,0xC2,0xC3)
		};
		static int testsCount = 50;
		static int bytesCount = 1000000;
		static int width = 1000;
		static int bytesPerPixel = 4;
		static Size randomedBitmapSize = new Size(width, bytesCount / width / bytesPerPixel);

		static void Main(string[] args)
		{
			CompareBytes();
			CompareLongs();
			CompareLongsLinq();
			CompareBitmapsEquals();
			CompareBitmapsMemcmp();

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		static void CompareBytes()
		{
			byte[] bytes = new byte[bytesCount];
			byte[] bytes2 = new byte[bytesCount];
			bool equals = true;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int j = 0; j < testsCount && equals; j++)
			{
				for (int i = 0; i < bytes.Length && equals; i++)
				{
					equals = bytes[i] == bytes2[i];
				}
			}

			stopwatch.Stop();
			Console.WriteLine($"Equals: {equals}, {nameof(CompareBytes)}: {stopwatch.Elapsed}");
		}

		static void CompareLongs()
		{
			long[] longs = new long[bytesCount / 8];
			long[] longs2 = new long[bytesCount / 8];
			bool equals = true;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int j = 0; j < testsCount && equals; j++)
			{
				for (int i = 0; i < longs.Length && equals; i++)
				{
					equals = longs[i] == longs2[i];
				}
			}

			stopwatch.Stop();
			Console.WriteLine($"Equals: {equals}, {nameof(CompareLongs)}: {stopwatch.Elapsed}");
		}

		static void CompareLongsLinq()
		{
			long[] longs = new long[bytesCount / 8];
			long[] longs2 = new long[bytesCount / 8];
			bool equals = true;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int j = 0; j < testsCount && equals; j++)
			{
				equals = longs.SequenceEqual(longs2);
			}

			stopwatch.Stop();
			Console.WriteLine($"Equals: {equals}, {nameof(CompareLongsLinq)}: {stopwatch.Elapsed}");
		}

		static void CompareBitmapsEquals()
		{
			var bitmap = RandomBitmap(randomedBitmapSize);
			var bitmap2 = (Bitmap)bitmap.Clone();
			bool equals = true;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int j = 0; j < testsCount && equals; j++)
			{
				equals = bitmap.Equals(bitmap2);
			}

			stopwatch.Stop();
			Console.WriteLine($"Equals: {equals}, {nameof(CompareBitmapsEquals)}: {stopwatch.Elapsed}");
		}

		static void CompareBitmapsMemcmp()
		{
			var bitmap = RandomBitmap(randomedBitmapSize);
			var bitmap2 = (Bitmap)bitmap.Clone();
			bool equals = true;

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			for (int j = 0; j < testsCount && equals; j++)
			{
				equals = BitmapComparer.CompareBitmapsMemCmp(bitmap, bitmap2);
			}

			stopwatch.Stop();
			Console.WriteLine($"Equals: {equals}, {nameof(CompareBitmapsMemcmp)}: {stopwatch.Elapsed}");
		}

		static Bitmap RandomBitmap(Size size)
		{
			var bitmap = new Bitmap(size.Width, size.Height);

			for (int h = 0; h < size.Height; h++)
			{
				for (int w = 0; w < size.Width; w++)
				{
					bitmap.SetPixel(w, h, colors[randomizer.Next(colors.Length)]);
				}
			}

			return bitmap;
		}
	}
}
