﻿using System.Collections.Generic;

namespace MC.EF6Inheritance.Infrastructure.Base
{
	public abstract class Settings : BaseEntity
	{
		public virtual Exercise Exercise { get; set; }
		public virtual ICollection<Result> Results { get; set; }
	}
}
