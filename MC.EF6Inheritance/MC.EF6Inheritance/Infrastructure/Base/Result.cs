﻿namespace MC.EF6Inheritance.Infrastructure.Base
{
	public abstract class Result : BaseEntity
	{
		public virtual User User { get; set; }
		public virtual Settings Settings { get; set; }
	}
}
