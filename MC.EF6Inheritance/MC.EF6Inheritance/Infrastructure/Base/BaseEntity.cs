﻿namespace MC.EF6Inheritance.Infrastructure.Base
{
	public abstract class BaseEntity
	{
		public int Id { get; set; }
	}
}
