﻿using MC.EF6Inheritance.Infrastructure.Base;

namespace MC.EF6Inheritance.Infrastructure
{
	public class KlotskiSettings : Settings
	{
		public decimal MinCost { get; set; }
		public decimal MaxCost { get; set; }
	}
}
