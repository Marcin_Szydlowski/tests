﻿using MC.EF6Inheritance.Infrastructure.Base;

namespace MC.EF6Inheritance.Infrastructure
{
	public class KlotskiResult : Result
	{
		public decimal Cost { get; set; }
	}
}
