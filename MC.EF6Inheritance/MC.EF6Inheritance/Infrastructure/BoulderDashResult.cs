﻿using MC.EF6Inheritance.Infrastructure.Base;

namespace MC.EF6Inheritance.Infrastructure
{
	public class BoulderDashResult : Result
	{
		public string Status { get; set; }
	}
}
