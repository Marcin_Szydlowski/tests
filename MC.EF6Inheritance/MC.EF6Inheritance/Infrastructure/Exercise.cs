﻿using MC.EF6Inheritance.Infrastructure.Base;
using System.Collections.Generic;

namespace MC.EF6Inheritance.Infrastructure
{
	public class Exercise : BaseEntity
	{
		public string Title { get; set; }
		public string Description { get; set; }

		public virtual ICollection<Settings> Settings { get; set; }
	}
}
