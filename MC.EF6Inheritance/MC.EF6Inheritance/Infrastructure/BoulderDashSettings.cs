﻿using MC.EF6Inheritance.Infrastructure.Base;

namespace MC.EF6Inheritance.Infrastructure
{
	public class BoulderDashSettings : Settings
	{
		public byte MinLength { get; set; }
		public byte MaxLength { get; set; }
	}
}
