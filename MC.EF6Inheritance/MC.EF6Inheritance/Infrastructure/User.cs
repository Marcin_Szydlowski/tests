﻿using MC.EF6Inheritance.Infrastructure.Base;
using System.Collections.Generic;

namespace MC.EF6Inheritance.Infrastructure
{
	public class User : BaseEntity
	{
		public string Name { get; set; }

		public virtual ICollection<Result> Results { get; set; }
	}
}
