﻿using MC.EF6Inheritance.Infrastructure;
using MC.EF6Inheritance.Infrastructure.Base;
using System.Data.Entity;

namespace MC.EF6Inheritance.DAL
{
	public class InheritanceContext : DbContext
	{
		public InheritanceContext()
			: base("InheritanceContext")
		{

		}

		public DbSet<User> Users { get; set; }
		public DbSet<Exercise> Exercises { get; set; }
		public DbSet<Settings> Settings { get; set; }
		public DbSet<Result> Results { get; set; }
	}
}
