﻿using MC.EF6Inheritance.Infrastructure;
using MC.EF6Inheritance.Infrastructure.Base;
using System.Collections.Generic;
using System.Data.Entity;

namespace MC.EF6Inheritance.DAL
{
	public class InheritanceInitializer : DropCreateDatabaseAlways<InheritanceContext>
	{
		protected override void Seed(InheritanceContext context)
		{
			// TODO: MSZ: remove below content and create users, exercises, settings, results

			var users = new List<User>
			{
				new User{ Name="Marcin"},
				new User{Name="Marcela"}
			};

			users.ForEach(u => context.Users.Add(u));
			context.SaveChanges();

			var exercises = new List<Exercise>
			{
				new Exercise{ Title="Klotski",Description="An awesome puzzle game"},
				new Exercise{ Title="Boulder Dash", Description="Run for your diamonds!"}
			};

			exercises.ForEach(e => context.Exercises.Add(e));
			context.SaveChanges();

			var settings = new List<Settings>
			{
				new KlotskiSettings{ Exercise= exercises[0],
				 MinCost=0.3m,
				MaxCost=30m},
				new KlotskiSettings{Exercise=exercises[0],
				MinCost=0.2m,
				MaxCost=15m},
				new BoulderDashSettings{
				Exercise=exercises[1],
				 MinLength=1,
				 MaxLength=20
				},
				new BoulderDashSettings{
				Exercise=exercises[1],
				 MinLength=4,
				 MaxLength=20
				},
				new BoulderDashSettings{
				Exercise=exercises[1],
				 MinLength=3,
				 MaxLength=25
				}
			};

			settings.ForEach(s => context.Settings.Add(s));
			context.SaveChanges();

			var results = new List<Result>
			{
				new KlotskiResult{
				 Cost=4m, 
				 Settings=settings[0],
				User=users[0]},
				 new KlotskiResult{
				 Cost=10m,
				  Settings=settings[0],
				 User=users[1]},
				 new KlotskiResult{
				 Cost=11m,
				  Settings=settings[1],
				 User=users[0]},
				 new BoulderDashResult{
				  Status="Bad",
				  Settings=settings[2],
				 User=users[1]},
				   new BoulderDashResult{
				  Status="Ok",
				  Settings=settings[3],
				 User=users[0]},
				   new BoulderDashResult{
				  Status="Average",
				  Settings=settings[3],
				 User=users[1]},
				   new BoulderDashResult{
				  Status="Awesome",
				  Settings=settings[3],
				 User=users[0]},
				   new BoulderDashResult{
				  Status="Interrupted",
				  Settings=settings[4],
				 User=users[1]},
			};

			results.ForEach(r => context.Results.Add(r));
			context.SaveChanges();
		}
	}
}
