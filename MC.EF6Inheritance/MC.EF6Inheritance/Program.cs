﻿using MC.EF6Inheritance.DAL;
using MC.EF6Inheritance.Infrastructure;
using System;
using System.Linq;

namespace MC.EF6Inheritance
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var context = new InheritanceContext())
			{
				foreach (var user in context.Users)
				{
					Console.WriteLine("User name: {0}, {1} result(s):", user.Name, user.Results.Count);

					foreach (var result in user.Results)
					{
						var type = result.GetType();

						if (type.BaseType == typeof(KlotskiResult))
						{
							var klotskiResult = (KlotskiResult)result;
							Console.WriteLine("\tCost={0:c}", klotskiResult.Cost);
						}
						else if (type.BaseType == typeof(BoulderDashResult))
						{
							var boulderDashResult = (BoulderDashResult)result;
							Console.WriteLine("\tStatus={0}", boulderDashResult.Status);
						}
					}

					Console.WriteLine("\tGames played:");
					var exercises = from r in user.Results
									group r.Settings by r.Settings.Exercise.Title into exs
									select new { Title = exs.Key, Times = exs.Count() };

					foreach (var exercise in exercises)
					{
						Console.WriteLine("\t\tGame title: \"{0}\", {1} time(s)", exercise.Title, exercise.Times);
					}
				}
			}

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}
