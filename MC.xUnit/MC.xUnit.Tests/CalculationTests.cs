﻿using System.Diagnostics;
using Xunit;

namespace MC.xUnit.Tests
{
	public class CalculationTests : IUseFixture<Startup>
	{
		public void SetFixture(Startup data)
		{
			Debug.WriteLine("Tests constructor");
		}

		[Fact]
		[Trait("Units", "Calculations")]
		public void Add_Test()
		{
			// arrange
			var a = 8;
			var b = 5;
			var expected = a + b;

			// act
			var result = Calculations.Add(a, b);

			// assert
			Assert.Equal(expected, result);
		}

		[Fact]
		[Trait("Units", "Calculations")]
		public void Sub_Test()
		{
			// arrange
			var a = 8;
			var b = 5;
			var expected = a - b;

			// act
			var result = Calculations.Sub(a, b);

			// assert
			Assert.Equal(expected, result);
		}
	}
}
