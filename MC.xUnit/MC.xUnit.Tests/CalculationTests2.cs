﻿using System.Diagnostics;
using Xunit;

namespace MC.xUnit.Tests
{
	public class CalculationTests2 : IUseFixture<Startup>
	{
		public void SetFixture(Startup data)
		{
			Debug.WriteLine("Tests constructor");
		}

		[Fact]
		[Trait("Units", "Calculations")]
		public void Mul_Test()
		{
			// arrange
			var a = 8;
			var b = 5;
			var expected = a * b;

			// act
			var result = Calculations.Mul(a, b);

			// assert
			Assert.Equal(expected, result);
		}

		[Fact]
		[Trait("Units", "Calculations")]
		public void Div_Test2()
		{
			// arrange
			var a = 8.0;
			var b = 5.0;
			var expected = a / b;

			// act
			var result = Calculations.Div(a, b);

			// assert
			Assert.Equal(expected, result);
		}
	}
}
