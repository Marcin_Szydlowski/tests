﻿using System;
using System.Diagnostics;

namespace MC.xUnit.Tests
{
	public class Startup : IDisposable
	{
		public Startup()
		{
			Debug.WriteLine("Tests begin");
		}

		public void Dispose()
		{
			Debug.WriteLine("Tests finished");
		}
	}
}
