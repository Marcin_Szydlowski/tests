﻿//#define !EMPLOYEE
//#define !HOUSEBOAT
//#define !RECIPE
//#define !DELEGATES
//#define !TRY
//#define !TYPE_SYSTEM
//#define !MULTITHREADING
//#define !TYPES_AND_REFLECTION
//#define !WORKING_WITH_DATA
#define LINQ

using MC.CertRepetitions.Examples;
using MC.CertRepetitions.Observers;
using System;

namespace MC.CertRepetitions
{

	class Program
	{
#if DELEGATES
		public delegate void WriteEmployeeIdDelegate(Employee p);						// contravariance
		public delegate Person GetPersonDelegate(HRList<Employee> hrList, uint id);	// covariance

		static Employee GetEmployee(HRList<Employee> hrList, uint id)
		{
			var result = hrList.Get(id);
			return result;
		}

		static void WritePersonId(Person p)
		{
			Console.WriteLine(p.Id);
		}

		static void SayHello()
		{
			Console.WriteLine("Hello");
		}
#endif

		static void Main(string[] args)
		{
#if EMPLOYEE
			Employee p = new Employee("Jake");
			Employee e = new Manager("Jane");
			Manager m = new Manager("Joan");
			var people = new List<Person> { p, e, m };

			var employeeSalary = "$2,871.45";
			var manager1Salary = "$8,940.28";
			var manager2Salary = "$10,554.32";
			var employeeSalaryValue = Decimal.Parse(employeeSalary, NumberStyles.Currency);
			//NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint);
			var manager1SalaryValue = Decimal.Parse(manager1Salary,
				NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint);
			var manager2SalaryValue = Decimal.Parse(manager2Salary,
				 NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint);
			p.Salary = employeeSalaryValue;
			e.Salary = manager1SalaryValue;
			m.Salary = manager2SalaryValue;

			people.ForEach(person => person.ShowName());
			people.ForEach(person =>
			{
				var emp = person as Employee;
				if (emp != null)
				{
					emp.ShowSalary();
				}
			});

			var hrList = new HRList<Employee>("Team A");
			hrList.Hire(p);
			hrList.Hire(e);
			hrList.Hire(m);
			foreach (var emp in hrList)
			{
				emp.ShowName();
			}
			hrList.Fire(e);
			foreach (var emp in hrList)
			{
				emp.ShowName();
			}
			double x = new double();
			decimal y = (decimal)x;


#elif HOUSEBOAT
			HouseBoat houseBoat = new HouseBoat();
			IBoat boat = (IBoat)houseBoat;
			IHouse house = (IHouse)houseBoat;
#elif RECIPE
			var recipes = new List<Recipe>
			{
				new Recipe{
					MainIngreidient="Flour",
					TotalTime=new TimeSpan(1,30,0),
					CostPerPerson=12.5m
				},
				new Recipe{
					MainIngreidient="Wine",
					TotalTime=new TimeSpan(2,15,0),
					CostPerPerson=30m
				},
				new Recipe{
					MainIngreidient="Sugar",
					TotalTime=new TimeSpan(0,45,0),
					CostPerPerson=6.8m
				},
				new Recipe{
					MainIngreidient="Salt",
					TotalTime=new TimeSpan(0,50,0),
					CostPerPerson=14.75m
				}
			};

			recipes.Sort(new ReciptMainIngredientComparer());
			recipes.ForEach(r => Console.WriteLine(r.MainIngreidient));
			recipes.Sort(new ReciptTotalTimeComparer());
			recipes.ForEach(r => Console.WriteLine(r.TotalTime));
			recipes.Sort(new ReciptCostPerPersonComparer());
			recipes.ForEach(r => Console.WriteLine(r.CostPerPerson.ToString("c")));
#elif DELEGATES
			Employee p = new Employee("Jake");
			Employee e = new Manager("Jane");
			Manager m = new Manager("Joan");
			HRList<Employee> hrList = new HRList<Employee>("J-Team", e, m);

			GetPersonDelegate getPerson = GetEmployee;
			var employee = getPerson(hrList, e.Id);
			employee.ShowName();
			WriteEmployeeIdDelegate writePersonId = WritePersonId;
			writePersonId(p);
			writePersonId(e);
			writePersonId(m);

			Func<float, float> f = delegate(float x)
			{
				return x * x;
			};

			Action note = () => SayHello();
			Func<float, float> f2 = (float x) => x * x;
			Action sayTheNameChanged = () => Console.WriteLine("Name had beend changed");
			m.OnNameChanged += sayTheNameChanged;
			m.OnNameChanged -= sayTheNameChanged;
			m.OnNameChanged -= sayTheNameChanged;

			m.Name = "Joannah";
#elif TRY
			try
			{
				try
				{

				}
				catch (Exception e)
				{

				}
				finally
				{

				}
			}
			catch (Exception e)
			{
				try
				{

				}
				catch (Exception e2)
				{

				}
				finally
				{

				}
			}
			finally
			{
				try
				{

				}
				catch (Exception e)
				{

				}
				finally
				{

				}
			}

			try
			{

			}
			finally
			{

			}

			try
			{
				try
				{
					ExceptionThrower.ArithmeticError();
				}
				catch (Exception)
				{
					throw;
				}
			}
			catch (Exception outerEx)
			{
				Console.WriteLine(outerEx.StackTrace);
			}
#elif TYPE_SYSTEM
			int number = new int();
			int num2 = 0;

			Console.WriteLine(number + ", " + num2);
#elif MULTITHREADING
			BarriersExample.Example();
			LockExample.Example();
#elif TYPES_AND_REFLECTION
			var typesAndReflectionExample = new TypesAndReflectionExample();
			typesAndReflectionExample.Subscribe(new BaseObserver("Types and reflection observer"));
			typesAndReflectionExample.LoadAssemblyExample();
			typesAndReflectionExample.GetExecutingAssemblyTypesExample();
			typesAndReflectionExample.GetModulesExample();
			typesAndReflectionExample.AssemblyCreateInstanceExample();
			typesAndReflectionExample.ActivatorCreateInstanceExample();
			typesAndReflectionExample.GetReferencedAssembliesExample();
			typesAndReflectionExample.GetInt32TypeInfoExample();
			typesAndReflectionExample.GetArrayRankExample();
			typesAndReflectionExample.GetConstructorsExample();
#elif WORKING_WITH_DATA
			var workingWithDataExample = new WorkingWithDataExample();
			workingWithDataExample.Subscribe(new BaseObserver("Working with data observer"));
			workingWithDataExample.BinarySearchExample();
			workingWithDataExample.JsonSerializationExample();
#elif LINQ
			var linqExample = new LINQExample();
			linqExample.Subscribe(new BaseObserver("LINQ observer"));
			linqExample.EvenNumbersExample();
			linqExample.EvenNumbersDeferredExample();
			linqExample.ManyWhereInsteadOfAndExample();
			linqExample.MethodInsteadOfExpressionExample();
			linqExample.OrderbyExample();
			linqExample.ProjectionExample();
			linqExample.AnonymousTypeExample();
			linqExample.AnonymousTypeWithNamedTypesExample();
			linqExample.JoiningExample();
			linqExample.OuterJoinExample();
			linqExample.CompositeKeysExample();
			linqExample.GroupingExample();
			linqExample.GroupingNumbersExample();
			linqExample.GroupingNumbersWithNamingExample();
			linqExample.LinqToXmlExample();
#endif

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}
