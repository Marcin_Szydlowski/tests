﻿
using System;
namespace MC.CertRepetitions.People
{
	public class Employee : Person
	{
		public decimal Salary { get; set; }

		public Employee(string name)
			: base(name)
		{

		}

		public Employee(string name, decimal salary)
			: this(name)
		{
			Salary = salary;
		}

		public override void ShowName()
		{
			Console.WriteLine(String.Format("Employee: {0}", Name));
		}

		public virtual void ShowSalary()
		{
			Console.WriteLine(String.Format("Employee salary: {0:c}", Salary));
		}
	}
}
