﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MC.CertRepetitions.People
{
	public class HRList<E> : IEnumerable<E>
		where E : Employee
	{
		private List<E> employees = new List<E>();
		public string Name { get; set; }

		public HRList(string name, params E[] employees)
		{
			Name = name;

			if (employees != null)
			{
				foreach (var e in employees)
				{
					Hire(e);
				}
			}
		}

		public void Hire(E employee)
		{
			if (!employees.Contains(employee))
			{
				employees.Add(employee);
			}
			else
			{
				throw new InvalidOperationException(String.Format("Employee with Id={0} already exists", employee.Id));
			}
		}

		public void Fire(E employee)
		{
			if (!employees.Contains(employee))
			{
				employees.Remove(employee);
			}
			else
			{
				throw new InvalidOperationException(String.Format("Employee with Id={0} doesn't exist", employee.Id));
			}
		}

		public E Get(uint id)
		{
			E employee = employees.FirstOrDefault(e => e.Id == id);

			if (employee != null)
			{
				return employee;
			}

			throw new InvalidOperationException(String.Format("Employee with Id={0} doesn't exist", employee.Id));
		}

		public IEnumerator<E> GetEnumerator()
		{
			return employees.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
