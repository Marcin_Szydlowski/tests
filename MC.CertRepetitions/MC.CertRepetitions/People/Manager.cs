﻿
using System;
namespace MC.CertRepetitions.People
{
	public class Manager : Employee
	{
		public Manager(string name)
			: base(name)
		{

		}

		public Manager(string name, decimal salary)
			: this(name)
		{
			Salary = salary;
		}

		public override void ShowName()
		{
			Console.WriteLine(String.Format("Manager: {0}", Name));
		}

		public override void ShowSalary()
		{
			Console.WriteLine(String.Format("Manager salary: {0:c}", Salary));
		}
	}
}
