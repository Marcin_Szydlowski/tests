﻿using System;

namespace MC.CertRepetitions.People
{
	public abstract class Person : IEquatable<Person>
	{
		protected static readonly Random Randomizer = new Random(DateTime.Now.Millisecond);

		public event Action OnNameChanged;

		public uint Id { get; private set; }
		private string name;
		public string Name
		{
			get { return name; }
			set
			{
				name = value;

				if (OnNameChanged != null)
				{
					OnNameChanged();
				}
			}
		}

		public Person(string name)
		{
			GenerateId();
			Name = name;
		}

		public abstract void ShowName();

		public bool Equals(Person other)
		{
			return Id == other.Id;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as Person);
		}

		private void GenerateId()
		{
			Id = (uint)(Randomizer.NextDouble() * UInt32.MaxValue);
		}
	}
}
