﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MC.CertRepetitions.Multithreading
{
	internal static class LockExample
	{
		private static object token = new object();
		private static readonly byte maxThreadsCount = 10;
		private static readonly Random randomizer = new Random(DateTime.Now.Millisecond);

		internal static void Example()
		{
			var semaphore = new Semaphore(0, 3);
			var mutex = new Mutex();

			for (byte i = 0; i < maxThreadsCount; i++)
			{
				Task writer = Task.Factory.StartNew(() =>
				{
					lock (token)
					{
						Thread.Sleep(randomizer.Next(1000));
						Console.WriteLine("Current thread id (lock): {0}", Thread.CurrentThread.ManagedThreadId);
					}

					//Monitor.Enter(token);

					//try
					//{
					//	Thread.Sleep(randomizer.Next(1000));
					//	Console.WriteLine("Current thread id (monitor): {0}", Thread.CurrentThread.ManagedThreadId);
					//}
					//finally
					//{
					//	Monitor.Exit(token);
					//}

					semaphore.WaitOne(3);
					Console.WriteLine("Inside semaphore section");
					Thread.Sleep(randomizer.Next(1000));
					semaphore.Release();

					mutex.WaitOne();
					Console.WriteLine("Inside mutex section");
					Thread.Sleep(randomizer.Next(1000));
					mutex.ReleaseMutex();
				});
			}
		}
	}
}
