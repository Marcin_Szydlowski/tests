﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MC.CertRepetitions.Multithreading
{
	internal static class BarriersExample
	{
		internal static void Example()
		{
			long number = 0;

			using (var barrier = new Barrier(4, b => Console.WriteLine("Current phase: {0}, number: {1}", b.CurrentPhaseNumber, number)))
			{
				Action calculations = () =>
				{
					Interlocked.Add(ref number, 3);
					barrier.SignalAndWait();

					Interlocked.Increment(ref number);
					barrier.SignalAndWait();

					Interlocked.Add(ref number, 2);
					barrier.SignalAndWait();

					Interlocked.Add(ref number, 11);
					barrier.SignalAndWait();
				};

				Parallel.Invoke(calculations, calculations, calculations, calculations);
			}
		}
	}
}
