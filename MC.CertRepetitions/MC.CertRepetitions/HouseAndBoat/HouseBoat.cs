﻿
namespace MC.CertRepetitions.HouseAndBoat
{
	public class HouseBoat : IHouse, IBoat
	{
		private float displacement;
		private ushort roomsCount;
		private float length;
		public string Name { get; set; }



		public ushort RoomsCount
		{
			get
			{
				return roomsCount;
			}
			set
			{
				roomsCount = value;
			}
		}

		public float Length
		{
			get
			{
				return length;
			}
			set
			{
				length = value;
			}
		}

		float IBoat.Displacement
		{
			get
			{
				return displacement;
			}
			set
			{
				displacement = value;
			}
		}

		float IBoat.Length
		{
			get
			{
				return length;
			}
			set
			{
				length = value;
			}
		}
	}
}
