﻿
namespace MC.CertRepetitions.HouseAndBoat
{
	public interface IBoat
	{
		float Displacement { get; set; }
		float Length { get; set; }
	}
}
