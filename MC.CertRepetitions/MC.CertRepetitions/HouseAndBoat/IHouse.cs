﻿
namespace MC.CertRepetitions.HouseAndBoat
{
	public interface IHouse
	{
		ushort RoomsCount { get; set; }
		float Length { get; set; }
	}
}
