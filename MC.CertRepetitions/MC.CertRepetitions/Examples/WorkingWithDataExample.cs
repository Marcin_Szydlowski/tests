﻿using MC.CertRepetitions.Examples.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace MC.CertRepetitions.Examples
{
	internal class WorkingWithDataExample : BaseExample
	{
		[DataContract]
		private class Person : IEquatable<Person>
		{
			[DataMember]
			private int id;

			[DataMember]
			public string FirstName;

			[DataMember]
			public string LastName;

			public void SetId(int id)
			{
				this.id = id;
			}

			public bool Equals(Person other)
			{
				return id.Equals(other.id) &&
					FirstName.Equals(other.FirstName) &&
					LastName.Equals(other.LastName);
			}
		}

		internal void BinarySearchExample()
		{
			Example(() =>
			{
				List<int> ints = new List<int> { 3, -4, 3, 12, 14, 7, -2, 5 };
				ints.Sort();
				int index = ints.BinarySearch(7);

				if (index >= 0)
				{
					Console.WriteLine("Object found at index {0}", index);
				}
				else
				{
					Console.WriteLine("Object is not on the list");
				}
			});
		}

		internal void JsonSerializationExample()
		{
			Example(() =>
			{
				Person person = new Person { FirstName = "Jay", LastName = "Johnson" };
				person.SetId(7);
				string fileName = "Person.json";
				using (Stream stream = new FileStream(fileName, FileMode.Create))
				{
					DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Person));
					serializer.WriteObject(stream, person);
				}

				Person personFromFile;
				using (Stream stream = new FileStream(fileName, FileMode.Open))
				{
					DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Person));
					personFromFile = (Person)serializer.ReadObject(stream);
				}

				if (person.Equals(personFromFile))
				{
					Console.WriteLine("Success");
				}
				else
				{
					Console.WriteLine("Error");
				}
			});
		}
	}
}
