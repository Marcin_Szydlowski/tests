﻿using MC.CertRepetitions.Examples.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace MC.CertRepetitions.Examples
{
	internal class LINQExample : BaseExample
	{
		private class Hometown
		{
			public string City { get; set; }
			public string State { get; set; }
			public string CityCode { get; set; }
		}

		private class Person
		{
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public string Address1 { get; set; }
			public string City { get; set; }
			public string State { get; set; }
			public string Zip { get; set; }
		}

		private class Employee
		{
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public int StateId { get; set; }
		}

		private class Employee2
		{
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public string City { get; set; }
			public string State { get; set; }
		}

		private class State
		{
			public int StateId { get; set; }
			public string StateName { get; set; }
		}

		List<int> ints = new List<int> { 3, -4, 3, 12, 14, 7, -2, 5, 19 };

		private List<Person> people = new List<Person>()
		{
			new Person()
			{
				FirstName = "John",
				LastName = "Smith",
				Address1 = "First St",
				City = "Havertown",
				State = "PA",
				Zip = "19084"
			},
			new Person()
			{
				FirstName = "Jane",
				LastName = "Doe",
				Address1 = "Second St",
				City = "Ewing",
				State = "NJ",
				Zip = "08560"
			},
			new Person()
			{
				FirstName = "Jack",
				LastName = "Jones",
				Address1 = "Third St",
				City = "Ft Washington",
				State = "PA",
				Zip = "19034"
			}
		};

		private List<Employee> employees = new List<Employee>()
		{
			new Employee()
			{
				FirstName = "John",
				LastName = "Smith",
				StateId = 1
			},
			new Employee()
			{
				FirstName = "Jane",
				LastName = "Doe",
				StateId = 2
			},
			new Employee()
			{
				FirstName = "Jack",
				LastName = "Jones",
				StateId = 1
			},
			new Employee()
			{
				FirstName = "Sue",
				LastName = "Smith",
				StateId = 3
			}
		};

		private List<Employee2> employees2 = new List<Employee2>()
		{
			new Employee2()
			{
				FirstName = "John",
				LastName = "Smith",
				City = "Havertown",
				State = "PA"
			},
			new Employee2()
			{
				FirstName = "Jane",
				LastName = "Doe",
				City = "Ewing",
				State = "NJ"
			},
			new Employee2()
			{
				FirstName = "Jack",
				LastName = "Jones",
				City = "Fort Washington",
				State = "PA"
			}
		};

		private List<State> states = new List<State>()
		{
			new State()
			{
				StateId = 1,
				StateName = "PA"
			},
			new State()
			{
				StateId = 2,
				StateName = "NJ"
			}
		};

		List<Hometown> hometowns = new List<Hometown>()
		{
			new Hometown()
			{
				City = "Havertown",
				State = "PA",
				CityCode = "1234"
			},
			new Hometown()
			{
				City = "Ewing",
				State = "NJ",
				CityCode = "5678"
			},
			new Hometown()
			{
				City = "Fort Washington",
				State = "PA",
				CityCode = "9012"
			}
		};

		internal void EvenNumbersExample()
		{
			Example(() =>
			{
				var evenNumbers = (from selected in ints
								   where selected % 2 == 0
								   select selected).ToList();

				evenNumbers.ForEach(n => Console.WriteLine("Even number: {0}", n));
			});
		}

		internal void EvenNumbersDeferredExample()
		{
			Example(() =>
			{
				var evenNumbers = from selected in ints
								  where selected % 2 == 0
								  select selected;

				Console.WriteLine("1st turn");
				foreach (var number in evenNumbers)
				{
					Console.WriteLine("Even number: {0}", number);
				}

				ints[2] = 16;

				Console.WriteLine("2nd turn");
				foreach (var number in evenNumbers)
				{
					Console.WriteLine("Even number: {0}", number);
				}
			});
		}

		internal void ManyWhereInsteadOfAndExample()
		{
			Example(() =>
			{
				var evenNumbers = (from n in ints
								   where n % 2 == 0
								   where n > 5
								   select n).ToList();

				evenNumbers.ForEach(n => Console.WriteLine("Even number: {0}", n));
			});
		}

		internal void MethodInsteadOfExpressionExample()
		{
			Example(() =>
			{
				var evenNumbers = (from n in ints
								   where EvenAndGT5(n)
								   select n).ToList();

				evenNumbers.ForEach(n => Console.WriteLine("Even number: {0}", n));
			});
		}

		internal void OrderbyExample()
		{
			Example(() =>
			{
				List<Hometown> hometowns = new List<Hometown>()
				{
					new Hometown() { City = "Philadelphia", State = "PA" },
					new Hometown() { City = "Ewing", State = "NJ" },
					new Hometown() { City = "Havertown", State = "PA" },
					new Hometown() { City = "Fort Washington", State = "PA" },
					new Hometown() { City = "Trenton", State = "NJ" }
				};

				var orderedHometowns = from h in hometowns
									   orderby h.State ascending, h.City ascending
									   select h;

				foreach (var hometown in orderedHometowns)
				{
					Console.WriteLine("City: {0}, state: {1}", hometown.City, hometown.State);
				}
			});
		}

		internal void ProjectionExample()
		{
			Example(() =>
			{
				List<Hometown> hometowns = new List<Hometown>()
				{
					new Hometown() { City = "Philadelphia", State = "PA" },
					new Hometown() { City = "Ewing", State = "NJ" },
					new Hometown() { City = "Havertown", State = "PA" },
					new Hometown() { City = "Fort Washington", State = "PA" },
					new Hometown() { City = "Trenton", State = "NJ" }
				};

				var orderedHometowns = from h in hometowns
									   select h.City;

				foreach (var hometown in orderedHometowns)
				{
					Console.WriteLine("City: {0}", hometown);
				}
			});
		}

		internal void AnonymousTypeExample()
		{
			Example(() =>
			{
				var names = from p in people
							select new { p.FirstName, p.LastName };

				foreach (var name in names)
				{
					Console.WriteLine("First name: {0}, last name: {1}", name.FirstName, name.LastName);
				}
			});
		}

		internal void AnonymousTypeWithNamedTypesExample()
		{
			Example(() =>
			{
				var names = from p in people
							select new { First = p.FirstName, Last = p.LastName };

				foreach (var name in names)
				{
					Console.WriteLine("First: {0}, last: {1}", name.First, name.Last);
				}
			});
		}

		internal void JoiningExample()
		{
			Example(() =>
			{
				var employeeByState = from e in employees
									  join s in states
									  on e.StateId equals s.StateId
									  select new { e.FirstName, s.StateName };

				foreach (var employee in employeeByState)
				{
					Console.WriteLine("{0} from {1}", employee.FirstName, employee.StateName);
				}
			});
		}

		internal void OuterJoinExample()
		{
			Example(() =>
			{
				var employeeByState = from e in employees
									  join s in states
									  on e.StateId equals s.StateId into employeeGroup
									  from item in employeeGroup.DefaultIfEmpty(new State { StateId = 0, StateName = String.Empty })
									  select new { e.FirstName, item.StateName };

				foreach (var employee in employeeByState)
				{
					Console.WriteLine("{0}, {1}", employee.FirstName, employee.StateName);
				}
			});
		}

		internal void CompositeKeysExample()
		{
			Example(() =>
			{
				var employeeByState = from e in employees2
									  join h in hometowns
									  on new { City = e.City, State = e.State } equals
									  new { City = h.City, State = h.State }
									  select new { e.LastName, h.CityCode };

				foreach (var employee in employeeByState)
				{
					Console.WriteLine("{0}, {1}", employee.LastName, employee.CityCode);
				}
			});
		}

		internal void GroupingExample()
		{
			Example(() =>
			{
				var employeeByState = from e in employees2
									  group e by e.State;

				foreach (var employeeGroup in employeeByState)
				{
					Console.WriteLine("{0}: {1} items", employeeGroup.Key, employeeGroup.Count());
					foreach (var employee in employeeGroup)
					{
						Console.WriteLine("    {0}, {1}", employee.LastName, employee.State);
					}
				}
			});
		}

		internal void GroupingNumbersExample()
		{
			Example(() =>
			{
				List<int> ints = new List<int> { 3, -4, 3, 12, 14, 7, -2, 5 };
				var groupedNumbers = from i in ints
									 group i by (i % 2 == 0 ? "Even" : "Odd");


				foreach (var numberGroup in groupedNumbers)
				{
					Console.WriteLine("{0}: {1} items", numberGroup.Key, numberGroup.Count());
					foreach (var number in numberGroup)
					{
						Console.WriteLine("    {0}", number);
					}
				}
			});
		}

		internal void GroupingNumbersWithNamingExample()
		{
			Example(() =>
			{
				var groupedNumbers = from i in ints
									 group i by (i % 2 == 0 ? "Even" : "Odd") into g
									 select new { Key = g.Key, SumOfNumbers = g.Sum() };

				foreach (var numberGroup in groupedNumbers)
				{
					Console.WriteLine("{0}, sum={1}", numberGroup.Key, numberGroup.SumOfNumbers);
				}
			});
		}

		internal void LinqToXmlExample()
		{
			Example(() =>
			{
				var xmlEmployees = new XElement("Root",
					from e in employees
					select new XElement("Employee",
						new XElement("FirstName", e.FirstName),
						new XElement("LastName", e.LastName)));

				Console.WriteLine(xmlEmployees);
			});
		}

		private bool EvenAndGT5(int n)
		{
			return n % 2 == 0 && n > 5;
		}
	}
}
