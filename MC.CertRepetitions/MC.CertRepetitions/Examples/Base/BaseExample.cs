﻿using MC.CertRepetitions.Observers;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace MC.CertRepetitions.Examples.Base
{
	internal abstract class BaseExample
	{
		protected List<IExampleObserver> observers = new List<IExampleObserver>();

		protected void Example(Action expression, [CallerMemberName]string callerMemberName = "")
		{
			observers.ForEach(o => o.OnNext(callerMemberName));

			try
			{
				expression();
			}
			catch (Exception ex)
			{
				observers.ForEach(o => o.OnError(ex, callerMemberName));
			}
		}

		public void Subscribe(BaseObserver observer)
		{
			observers.Add(observer);
		}
	}
}
