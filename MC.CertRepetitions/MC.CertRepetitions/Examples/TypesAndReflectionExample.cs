﻿using MC.CertRepetitions.Examples.Base;
using MC.CertRepetitions.Observers;
using MC.CertRepetitions.People;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

namespace MC.CertRepetitions.Examples
{
	internal class TypesAndReflectionExample : BaseExample
	{
		internal void LoadAssemblyExample()
		{
			Example(() =>
			{
				Assembly loadedAssembly = Assembly.Load("System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
				Console.WriteLine("CodeBase: {0}", loadedAssembly.CodeBase);
				Console.WriteLine("FullName: {0}", loadedAssembly.FullName);
				Console.WriteLine("GlobalAssemblyCache: {0}", loadedAssembly.GlobalAssemblyCache);
				Console.WriteLine("ImageRuntimeVersion: {0}", loadedAssembly.ImageRuntimeVersion);
				Console.WriteLine("Location: {0}", loadedAssembly.Location);
			});
		}

		internal void GetExecutingAssemblyTypesExample()
		{
			Example(() =>
			{
				Assembly executingAssembly = Assembly.GetExecutingAssembly();
				Array.ForEach(executingAssembly.GetTypes(), t =>
				{
					Console.WriteLine("Type name: {0}", t.Name);
				});
			});
		}

		internal void GetModulesExample()
		{
			Example(() =>
			{
				Assembly loadedAssembly = Assembly.Load("System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
				Module[] modules = loadedAssembly.GetModules();
				Array.ForEach(modules, m =>
				{
					Console.WriteLine("Module's name: \"{0}\"", m.Name);
				});
			});
		}

		internal void AssemblyCreateInstanceExample()
		{
			Example(() =>
			{
				Assembly loadedAssembly = Assembly.Load("System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
				BooleanSwitch booleanSwitch = (BooleanSwitch)loadedAssembly.CreateInstance("System.Diagnostics.BooleanSwitch", false, BindingFlags.CreateInstance, null, new object[] { "Some name", "Some description" }, Thread.CurrentThread.CurrentCulture, null);
				Console.WriteLine("Boolean switch: name=\"{0}\", description=\"{1}\"", booleanSwitch.DisplayName, booleanSwitch.Description);
			});
		}

		internal void ActivatorCreateInstanceExample()
		{
			Example(() =>
			{
				Assembly loadedAssembly = Assembly.Load("System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
				DateTime defaultDate = Activator.CreateInstance<DateTime>();
				DateTime anotherDate = (DateTime)Activator.CreateInstance(typeof(DateTime), new object[] { 2015, 4, 26 });
				Console.WriteLine("Default date: {0}", defaultDate.ToShortDateString());
				Console.WriteLine("Another date: {0}", anotherDate.ToShortDateString());
			});
		}

		internal void GetReferencedAssembliesExample()
		{
			Example(() =>
			{
				Assembly loadedAssembly = Assembly.Load("System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
				AssemblyName[] referencedAssemblyNames = loadedAssembly.GetReferencedAssemblies();
				Array.ForEach(referencedAssemblyNames, r =>
				{
					Console.WriteLine("Assembly name: {0}", r.Name);
					Console.WriteLine("Assembly version: {0}", r.Version);
				});
			});
		}

		internal void GetInt32TypeInfoExample()
		{
			Example(() =>
			{
				DateTime variable = DateTime.Now;
				Type type = variable.GetType();
				Console.WriteLine("AssemblyQualifiedName: {0}", type.AssemblyQualifiedName);
				Console.WriteLine("FullName: {0}", type.FullName);
				Console.WriteLine("IsValueType: {0}", type.IsValueType);
				Console.WriteLine("Name: {0}", type.Name);
				Console.WriteLine("Namespace: {0}", type.Namespace);
			});
		}

		internal void GetArrayRankExample()
		{
			Example(() =>
			{
				int[, ,] intCube = new int[5, 5, 5];
				var cubeType = intCube.GetType();
				Console.WriteLine("Array rank: {0}", cubeType.GetArrayRank());
			});
		}

		internal void GetConstructorsExample()
		{
			Example(() =>
			{
				Manager manager = new Manager("Jason");
				ConstructorInfo[] constructors = manager.GetType().GetConstructors();
				for (int i = 0; i < constructors.Length; i++)
				{
					ConstructorInfo c = constructors[i];
					Console.WriteLine("Constructor #{0}", i + 1);
					ParameterInfo[] parameters = c.GetParameters();
					Array.ForEach(parameters, p =>
					{
						Console.WriteLine("    Parameter: name=\"{0}\", type=\"{1}\"", p.Name, p.ParameterType.Name);
					});
				}
			});
		}		
	}
}
