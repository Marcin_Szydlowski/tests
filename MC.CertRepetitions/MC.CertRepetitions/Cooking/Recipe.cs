﻿
using System;
namespace MC.CertRepetitions.Cooking
{
	public class Recipe : IComparable<Recipe>
	{
		public string MainIngreidient { get; set; }
		public TimeSpan TotalTime { get; set; }
		public decimal CostPerPerson { get; set; }

		public int CompareTo(Recipe other)
		{
			return MainIngreidient.CompareTo(other.MainIngreidient);
		}
	}
}
