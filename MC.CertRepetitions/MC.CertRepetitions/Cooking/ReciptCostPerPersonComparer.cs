﻿using System.Collections.Generic;

namespace MC.CertRepetitions.Cooking
{
	class ReciptCostPerPersonComparer : IComparer<Recipe>
	{
		public int Compare(Recipe x, Recipe y)
		{
			return x.CostPerPerson.CompareTo(y.CostPerPerson);
		}
	}
}
