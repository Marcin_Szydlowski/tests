﻿using System.Collections.Generic;

namespace MC.CertRepetitions.Cooking
{
	public class ReciptMainIngredientComparer : IComparer<Recipe>
	{
		public int Compare(Recipe x, Recipe y)
		{
			return x.MainIngreidient.CompareTo(y.MainIngreidient);
		}
	}
}
