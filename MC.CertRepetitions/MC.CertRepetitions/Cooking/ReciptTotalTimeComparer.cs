﻿using System.Collections.Generic;

namespace MC.CertRepetitions.Cooking
{
	class ReciptTotalTimeComparer : IComparer<Recipe>
	{
		public int Compare(Recipe x, Recipe y)
		{
			return x.TotalTime.CompareTo(y.TotalTime);
		}
	}
}
