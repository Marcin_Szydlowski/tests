﻿using System;

namespace MC.CertRepetitions.Observers
{
	internal interface IExampleObserver
	{
		string Name { get; set; }
		void OnCompleted(string callerMemberName = "");
		void OnError(Exception error, string callerMemberName = "");
		void OnNext(string callerMemberName = "");
	}
}
