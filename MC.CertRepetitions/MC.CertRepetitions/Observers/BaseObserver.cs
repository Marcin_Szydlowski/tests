﻿using System;

namespace MC.CertRepetitions.Observers
{
	internal class BaseObserver : IExampleObserver
	{
		private string name = "Default observer's name";

		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				if (String.IsNullOrEmpty(value))
				{
					throw new ArgumentNullException("value");
				}
			}
		}

		public BaseObserver(string name)
		{
			if (String.IsNullOrEmpty(name))
			{
				throw new ArgumentNullException("name");
			}

			this.name = name;
		}

		public void OnCompleted(string callerMemberName = "")
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Observation in \"{0}\" observer finished and called by method \"{1}\"", name, callerMemberName);
			Console.ResetColor();
		}

		public void OnError(Exception error, string callerMemberName = "")
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("Observer \"{0}\", error called by method \"{1}\", error: {2}", name, callerMemberName, error);
			Console.ResetColor();
		}

		public void OnNext(string callerMemberName = "")
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("Observer \"{0}\", next step called by method \"{1}\"", name, callerMemberName);
			Console.ResetColor();
		}
	}
}
