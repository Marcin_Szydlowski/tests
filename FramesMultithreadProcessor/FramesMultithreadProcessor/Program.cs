﻿using FramesMultithreadProcessor.Data;
using FramesMultithreadProcessor.Threads;
using System;
using System.IO;
using System.Threading.Tasks;

namespace FramesMultithreadProcessor
{
	class Program
	{
		private struct Range
		{
			public uint Start;
			public uint Length;

			public Range(uint start, uint length)
			{
				Start = start;
				Length = length;
			}
		}

		static readonly byte framesCount = 20;
		static readonly byte threadsCount = 8;

		static void Main(string[] args)
		{
			FramesCollection dataCollection = new FramesCollection(framesCount);

			// Process data by n threads
			byte[] allData = ProcessFramesParallelly(dataCollection, threadsCount);

			// Check
			CheckDataAfterProcessing(dataCollection, allData);

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		private static byte[] ProcessFramesParallelly(FramesCollection framesCollection, byte tasksCount)
		{
			if (framesCollection.Length < tasksCount)
			{
				tasksCount = (byte)framesCollection.Length;
			}

			Range[] rangesForProcessing = PrepareRangesForProcessing(framesCollection, tasksCount);

			Task[] tasks = new Task[tasksCount];
			byte[] allData;

			using (var memoryStream = new MemoryStream())
			{
				ProcessedFramesWriter processedFramesWriter = new ProcessedFramesWriter(memoryStream);

				for (int i = 0; i < tasksCount; i++)
				{
					Range range = rangesForProcessing[i];
					tasks[i] = Task.Factory.StartNew(() => ProcessFrame(range, framesCollection, processedFramesWriter));
				}

				Task.WaitAll(tasks);
				allData = memoryStream.ToArray();
			}

			return allData;
		}

		private static Range[] PrepareRangesForProcessing(FramesCollection framesCollection, byte tasksCount)
		{
			Range[] rangesToProcess = new Range[tasksCount];
			uint rangeLength = (uint)Math.Floor((float)framesCollection.Length / tasksCount);
			for (int i = 0; i < tasksCount; i++)
			{
				rangesToProcess[i] = new Range((uint)(i * rangeLength), i < tasksCount - 1 ? rangeLength : (uint)(framesCollection.Length - rangeLength * i));
			}

			return rangesToProcess;
		}

		private static void ProcessFrame(Range range, FramesCollection framesCollection, ProcessedFramesWriter processedFramesWriter)
		{
			for (int j = 0; j < range.Length; j++)
			{
				long frameNumber = range.Start + j;

				byte[] processedFrameData = FrameProcessor.ProcessFrame(framesCollection[frameNumber], frameNumber);
				processedFramesWriter.WriteFrame(processedFrameData, frameNumber);
			}
		}

		private static void CheckDataAfterProcessing(FramesCollection dataCollection, byte[] allData)
		{
			for (long i = 0, start = 1; i < dataCollection.Length; i++, start += Frame.FrameLength + 3)
			{
				if (allData[start] != i)
				{
					throw new InvalidOperationException("Data aren't match");
				}
			}
		}
	}
}