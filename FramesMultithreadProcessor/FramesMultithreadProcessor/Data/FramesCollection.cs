﻿namespace FramesMultithreadProcessor.Data
{
	public class FramesCollection
	{
		private Frame[] frames;

		public int Length
		{
			get
			{
				return frames.Length;
			}
		}

		public FramesCollection(byte framesCount)
		{
			frames = new Frame[framesCount];
			for (int i = 0; i < framesCount; i++)
			{
				frames[i] = new Frame();
			}
		}

		public Frame this[long i]
		{
			get
			{
				return frames[i];
			}
			set
			{
				frames[i] = value;
			}
		}
	}
}