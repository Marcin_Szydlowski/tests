﻿using System;

namespace FramesMultithreadProcessor.Data
{
	public class Frame
	{
		public static readonly uint FrameLength = 1000000;
		static readonly Random randomizer = new Random(DateTime.Now.Millisecond);
		public byte[] Data = new byte[FrameLength];

		public Frame()
		{
			randomizer.NextBytes(Data);
		}

		public Frame(byte[] data)
		{
			Data = data;
		}
	}
}