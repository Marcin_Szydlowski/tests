﻿using System;
using System.Collections.Concurrent;

namespace FramesMultithreadProcessor.Data
{
	public static class QuantizationManager
	{
		static readonly ushort dataMaxValue = 256;
		static readonly byte quantsCount = 8;
		public static readonly byte[] Quants;
		static ConcurrentDictionary<byte, byte> closestsQuants = new ConcurrentDictionary<byte, byte>();

		static QuantizationManager()
		{
			Quants = new byte[quantsCount];
			int step = dataMaxValue / quantsCount;
			for (int i = 0; i < quantsCount; i++)
			{
				Quants[i] = (byte)(step * i - 1);
			}
		}

		public static void Reset()
		{
			closestsQuants.Clear();
		}

		public static Frame QuantizeFrame(Frame frame)
		{
			int dataLength = frame.Data.Length;
			byte[] quantizedFrameData = new byte[dataLength];

			for (int i = 0; i < dataLength; i++)
			{
				byte frameData = frame.Data[i];
				byte closestQuant = 0;
				if (!closestsQuants.TryGetValue(frameData, out closestQuant))
				{
					closestQuant = GetNearestQuant(frameData);
					closestsQuants.TryAdd(frameData, closestQuant);
				}

				quantizedFrameData[i] = closestQuant;
			}

			return new Frame(quantizedFrameData);
		}

		private static byte GetNearestQuant(byte v)
		{
			int minDistance = int.MaxValue;
			byte minValue = Quants[0];
			foreach (var quant in Quants)
			{
				if (quant == v)
				{
					minValue = quant;
					break;
				}

				byte distance = (byte)Math.Abs(v - quant);
				if (distance < minDistance)
				{
					minDistance = distance;
					minValue = quant;
				}
			}

			return minValue;
		}
	}
}