﻿using System;
using System.Collections.Concurrent;
using System.IO;

namespace FramesMultithreadProcessor.Threads
{
	public class ProcessedFramesWriter
	{
		Stream stream;
		byte nextFrameToSaveNumber = 0;
		ConcurrentDictionary<long, byte[]> framesDataToSave = new ConcurrentDictionary<long, byte[]>();

		public ProcessedFramesWriter(Stream stream)
		{
			this.stream = stream;
		}

		public void WriteFrame(byte[] frameData, long orderNumber)
		{
			Console.WriteLine($"{orderNumber} frame's data to save");
			framesDataToSave.TryAdd(orderNumber, frameData);

			lock (framesDataToSave)
			{
				if (orderNumber == nextFrameToSaveNumber)
				{
					byte[] frameDataToWrite;
					while (framesDataToSave.TryRemove(nextFrameToSaveNumber, out frameDataToWrite))
					{
						Console.WriteLine($" -> saving {nextFrameToSaveNumber} ...");

						stream.Write(frameDataToWrite, 0, frameDataToWrite.Length);
						nextFrameToSaveNumber++;
					}
				}
			}
		}
	}
}