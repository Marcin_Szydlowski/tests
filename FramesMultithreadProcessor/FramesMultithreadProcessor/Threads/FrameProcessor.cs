﻿using FramesMultithreadProcessor.Data;
using System;

namespace FramesMultithreadProcessor.Threads
{
	public static class FrameProcessor
	{
		public static byte[] ProcessFrame(Frame frame, long orderNumber)
		{
			byte[] processedFrameData = new byte[Frame.FrameLength + 3];
			processedFrameData[0] = 0;
			processedFrameData[1] = (byte)orderNumber;
			processedFrameData[2] = 0;

			Frame quantizedFrame = QuantizationManager.QuantizeFrame(frame);
			Array.Copy(quantizedFrame.Data, 0, processedFrameData, 3, Frame.FrameLength);

			return processedFrameData;
		}
	}
}