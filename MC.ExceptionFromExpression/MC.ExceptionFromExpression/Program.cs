﻿using System;
using System.Diagnostics;

namespace MC.ExceptionFromExpression
{
	class Program
	{
		private static readonly int TestsCount = 100000;

		private static Stopwatch stopwatch = new Stopwatch();

		static void Main(string[] args)
		{
			CheckNullByConditionals();
			CheckNullByExtensionMethods();
			CheckNullByExpressions(3);

			Contract.NotNull(() => args);

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		static void CheckNullByConditionals()
		{
			stopwatch.Reset();
			stopwatch.Start();
			object obj = 'a';

			for (int i = 0; i < TestsCount; i++)
			{
				if (obj == null)
				{
					throw new ArgumentNullException($"\"{nameof(obj)}\" is null");
				}
			}

			stopwatch.Stop();
			Console.WriteLine(stopwatch.Elapsed.ToString());
		}

		static void CheckNullByExtensionMethods()
		{
			stopwatch.Reset();
			stopwatch.Start();
			object obj = 'a';

			for (int i = 0; i < TestsCount; i++)
			{
				obj.NotNull(nameof(obj));
			}

			stopwatch.Stop();
			Console.WriteLine(stopwatch.Elapsed.ToString());
		}

		static void CheckNullByExpressions(int a)
		{
			stopwatch.Reset();
			stopwatch.Start();
			object obj = null;

			for (int i = 0; i < TestsCount; i++)
			{
				Contract.NotNull(() => obj);
			}

			stopwatch.Stop();
			Console.WriteLine(stopwatch.Elapsed.ToString());
		}
	}
}