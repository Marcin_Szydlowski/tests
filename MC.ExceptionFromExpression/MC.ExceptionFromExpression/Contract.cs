﻿using System;
using System.Linq.Expressions;

namespace MC.ExceptionFromExpression
{
	public static class Contract
	{
		internal static void NotNull(Expression<Func<object>> argument)
		{
			var memberSelector = (MemberExpression)argument.Body;
			var constantSelector = (ConstantExpression)memberSelector.Expression;
			//object value = ((FieldInfo)memberSelector.Member).GetValue(constantSelector.Value);

			if (constantSelector.Value == null)
			{
				throw new ArgumentNullException($"\"{memberSelector.Member.Name}\" is null");
			}
		}

		internal static void NotNull(this object obj, string name)
		{
			if (obj == null)
			{
				throw new ArgumentNullException($"\"{name}\" is null");
			}
		}
	}
}