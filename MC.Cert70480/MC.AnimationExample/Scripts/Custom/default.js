﻿/// <reference path='../jquery-2.1.4.min.js' />

$(function () {
	var miliseconds = 500;
	var opacity = 0.5;

	function displayCoverAsync() {
		return $('#cover').fadeTo(miliseconds, opacity).promise();
	}

	function showMessageContentAsync(message) {
		$('#message').html(message);
		$('#messageBox').show();
		return $('#messageContent').slideDown(miliseconds).promise();
	}

	function showMessageAsync(message) {
		var coverPromise = displayCoverAsync();
		var messagePromise = coverPromise.pipe(function () {
			return showMessageContentAsync(message);
		});

		return messagePromise;
	}

	function displayTimeAsync() {
		var message = 'The time is now ' + getTime();
		return showMessageAsync(message);
	}

	function getTime() {
		var dateTime = new Date();
		var hours = dateTime.getHours();
		var minutes = dateTime.getMinutes();
		return hours + ':' + (minutes < 10 ? '0' + minutes : minutes);
	}

	function hideMessageContentAsync() {
		var promise = $('#messageContent').slideUp(miliseconds).promise();
		promise.done(function () {
			$('#messageBox').hide();
		});

		return promise;
	}

	function hideCoverAsync() {
		return $('#cover').fadeOut(miliseconds).promise();
	}

	function hideMessageAsync() {
		var messagePromise = hideMessageContentAsync();
		var coverPromise = messagePromise.pipe(function () {
			return hideCoverAsync();
		});

		return coverPromise;
	}

	init();

	function init() {
		setEvents();
	}

	function setEvents() {
		$('#btnShowMessage').click(displayTimeAsync);
		$('#messageOk').click(hideMessageAsync);
	}
});
