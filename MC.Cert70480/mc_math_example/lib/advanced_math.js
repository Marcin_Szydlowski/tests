﻿var call_counter = require('./call_counter.js');

function mul(x, y) {
	call_counter();
	return x * y;
}

function div(x, y) {
	call_counter();
	
	if (y == 0) {
		throw 'y equals zero';
	}
	
	return x / y;
}

function fibo(n) {
	call_counter();
	return private_fibo(n);
}

function private_fibo(n) {
	if (n == undefined) {
		throw 'n is undefined';
	}
	
	if (n < 0) {
		throw 'n is less than zero';
	}
	
	var result = 0;
	
	if (n < 2) {
		return n;
	}
	else {
		var first = 1;
		var second = 2;
		
		for (var c = 2; c <= n; c++) {
			result = first + second;
			first = second;
			second = result;
		}
	}
	
	return result;
}

module.exports = {
	multiplication: mul,
	division: div,
	fibonacci: fibo
}