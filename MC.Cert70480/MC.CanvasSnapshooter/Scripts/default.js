﻿/// <reference path="jquery-2.1.3.min.js" />

$(function () {
	init();

	function init() {
		setEvents();
	}

	function setEvents() {
		$('#btnSnapshot').on('click', drawVideoFrame);
	}

	function drawVideoFrame() {
		var canvas = $('#myCanvas');
		var video = $('#myVideo');
		canvas[0].getContext('2d').drawImage(video[0], 0, 0, 360, 240);
	}
});