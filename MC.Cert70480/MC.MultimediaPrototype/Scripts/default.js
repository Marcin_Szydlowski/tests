﻿/// <reference path="jquery-2.1.3.min.js" />

$(function () {
	init();

	function init() {
		setEvents();
	}

	function setEvents() {
		$('#media').on('play', function () {
			$('#message').html($('#media')[0].currentSrc);
		});
	}
});