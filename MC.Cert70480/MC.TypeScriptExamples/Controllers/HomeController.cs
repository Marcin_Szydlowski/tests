﻿using System.Web.Mvc;

namespace MC.TypeScriptExamples.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

	}
}
