﻿module Geometry {
	export class Point {
		constructor(public x: number = 0, public y: number = 0) { }

		toString() {
			return '(' + this.x + ', ' + this.y + ')';
		}
	}
}
