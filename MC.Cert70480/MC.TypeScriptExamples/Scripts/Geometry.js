var Geometry;
(function (Geometry) {
    var Point = (function () {
        function Point(x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            this.x = x;
            this.y = y;
        }
        Point.prototype.toString = function () {
            return '(' + this.x + ', ' + this.y + ')';
        };
        return Point;
    })();
    Geometry.Point = Point;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=Geometry.js.map