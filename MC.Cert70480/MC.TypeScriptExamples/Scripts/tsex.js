﻿var Geometry;
(function (Geometry) {
    var Point = (function () {
        function Point(x, y) {
            if (typeof x === "undefined") { x = 0; }
            if (typeof y === "undefined") { y = 0; }
            this.x = x;
            this.y = y;
        }
        Point.prototype.toString = function () {
            return '(' + this.x + ', ' + this.y + ')';
        };
        return Point;
    })();
    Geometry.Point = Point;
})(Geometry || (Geometry = {}));
var point = new Geometry.Point();

console.info(point.toString());
