﻿/// <reference path="_references.js" />

(function () {
	this.calcApp = this.calcApp || {};
	var ns = this.calcApp;

	ns.initialize = function () {
		var calculator = new ns.Calculator();

		$('button[id^="btnNumber"]').on('click', calculator.numberClick);
		$('#btnPlus').on('click', calculator.plusClick);
		$('#btnMinus').on('click', calculator.minusClick);
		$('#btnMul').on('click', calculator.mulClick);
		$('#btnDiv').on('click', calculator.divClick);
		$('#btnClearEntry').on('click', calculator.clearEntry);
		$('#btnClear').on('click', calculator.clear);
		calculator.clear();
	}

	ns.Calculator = (function () {
		function Calculator() {

		}

		Calculator.prototype.numberClick = function () {
			var txtInput = $('#txtInput');
			txtInput.val(txtInput.val() == '0' ? $(this).text() : txtInput.val() + $(this).text());
		}

		Calculator.prototype.plusClick = function () {
			var txtResult = $('#txtResult');
			txtResult.val(Number(txtResult.val()) + Number(txtInput.value));
			Calculator.prototype.clearEntry();
		}

		Calculator.prototype.minusClick = function () {
			var txtResult = $('#txtResult');
			txtResult.val(Number(txtResult.val()) - Number(txtInput.value));
			Calculator.prototype.clearEntry();
		}

		Calculator.prototype.mulClick = function () {
			var txtResult = $('#txtResult');
			txtResult.val(Number(txtResult.val()) * Number(txtInput.value));
			Calculator.prototype.clearEntry();
		}

		Calculator.prototype.divClick = function () {
			var txtResult = $('#txtResult');
			txtResult.val(Number(txtResult.val()) / Number(txtInput.value));
			Calculator.prototype.clearEntry();
		}

		Calculator.prototype.clearEntry = function () {
			$('#txtInput').val('0');
		}

		Calculator.prototype.clear = function () {
			$('#txtInput').val('0');
			$('#txtResult').val('0');
		}

		return Calculator;
	}());
})();