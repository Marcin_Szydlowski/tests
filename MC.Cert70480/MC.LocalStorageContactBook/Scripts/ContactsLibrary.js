﻿/// <reference path="jquery-2.1.4.min.js" />

$(function () {
	init();

	function init() {
		contactsNamespace.initialize();
	}

});

(function () {
	this.contactsNamespace = this.contactsNamespace || {};
	var ns = this.contactsNamespace;
	var currentRecord;
	var db;

	ns.initialize = function () {
		prepareDB();

		$('#btnSave').on('click', ns.save);
		var request = indexedDB.open('Chapter 16', 1);

		request.onupgradeneeded = function (response) {
			var options = { keypath: 'id', autoIncrement: true };
			response.currentTarget.result.createObjectStore('contacts', options);
		};

		request.onsuccess = function (response) {
			db = request.result;
			ns.display();
		}
	};

	ns.display = function () {
		$('#currentAction').html('Add Contact');
		currentRecord = { key: null, contact: {} };
		displayCurrentRecord();

		var trans = db.transaction('contacts', 'readonly');
		var request = trans.objectStore('contacts').openCursor();
		var results = [];

		request.onsuccess = function (response) {
			var cursor = response.target.result;

			if (!cursor) {
				bindToGrid(results);

				return;
			}

			results.push({ key: cursor.key, contact: cursor.value });
			cursor.continue();
		};
	};

	ns.save = function () {
		var contact = currentRecord.contact;
		contact.firstName = $('#firstName').val();
		contact.lastName = $('#lastName').val();
		contact.email = $('#email').val();
		contact.phoneNumber = $('#phoneNumber').val();

		var trans = db.transaction('contacts', 'readwrite');
		var contacts = trans.objectStore('contacts');
		var request = currentRecord.key != null
						? contacts.put(contact, currentRecord.key)
						: contacts.add(contact);
		request.onsuccess = function (response) {
			ns.display();
		};
	};

	ns.loadContact = function () {
		var key = parseInt($(this).data('key'));
		var trans = db.transaction('contacts', 'readonly');
		var store = trans.objectStore('contacts');
		var request = store.get(key);

		request.onsuccess = function (response) {
			$('#currentAction').html('Edit Contact');
			currentRecord = { key: key, contact: response.target.result };
			displayCurrentRecord();
		}
	};

	function prepareDB() {
		window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
		window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
		window.IDBCursor = window.IDBCursor || window.webkitIDBCursor;
	};

	function displayCurrentRecord() {
		var contact = currentRecord.contact;
		$('#firstName').val(contact.firstName);
		$('#lastName').val(contact.lastName);
		$('#email').val(contact.email);
		$('#phoneNumber').val(contact.phoneNumber);
	}

	function bindToGrid(contacts) {
		var html = '';

		for (var c = 0; c < contacts.length; c++) {
			var key = contacts[c].key;
			var contact = contacts[c].contact;
			html += '<tr><td>' + contact.email + '</td>';
			html += '<td>' + contact.firstName + ' ' + contact.lastName + '</td>';
			html += '<td><a class="edit" href="javascript:void(0)" data-key=' + key + '>Edit</a></td></tr>';
		}

		html = html || '<tr><td colspan="3">No records available</td></tr>';
		$('#contacts tbody').html(html);
		$('#contacts a.edit').on('click', ns.loadContact);
	}
})();