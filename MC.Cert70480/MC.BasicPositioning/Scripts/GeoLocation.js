﻿/// <reference path="jquery-2.1.4.min.js" />

$(function () {
	var watchId = 0;
	var lastCoords;

	init();

	function init() {
		getLocation();
		setEvents();
	}

	function getLocation() {
		if (supportsGeolocation) {
			var options = {
				enableHighAccuracy: true
			};

			watchId = navigator.geolocation.getCurrentPosition(showPosition, showError, options);
		}
		else {
			showMessage('Geolocation is not supported by this browser.');
		}
	}

	function setEvents() {
		$('#startMonitoring').on('click', getLocation);
		$('#stopMonitoring').on('click', endWatch)
	}

	function supportsGeolocation() {
		return 'geolocation' in navigator;
	}

	function showPosition(position) {
		var dateTime = new Date(position.timestamp).toLocaleString();
		var distance = 0;

		if (lastCoords) {
			distance = getDistance(lastCoords.lat, lastCoords.lon, position.coords.latitude, position.coords.longitude);
		}

		showMessage('Latitude: ' + position.coords.latitude + '<br />' +
		'Longitude: ' + position.coords.longitude + '<br />' +
		'Timestamp: ' + dateTime + '<br />' + (distance ? ('Position changed by: ' + distance + ' miles') : ''));

		lastCoords =
			{
				lat: position.coords.latitude,
				lon: position.coords.longitude
			};
	}

	function showMessage(message) {
		$('#message').html(message);
	}

	function showError(error) {
		switch (error.code) {
			case error.PERMISSION_DENIED:
				showMessage('User denied Geolocation access request.');
				break;

			case error.POSITION_UNAVAILABLE:
				showMessage('Location information unavailable.');
				break;

			case error.TIMEOUT:
				showMessage('Get user location request timed out.');
				break;

			case error.UNKNOWN_ERROR:
				showMessage('An unknown error occured.');
				break;
		}
	}

	function endWatch() {
		if (watchId != 0) {
			navigator.geolocation.clearWatch(watchId);
			watchId = 0;
			showMessage('Monitoring ended.');
		}
	}

	function getDistance(lat1, lon1, lat2, lon2) {
		var earthRadius = 3959;		//miles
		var latRadians = getRadians(lat2 - lat1);
		var lonRadians = getRadians(lon2 - lon1);
		var sinLat = Math.sin(latRadians / 2);
		var sinLon = Math.sin(lonRadians / 2);
		var a = sinLat * sinLat +
				Math.cos(getRadians(lat1)) * Math.cos(getRadians(lat2)) *
				sinLon * sinLon;
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var distance = earthRadius * c;

		return distance;
	}

	function getRadians(value) {
		return value * Math.PI / 180;
	}
});