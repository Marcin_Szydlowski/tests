﻿var math_example = require('mc_math_example');
var result = 0;

console.log();
result = math_example.addition(5, 10);
console.log('addition(5,10) = ' + result);

console.log();
result = math_example.substraction(5, 10);
console.log('substraction(5,10) = ' + result);

console.log();
result = math_example.multiplication(5, 10);
console.log('multiplication(5,10) = ' + result);

console.log();
result = math_example.division(5, 10);
console.log('division(5,10) = ' + result);

console.log();
result = math_example.fibonacci(5);
console.log('fibonacci(5) = ' + result);

console.log('done');