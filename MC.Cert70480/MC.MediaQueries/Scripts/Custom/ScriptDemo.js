﻿$(function () {
	var sampleArray = [];

	init();

	function init() {
		try {
			var number = 9;
			if (number == 0) {
				throw new Error('Number is 0');
			}
			else if (number < 10) {
				throw {
					name: 'Less than 10 error',
					message: 'Value is still too small',
					description: 'A custom error',
					number: 700
				}
			}
			allert('Hello!');
		}
		catch (err) {
			alert(err.message + '\n' + err.name + '\n' + err.description + '\n' + err.number);
		}
		finally {
			sampleArray = new Array();
		}

		sampleArray.push(3);
		sampleArray.push(4);
		var result = '';

		for (var i in sampleArray) {
			result += sampleArray[i].toString() + ', ';
		}

		alert(result);
		result = '';

		$.each(sampleArray, function (index, value) {
			result += index + ':' + value + '\n';
		});

		alert(result);
		result = '';

		var object = {
			'key1': 'value1',
			'key2': 'value2',
			'key3': 'value4'
		};
		
		$.each(object, function (key, value) {
			result += key + ':' + value + '\n';
		});

		alert(result);
		result = '';

		for (var i = 0; i < sampleArray.length; i++) {
			result += i + ':' + sampleArray[i] + '\n';
		}

		alert(result);
		result = '';
	}
});