﻿$(function () {
	var OperationKind = {
		ADDITION: {},
		SUBSTRACTION: {},
		MULTIPLICATION: {},
		DIVISION: {}
	};
	
	init();
	
	function init() {
		setEvents();
	}
	
	function setEvents() {
		$('#btnAdd').on('click', addNumbers);
		$('#btnSub').on('click', subNumbers);
		$('#btnMul').on('click', mulNumbers);
		$('#btnDiv').on('click', divNumbers);
	}
	
	function addNumbers(){
		processNumbers({ kind: OperationKind.ADDITION });
	}
	
	function subNumbers(){
		processNumbers({ kind: OperationKind.SUBSTRACTION });
	}
	
	function mulNumbers() {
		processNumbers({ kind: OperationKind.MULTIPLICATION });
	}
	
	function divNumbers() {
		processNumbers({ kind: OperationKind.DIVISION });
	}
	
	function processNumbers(operator) {
		var data = getFormData();
		serverProcessNumbers(data, operator)
		.done(displayResult)
		.fail(displayError);
	}
	
	function getFormData() {
		var x = $('#x').val();
		var y = $('#y').val();
		return { 'x': x, 'y': y };
	}
	
	function serverProcessNumbers(data, operator) {
		var methodName;
		
		if (operator.kind == OperationKind.ADDITION) {
			methodName = 'addition';
		} else if (operator.kind == OperationKind.SUBSTRACTION) {
			methodName = 'substraction';
		} else if (operator.kind == OperationKind.MULTIPLICATION) {
			methodName = 'multiplication';
		} else if (operator.kind == OperationKind.DIVISION) {
			methodName = 'division';
		}
		
		return $.getJSON('/' + methodName, data);
	}
	
	function displayResult(serverData) {
		$('#result').html(serverData.result);
	}
	
	function displayError(serverData, error) {
		var value = 'No result';
		
		if ('result' in serverData) {
			value = serverData.result;
		}
		
		$('#result').html(value + ' - ' + error);
	}
});