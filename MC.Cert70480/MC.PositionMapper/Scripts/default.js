﻿/// <reference path="jquery-2.1.4.min.js" />

$(function () {
	var watchId = 0;

	init();

	function init() {
		getLocation();
	}

	function getLocation() {
		if (supportsGeolocation) {
			watchId = navigator.geolocation.getCurrentPosition(showPosition, showError);
		}
		else {
			showMessage('Geolocation is not supported by this browser.');
		}
	}

	function supportsGeolocation() {
		return 'geolocation' in navigator;
	}

	function showPosition(position) {
		var mapCanvas = $('#map');
		var coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		var options = {
			zoom: 13,
			center: coords,
			mapTypeControl: false,
			navigationControlOptions: {
				style: google.maps.NavigationControlStyle.SMALL
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		var map = new google.maps.Map(mapCanvas[0], options);
		var marker = new google.maps.Marker({
			position: coords,
			map: map,
			title: 'You are here!'
		});
	}

	function showMessage(message) {
		$('#message').html(message);
	}

	function showError(error) {
		switch (error.code) {
			case error.PERMISSION_DENIED:
				showMessage('User denied Geolocation access request.');
				break;

			case error.POSITION_UNAVAILABLE:
				showMessage('Location information unavailable.');
				break;

			case error.TIMEOUT:
				showMessage('Get user location request timed out.');
				break;

			case error.UNKNOWN_ERROR:
				showMessage('An unknown error occured.');
				break;
		}
	}
});