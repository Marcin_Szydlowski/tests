﻿/// <reference path="jquery-2.1.3.min.js" />

$(function () {
	var draggingClassName = 'dragging';
	var draggedItem;

	init();

	function init() {
		setEvents();
	}

	function setEvents() {
		$('.item').on('dragstart', dragging);
		$('.item').on('dragend', draggingEnded);
		$('.hole').on('dragenter', preventDefault);
		$('.hole').on('dragover', preventDefault);
		$('.hole').on('drop', dropItem);
	}

	function dragging(e) {
		draggedItem = $(e.target);
		draggedItem.addClass(draggingClassName);
	}

	function draggingEnded(e) {
		$(e.target).removeClass(draggingClassName);
	}

	function preventDefault(e) {
		e.preventDefault();
	}

	function dropItem(e) {
		var hole = $(e.target);

		if (hole.hasClass('hole') && hole.children().length == 0) {
			draggedItem.detach();
			draggedItem.appendTo(hole);
		}
	}
});