﻿/// <reference path="jquery-2.1.3.min.js" />

$(function () {
	init();

	function init() {
		setEvents();
	}

	function setEvents() {
		jQuery.event.props.push('dataTransfer');
		$('#carList').on('dragstart', dragging);
		$('#favoriteCars').on('dragenter', preventDefault);
		$('#favoriteCars').on('dragover', preventDefault);
		$('#favoriteCars').on('drop', dropItem);
	}

	function preventDefault(e) {
		e.preventDefault();
	}

	function dragging(e) {
		var value = $(e.target).data('value');
		e.dataTransfer.setData('text', value);
		e.dataTransfer.effectAllowed = 'copy';
	}

	function dropItem(e) {
		var data = e.dataTransfer.getData('text').split(',');

		if (data[0] == 'car') {
			var li = document.createElement('li');
			li.textContent = data[1];
			e.target.appendChild(li);
		}
	}
});