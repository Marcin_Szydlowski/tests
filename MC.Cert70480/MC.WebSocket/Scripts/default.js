﻿/// <reference path="jquery-2.1.4.min.js" />

$(function () {
	var wsUri = 'ws://echo.websocket.org/';
	var webSocket;
	var timerId = 0;

	init();

	function init() {
		if (checkSupported()) {
			connect();
			$('#btnSend').click(doSend);
		}
	}

	function writeOutput(message) {
		var output = $('#divOutput');
		output.html(output.html() + '<br />' + message);
	}

	function checkSupported() {
		if (window.WebSocket) {
			writeOutput('WebSockets supported!');
			return true;
		} else {
			writeOutput('WebSockets NOT supported');
			$('#btnSend').attr('disabled', 'disabled');
			return false;
		}
	}

	function connect() {
		webSocket = new WebSocket(wsUri);
		webSocket.onopen = function (e) { onOpen(e) };
		webSocket.onclose = function (e) { onClose(e) };
		webSocket.onmessage = function (e) { onMessage(e) };
		webSocket.onerror = function (e) { onError(e) };
	}

	function doSend() {
		var message = $('#txtMessage').val();

		if (webSocket.readyState != webSocket.OPEN) {
			writeOutput('NOT OPEN: ' + message);
			return;
		}

		writeOutput('SENT: ' + message);
		webSocket.send(message);
	}

	function onOpen(e) {
		writeOutput('CONNECTED');
		keepAlive();
	}

	function onClose(e) {
		cancelKeepAlive();
		writeOutput('DISCONNECTED');
	}

	function onMessage(e) {
		writeOutput('RESPONSE: ' + e.data);
	}

	function onError(e) {
		writeOutput('ERROR: ' + e.data);
	}

	function keepAlive() {
		var timeout = 5000;

		if (webSocket.readyState == webSocket.OPEN) {
			webSocket.send('');
		}

		timerId = setTimeout(keepAlive, timeout);
	}

	function cancelKeepAlive() {
		if (timerId) {
			cancelTimeout(timerId);
		}
	}
});