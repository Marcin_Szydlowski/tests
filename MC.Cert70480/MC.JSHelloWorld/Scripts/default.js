﻿var greeting = 'Hello World';

function areaOfPizzaSlice(diameter, slicesPerPizza) {
	return areaOfPizza(diameter) / slicesPerPizza;

	function areaOfPizza(diameter) {
		var radius = diameter / 2;
		return Math.PI * radius * radius;
	}
}