﻿/// <reference path="jquery-2.1.4.min.js" />

$(function () {
	init();

	function init() {
		//setCookie('firstName', 'somename', 1);
		//var firstName = getCookie('firstName');
		//alert(firstName);

		//$.cookie('lastName', 'somelastname');
		//alert($.cookie('lastName'));

		if (isWebStorageSupported) {
			if (localStorage['thirdName']) {
				alert('third name already exist');
				alert(localStorage['person']);
			}
			else {
				localStorage.setItem('thirdName', 'another name');
				var person = { firstName: 'Marcelina', lastName: 'Winterhand' };
				localStorage['person'] = JSON.stringify(person);				
			}
		}
		else {
			alert("Your browser doesn't support local storage.");
		}		
	}

	function isWebStorageSupported() {
		return 'localStorage' in window;
	}

	function setCookie(cookieName,cookieValue,expirationDays) {
		var expirationDate = new Date();
		expirationDate.setDate(expirationDate.getDate() + expirationDays);
		cookieValue += '; expires=' + expirationDate.toUTCString();
		document.cookie = cookieName + '=' + cookieValue;
	}

	function getCookie(cookieName) {
		var cookies = document.cookie.split(';');

		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i];
			var index = cookie.indexOf('=');
			var key = cookie.substr(0, index);
			var val = cookie.substr(index + 1);

			if (key == cookieName) {
				return val;
			}
		}
	}
});