﻿/// <reference path="jquery-2.1.3.min.js" />

$(function () {
	init();

	function init() {
		setEvents();
	}

	function setEvents() {
		jQuery.event.props.push('dataTransfer');
		$('#target').on('dragenter', preventDefault);
		$('#target').on('dragover', preventDefault);
		$('#target').on('drop', dropItem);
	}

	function preventDefault(e) {
		e.preventDefault();
	}

	function dropItem(e) {
		var files = e.dataTransfer.files;
		var table = $('#fileInfo');

		table.html('<thead><tr><th>Name</th><th>Type</th><th>Size</th></tr></thead>');

		for (var i = 0; i < files.length; i++) {
			$('<tr><td>' +
			files[i].name + '</td><td>' +
			files[i].type + '</td><td>' +
			files[i].size + '</td></tr>').appendTo(table);
		}

		preventDefault(e);
	}
});