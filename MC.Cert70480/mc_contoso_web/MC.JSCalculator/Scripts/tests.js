﻿/// <reference path="_references.js" />

module('Calculator Test Suite', {
	setup: function () {
		calcApp.initialize();
	}
});

test('Initialize Test', function () {
	expect(2);
	var expected = '0';
	assertEqual($('#txtInput').val(), expected);
	assertEqual($('#txtResult').val(), expected);
});

test('Button Click Test', function () {
	var buttonQuantity = 10;
	expect(buttonQuantity * 2);

	for (var i = 0; i < buttonQuantity; i++) {
		$('#btnNumber' + i).triggerHandler('click');
		var result = $('#txtInput').val()[$('#txtInput').val().length - 1];
		var expected = String(i);
		assertEqual(result, expected);
		var expectedLength = i < 2 ? 1 : i;
		assertEqual($('#txtInput').val().length, expectedLength);
	}
});

test('Add Test', function () {
	expect(2);
	$('#txtInput').val('10');
	$('#txtResult').val('20');
	$('#btnPlus' + i).triggerHandler('click');
	var expected = '30';
	assertEqual($('#txtResult').val(), expected);
	expected = 0;
	assertEqual($('#txtInput').val(), expected);
});

test('Substract Test', function () {
	expect(2);
	$('#txtInput').val('5');
	$('#txtResult').val('20');
	$('#btnMinus' + i).triggerHandler('click');
	var expected = '15';
	assertEqual($('#txtResult').val(), expected);
	expected = '0';
	assertEqual($('#txtInput').val(), expected);
});

test('Multiplication Test', function () {
	expect(2);
	$('#txtInput').val('10');
	$('#txtResult').val('20');
	$('#btnMul' + i).triggerHandler('click');
	var expected = '200';
	assertEqual($('#txtResult').val(), expected);
	expected = '0';
	assertEqual($('#txtInput').val(), expected);
});

test('Division Test', function () {
	expect(2);
	$('#txtInput').val('10');
	$('#txtResult').val('4');
	$('#btnDiv' + i).triggerHandler('click');
	var expected = '0.4';
	assertEqual($('#txtResult').val(), expected);
	expected = '0';
	assertEqual($('#txtInput').val(), expected);
});

test('Clear Entry Test', function () {
	expect(1);
	$('#txtInput').val('10');
	$('#btnClearEntry' + i).triggerHandler('click');
	var expected = '0';
	assertEqual($('#txtInput').val(), expected);
});

test('Clear Test', function () {
	expect(2);
	$('#txtInput').val('10');
	$('#txtResult').val('20');
	$('#btnClear' + i).triggerHandler('click');
	var expected = '0';
	assertEqual($('#txtInput').val(), expected);
	assertEqual($('#txtResult').val(), expected);
});

function assertEqual(value, expected) {
	equal(value, expected, 'Expected value: ' + expected + ', actual value: ' + value);
}