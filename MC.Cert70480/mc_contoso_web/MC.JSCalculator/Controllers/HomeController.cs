﻿using System.Web.Mvc;

namespace MC.JSCalculator.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult CalculatorTest()
		{
			return View();
		}

		public ActionResult ContactUs()
		{
			return View();
		}
	}
}
