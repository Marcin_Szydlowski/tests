﻿using NUnit.Framework;
using System;
using System.Diagnostics;

namespace MC.NUnit.Tests
{
	public class WriteInfoAttribute : Attribute, ITestAction
	{
		public void BeforeTest(TestDetails testDetails)
		{
			Debug.WriteLine("Before " + testDetails.FullName);
		}

		public void AfterTest(TestDetails testDetails)
		{
			Debug.WriteLine("After " + testDetails.FullName);
		}

		public ActionTargets Targets
		{
			get { return ActionTargets.Default; }
		}
	}
}
