﻿using NUnit.Framework;
using System.Diagnostics;

[SetUpFixture]
public class Common
{
	[SetUp]
	public void Setup()
	{
		Debug.WriteLine("Before tests");
	}

	[TearDown]
	public void TearDown()
	{
		Debug.WriteLine("After tests");
	}

	//[TestFixtureSetUp]
	//public void TestSetup([CallerMemberName]string memberName = "")
	//{
	//	Debug.WriteLine(MethodBase.GetCurrentMethod().Name);
	//}
}
