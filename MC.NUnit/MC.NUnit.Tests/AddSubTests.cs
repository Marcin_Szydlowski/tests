﻿using NUnit.Framework;

namespace MC.NUnit.Tests
{
	[TestFixture]
	public class AddSubTests
	{
		[Test]
		[WriteInfo]
		[Category("Units")]
		[TestCase(1, 2)]
		[TestCase(1, -2)]
		[TestCase(-1, -8)]
		public void Add_Test(int a, int b)
		{
			// arrange
			var expected = a + b;

			// act
			var result = Calculations.Add(a, b);

			// assert
			Assert.AreEqual(expected, result);
		}

		[Test]
		[WriteInfo]
		[Category("Units")]
		[TestCase(5, 4)]
		[TestCase(-7, 4)]
		[TestCase(5, -4)]
		public void Sub_Test(int a, int b)
		{
			// arrange
			var expected = a - b;

			// act
			var result = Calculations.Sub(a, b);

			// assert
			Assert.AreEqual(expected, result);
		}
	}
}
