﻿using MC.NUnit;
using MC.NUnit.Tests;
using NUnit.Framework;

namespace OtherNamespace
{
	[TestFixture]
	public class MulDivTests
	{
		[Test]
		[WriteInfo]
		[Category("Units")]
		[TestCase(1, 2)]
		[TestCase(1, -2)]
		[TestCase(1, -8)]
		public void Mul_Test(int a, int b)
		{
			// arrange
			var expected = a * b;

			// act
			var result = Calculations.Mul(a, b);

			// assert
			Assert.AreEqual(expected, result);
		}

		[Test]
		[WriteInfo]
		[Category("Units")]
		[TestCase(5, 4)]
		[TestCase(-7, 4)]
		[TestCase(5, -4)]
		public void Div_Test(double a, double b)
		{
			// arrange
			var expected = a / b;

			// act
			var result = Calculations.Div(a, b);

			// assert
			Assert.AreEqual(expected, result);
		}
	}
}
