﻿using System.Collections.Generic;

namespace MC.Infrastructure
{
	public interface IFactory
	{
		string AssortmentName { get; }
		bool CanProduce { get; }
		uint MagazineSize { get; }
		uint AvailablePlace { get; }
		List<ICargo> WaitingCargo { get; }
		List<ITruck> AvailableTrucks { get; }
		decimal EarnedMoney { get; }
		ICargo Produce(string productDescription, decimal productPrice, uint productsCount);
		ITruck GetFirstAvailableTruck();
		void SellGoods(ICargo cargo, ITruck truck);
	}
}