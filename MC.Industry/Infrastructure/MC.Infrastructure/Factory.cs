﻿using MC.CommonUtils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MC.Infrastructure
{
	public class Factory : IFactory, IEquatable<IFactory>
	{
		private readonly List<ITruck> availableTrucks = new List<ITruck>
		{
			new Truck("MAN", 30),
			new Truck("Peugeot", 32),
			new Truck("DAF", 28)
		};

		public string AssortmentName { get; }

		public bool CanProduce
		{
			get
			{
				return AvailablePlace > 0;
			}
		}

		public uint MagazineSize { get; }

		public uint AvailablePlace
		{
			get
			{
				return (uint)(MagazineSize - WaitingCargo.Sum(c => c.ProductsCount));
			}
		}

		public List<ICargo> WaitingCargo { get; } = new List<ICargo>();

		public List<ITruck> AvailableTrucks
		{
			get
			{
				return availableTrucks.Where(t => t.IsAvailable).ToList();
			}
		}

		public decimal EarnedMoney { get; }

		public Factory(string assortmentName, uint magazineSize)
		{
			assortmentName.EnsureNotNullOrEmpty(nameof(assortmentName));

			AssortmentName = assortmentName;
			MagazineSize = magazineSize;
		}

		public ICargo Produce(string productDescription, decimal productPrice, uint productsCount)
		{
			productDescription.EnsureNotNullOrEmpty(nameof(productDescription));
			productPrice.EnsurePositive(nameof(productPrice));

			if (productsCount <= AvailablePlace)
			{
				ICargo cargo = new Cargo(productDescription, productPrice, productsCount);
				WaitingCargo.Add(cargo);
				return cargo;
			}

			return null;
		}

		public ITruck GetFirstAvailableTruck()
		{
			return AvailableTrucks.FirstOrDefault();
		}

		public void SellGoods(ICargo cargo, ITruck truck)
		{
			truck.Carry(cargo);
			WaitingCargo.Remove(cargo);
		}

		public bool Equals(IFactory other)
		{
			if (other != null)
			{
				return AssortmentName.Equals(other.AssortmentName);
			}

			return false;
		}

		public override bool Equals(object obj)
		{
			IFactory factory = obj as IFactory;

			if (factory != null)
			{
				return Equals(factory);
			}

			return false;
		}

		public override int GetHashCode()
		{
			return AssortmentName.GetHashCode();
		}
	}
}