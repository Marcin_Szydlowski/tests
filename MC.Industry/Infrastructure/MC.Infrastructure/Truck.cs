﻿using MC.CommonUtils.Extensions;
using System;

namespace MC.Infrastructure
{
	public class Truck : ITruck, IEquatable<ITruck>
	{
		public string Manufacturer { get; }
		public byte MaxLoad { get; }

		public bool IsAvailable
		{
			get
			{
				return CarriedCargo == null;
			}
		}

		public ICargo CarriedCargo { get; private set; }

		public IFactory Owner { get; }

		public Truck(string manufacturer, byte maxLoad)
		{
			manufacturer.EnsureNotNullOrEmpty(nameof(manufacturer));

			Manufacturer = manufacturer;
			MaxLoad = maxLoad;
		}

		public void Carry(ICargo cargo)
		{
			if (CarriedCargo != null)
			{
				throw new InvalidOperationException("Another cargo is actually transported");
			}

			cargo.EnsureNotNull(nameof(cargo));

			CarriedCargo = cargo;
		}

		public void Unload()
		{
			CarriedCargo = null;
		}

		public bool Equals(ITruck other)
		{
			if (other != null)
			{
				return Manufacturer.Equals(other.Manufacturer) &&
					MaxLoad.Equals(other.MaxLoad);
			}

			return false;
		}

		public override bool Equals(object obj)
		{
			ITruck truck = obj as ITruck;

			if (truck != null)
			{
				return Equals(truck);
			}

			return false;
		}

		public override int GetHashCode()
		{
			return Manufacturer.GetHashCode() ^
				MaxLoad.GetHashCode();
		}
	}
}