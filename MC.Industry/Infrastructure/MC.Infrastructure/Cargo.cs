﻿using MC.CommonUtils.Extensions;
using System;

namespace MC.Infrastructure
{
	public class Cargo : ICargo, IEquatable<ICargo>
	{
		public string ProductDescription { get; }
		public decimal ProductPrice { get; }
		public uint ProductsCount { get; }

		public decimal TotalValue
		{
			get
			{
				return ProductPrice * ProductsCount;
			}
		}

		public Cargo(string productDescription, decimal productPrice, uint productsCount)
		{
			productDescription.EnsureNotNullOrEmpty(nameof(productDescription));
			productPrice.EnsurePositive(nameof(productPrice));

			ProductDescription = productDescription;
			ProductPrice = productPrice;
			ProductsCount = productsCount;
		}

		public bool Equals(ICargo other)
		{
			if (other != null)
			{
				return ProductDescription.Equals(other.ProductDescription) &&
					ProductPrice.Equals(other.ProductPrice) &&
					ProductsCount.Equals(other.ProductsCount);
			}

			return false;
		}

		public override bool Equals(object obj)
		{
			ICargo cargo = obj as ICargo;

			if (cargo != null)
			{
				return Equals(cargo);
			}

			return false;
		}

		public override int GetHashCode()
		{
			return ProductDescription.GetHashCode() ^
				ProductPrice.GetHashCode() ^
				ProductsCount.GetHashCode();
		}
	}
}