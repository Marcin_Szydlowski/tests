﻿namespace MC.Infrastructure
{
	public interface ITruck
	{
		string Manufacturer { get; }
		byte MaxLoad { get; }
		bool IsAvailable { get; }
		ICargo CarriedCargo { get; }
		void Carry(ICargo cargo);
		void Unload();
	}
}