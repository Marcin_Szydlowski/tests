﻿namespace MC.Infrastructure
{
	public interface ICargo
	{
		string ProductDescription { get; }
		decimal ProductPrice { get; }
		uint ProductsCount { get; }
		decimal TotalValue { get; }
	}
}