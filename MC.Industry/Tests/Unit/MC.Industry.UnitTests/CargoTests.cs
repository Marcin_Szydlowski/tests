﻿using MC.Infrastructure;
using NUnit.Framework;
using System;

namespace MC.Industry.Tests
{
	[TestFixture]
	public class CargoTests
	{
		private static readonly object[] InvalidValues =
		{
			new object[]{ null, 10.99m, (uint)5 },
			new object[] { "", 13.99m, (uint)5 },
			new object[] { "Regal Armchair", decimal.MinusOne, (uint)5 }
		};

		private static readonly object[] OtherCargos =
		{
			new object[] { null },
			new object[] { new Cargo("Stool", 10.99m, 10) },
			new object[] { new Cargo("Stool", 8.99m, 15) },
			new object[] { new Cargo("Armchair", 8.99m, 10) }
		};

		[Test]
		[Category("Units Cargo success")]
		public void Cargo_ShouldInitializeWithDefaultValues_Success()
		{
			// Arrange
			ICargo cargo = new Cargo("Chair", 34.99m, 10);

			// Act
			string productDescription = cargo.ProductDescription;
			decimal productPrice = cargo.ProductPrice;
			uint productsCount = cargo.ProductsCount;
			decimal totalValue = cargo.TotalValue;

			// Assert
			Assert.AreEqual("Chair", productDescription, "Names aren't equal");
			Assert.AreEqual(34.99m, productPrice, "Prices aren't equal");
			Assert.AreEqual(10, productsCount, "Products count aren't equal");
			Assert.AreEqual(349.9m, totalValue, "Total values aren't equal");
		}

		[Test]
		[Category("Units Cargo fail")]
		[TestCaseSource("InvalidValues")]
		public void Cargo_ShouldThrowsForInvalidValues_Fail(string productName, decimal pricePerUnit, uint productsCount)
		{
			// Arrange

			// Act
			try
			{
				ICargo cargo = new Cargo(productName, pricePerUnit, productsCount);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Units Cargo success")]
		public void Cargo_ShouldBeEqual_Success()
		{
			// Arrange
			ICargo cargo1 = new Cargo("Chair", 10.89m, 20);
			ICargo cargo2 = new Cargo("Chair", 10.89m, 20);

			// Act
			bool areEqual = cargo1.Equals(cargo2);

			// Assert
			Assert.True(areEqual);
		}

		[Test]
		[Category("Units Cargo success")]
		[TestCaseSource("OtherCargos")]
		public void Cargo_ShouldNotBeEqual_Success(ICargo otherCargo)
		{
			// Arrange
			ICargo cargo = new Cargo("Stool", 8.99m, 10);

			// Act
			bool areEqual = cargo.Equals(otherCargo);

			// Assert
			Assert.False(areEqual);
		}

		[Test]
		[Category("Units Cargo success")]
		public void Factory_ShouldGetHashCode_Success()
		{
			// Arrange
			ICargo cargo = new Cargo("Stool", 8.99m, 10);
			int hashCode = "Stool".GetHashCode() ^ 8.99m.GetHashCode() ^ ((uint)10).GetHashCode();

			// Act
			int result = cargo.GetHashCode();

			// Assert
			Assert.AreEqual(hashCode, result);
		}
	}
}