﻿using MC.Infrastructure;
using NUnit.Framework;
using System;

namespace MC.Industry.UnitTests
{
	[TestFixture]
	public class FactoryTests
	{
		private static readonly object[] InvalidFactories =
		{
			new object[] { null },
			new object[] { new Factory("Cars", 5000) }
		};

		[Test]
		[Category("Units Factory success")]
		public void Factory_ShouldInitializeWithDefaultValues_Success()
		{
			// Arrange
			IFactory factory = new Factory("Furniture", 10000);

			// Act
			string assortmentName = factory.AssortmentName;
			bool canProduce = factory.CanProduce;
			uint magazineSize = factory.MagazineSize;
			uint availablePlace = factory.AvailablePlace;
			int availableTrucksCount = factory.AvailableTrucks.Count;
			decimal earnedMoney = factory.EarnedMoney;

			// Assert
			Assert.AreEqual("Furniture", assortmentName, "Assortments' names aren't equal");
			Assert.True(canProduce, "Factory cannot produce");
			Assert.AreEqual(10000, magazineSize, "Magazines' sizes aren't equal");
			Assert.AreEqual(10000, availablePlace, "Available places values arent' equal");
			Assert.AreEqual(3, availableTrucksCount, "Available trucks' count is different");
			Assert.AreEqual(decimal.Zero, earnedMoney, "Factory shouldn't have any money");
		}

		[Test]
		[Category("Units Factory fail")]
		[TestCase(null)]
		[TestCase("")]
		public void Factory_ShouldThrowsForInvalidValues_Fail(string assortment)
		{
			// Arrange

			// Act
			try
			{
				IFactory factory = new Factory(assortment, 10000);

			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Units Factory success")]
		public void Factory_ShouldBeEqual_Success()
		{
			// Arrange
			IFactory factory1 = new Factory("Furniture", 2000);
			IFactory factory2 = new Factory("Furniture", 2000);

			// Act
			bool areEqual = factory1.Equals(factory2);

			// Assert
			Assert.True(areEqual);
		}

		[Test]
		[Category("Units Factory success")]
		[TestCaseSource("InvalidFactories")]
		public void Factory_ShouldNotBeEqual_Success(IFactory otherFactory)
		{
			// Arrange
			IFactory factory = new Factory("Furniture", 2000);

			// Act
			bool areEqual = factory.Equals(otherFactory);

			// Assert
			Assert.False(areEqual);
		}

		[Test]
		[Category("Units Factory success")]
		public void Factory_ShouldGetHashCode_Success()
		{
			// Arrange
			IFactory factory = new Factory("Furniture", 2000);
			int hashCode = "Furniture".GetHashCode();

			// Act
			int result = factory.GetHashCode();

			// Assert
			Assert.AreEqual(hashCode, result);
		}
	}
}