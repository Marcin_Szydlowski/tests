﻿using MC.Infrastructure;
using NUnit.Framework;
using System;

namespace MC.Industry.UnitTests
{
	[TestFixture]
	public class TruckTests
	{
		private static readonly object[] InvalidTrucks =
		{
			new object[] { null },
			new object[] { new Truck("Mack", 42) }
		};

		[Test]
		[Category("Units Truck success")]
		public void Truck_ShouldInitializeWithDefaultValues_Success()
		{
			// Arrange
			ITruck truck = new Truck("MAN", 30);

			// Act
			string manufacturerName = truck.Manufacturer;
			byte maxLoad = truck.MaxLoad;
			bool isAvailable = truck.IsAvailable;

			// Assert
			Assert.AreEqual("MAN", manufacturerName, "Manufacturers aren't equal");
			Assert.AreEqual(30, maxLoad, "Max load does not match");
			Assert.True(isAvailable, "Truck is not available");
		}

		[Test]
		[Category("Units Truck fail")]
		[TestCase(null)]
		[TestCase("")]
		public void Truck_ShouldThrowsForInvalidValues_Fail(string manufacturer)
		{
			// Arrange

			// Act
			try
			{
				ITruck truck = new Truck(manufacturer, 30);

			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Units Truck success")]
		public void Truck_ShouldBeEqual_Success()
		{
			// Arrange
			ITruck truck1 = new Truck("Peugeot", 25);
			ITruck truck2 = new Truck("Peugeot", 25);

			// Act
			bool areEqual = truck1.Equals(truck2);

			// Assert
			Assert.True(areEqual);
		}

		[Test]
		[Category("Units Truck success")]
		[TestCaseSource("InvalidTrucks")]
		public void Truck_ShouldNotBeEqual_Success(ITruck otherTruck)
		{
			// Arrange
			ITruck truck = new Truck("Peugeot", 25);

			// Act
			bool areEqual = truck.Equals(otherTruck);

			// Assert
			Assert.False(areEqual);
		}

		[Test]
		[Category("Units Truck success")]
		public void Truck_ShouldGetHashCode_Success()
		{
			// Arrange
			ITruck truck = new Truck("Peugeot", 25);
			int hashCode = "Peugeot".GetHashCode() ^ ((byte)25).GetHashCode();

			// Act
			int result = truck.GetHashCode();

			// Assert
			Assert.AreEqual(hashCode, result);
		}
	}
}