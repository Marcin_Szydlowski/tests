﻿using MC.Infrastructure;
using NUnit.Framework;
using System;

namespace MC.Industry.FunctionalTests
{
	[TestFixture]
	public class TruckTests
	{
		[Test]
		[Category("Functional Truck success")]
		public void Truck_ShouldNotHaveCargo_Success()
		{
			// Arrange
			ITruck truck = new Truck("Star", 15);

			// Act
			ICargo cargo = truck.CarriedCargo;

			// Assert
			Assert.IsNull(cargo);
		}

		[Test]
		[Category("Functional Truck success")]
		public void Truck_ShouldCarryCargo_Success()
		{
			// Arrange
			ITruck truck = new Truck("Star", 15);
			ICargo cargo = new Cargo("Steel beams", 600m, 10);

			// Act
			truck.Carry(cargo);
			bool isAvailable = truck.IsAvailable;
			ICargo carriedCargo = truck.CarriedCargo;

			// Assert
			Assert.False(isAvailable, "Truck is available with cargo");
			Assert.NotNull(carriedCargo, "Truck has not cargo");
		}

		[Test]
		[Category("Functional Truck fail")]
		public void Truck_ShouldThrowsWhenCargoIsNull_Fail()
		{
			// Arrange
			ITruck truck = new Truck("Star", 15);

			// Act
			try
			{
				truck.Carry(null);
			}
			catch (ArgumentNullException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Functional Truck fail")]
		public void Truck_ShouldThrowsWhenAnotherCargoIsCarriedOn_Fail()
		{
			// Arrange
			ITruck truck = new Truck("Star", 15);
			ICargo cargo = new Cargo("Steel beams", 600m, 10);
			ICargo anotherCargo = new Cargo("Nails", 0.02m, 50000);

			// Act
			truck.Carry(cargo);

			try
			{
				truck.Carry(anotherCargo);
			}
			catch (InvalidOperationException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Functional Truck success")]
		public void Truck_ShouldUnloadCargo_Success()
		{
			// Arrange
			ITruck truck = new Truck("Star", 15);
			ICargo cargo = new Cargo("Steel beams", 600m, 10);

			// Act
			truck.Carry(cargo);
			truck.Unload();
			bool isAvailable = truck.IsAvailable;
			ICargo carriedCargo = truck.CarriedCargo;

			// Assert
			Assert.True(isAvailable, "Truck isn't available after cargo unload");
			Assert.IsNull(carriedCargo, "Truck has cargo after unload");
		}

		// TODO: MSZ: mock
	}
}