﻿using MC.Infrastructure;
using NUnit.Framework;
using System;
using System.Linq;

namespace MC.Industry.FunctionalTests
{
	[TestFixture]
	public class FactoryTests
	{
		private static readonly object[] InvalidValues =
	{
			new object[] { null, 10.99m, (uint)5 },
			new object[] { "", 13.99m, (uint)5 },
			new object[] { "Steel hammer 2kg", decimal.MinusOne, (uint)5 }
		};

		[Test]
		[Category("Functional Factory success")]
		[TestCase((uint)1000)]
		[TestCase((uint)9999)]
		public void Factory_ShouldProduce_Success(uint productsCount)
		{
			// Arrange
			IFactory factory = new Factory("Clothes", 10000);

			// Act
			ICargo cargo = factory.Produce("Trousers", 4.79m, productsCount);
			string productDescription = cargo.ProductDescription;
			decimal productPrice = cargo.ProductPrice;
			bool canProduce = factory.CanProduce;
			int waitingCargoCount = factory.WaitingCargo.Count;

			// Assert
			Assert.AreEqual("Trousers", productDescription, "Products' descriptions aren't equal");
			Assert.AreEqual(4.79m, productPrice, "Products' prices aren't equal");
			Assert.True(canProduce, "Factory cannot produce");
			Assert.AreEqual(1, waitingCargoCount, "Waiting cargo's counts aren't equal");
		}

		[Test]
		[Category("Functional Factory success")]
		public void Factory_ShouldProduceOnlyOnce_Success()
		{
			// Arrange
			IFactory factory = new Factory("Clothes", 10000);

			// Act
			ICargo cargo = factory.Produce("Trousers", 4.79m, 10000);
			bool canProduce = factory.CanProduce;

			// Assert
			Assert.False(canProduce, "Factory cannot produce");
		}

		[Test]
		[Category("Functional Factory fail")]
		[TestCaseSource("InvalidValues")]
		public void Factory_ShouldNotProduceInvalidParameters_Fail(string productDescription, decimal productPrice, uint productsCount)
		{
			// Arrange
			IFactory factory = new Factory("Tools", 10000);

			// Act
			try
			{
				ICargo cargo = factory.Produce(productDescription, productPrice, productsCount);
			}
			catch (ArgumentException)
			{
				Assert.Pass();
			}

			// Assert
			Assert.Fail();
		}

		[Test]
		[Category("Functional Factory fail")]
		public void Factory_ShouldNotProduceMagazineTooSmall_Fail()
		{
			// Arrange
			IFactory factory = new Factory("Clothes", 10000);

			// Act
			ICargo cargo = factory.Produce("Coats", 15.59m, 10001);

			// Assert
			Assert.IsNull(cargo);
		}

		[Test]
		[Category("Functional Factory success")]
		public void Factory_ShouldGetFirstAvailableTruck_Success()
		{
			// Arrange
			IFactory factory = new Factory("Clothes", 10000);

			// Act
			ITruck firstAvailableTruck = factory.GetFirstAvailableTruck();

			// Assert
			Assert.IsNotNull(firstAvailableTruck);
		}

		[Test]
		[Category("Functional Factory success")]
		public void Factory_ShouldNotGetMoreAvailableTrucks_Success()
		{
			// Arrange
			IFactory factory = new Factory("Clothes", 10000);
			ITruck firstAvailableTruck = factory.GetFirstAvailableTruck();
			ITruck anotherAvailableTruck = factory.GetFirstAvailableTruck();
			ITruck lastAvailableTruck = factory.GetFirstAvailableTruck();

			// Act
			ITruck truck = factory.GetFirstAvailableTruck();
			int availableTrucksCount = factory.AvailableTrucks.Count;

			// Assert
			Assert.IsNull(truck, "Factory should have only three trucks");
			Assert.AreEqual(0, availableTrucksCount, "Available trucks' count is greater than zero");
		}

		[Test]
		[Category("Functional Factory success")]
		public void Factory_ShouldSellGoods_Success()
		{
			// Arrange
			IFactory factory = new Factory("Tools", 10000);
			factory.Produce("Saw", 34m, 400);
			factory.Produce("Hammer", 2m, 750);
			ITruck truck = factory.GetFirstAvailableTruck();

			// Act
			factory.SellGoods(factory.WaitingCargo.First(c => c.ProductDescription == "Hammer"), truck);
			truck.Unload();
			decimal earnedMoney = factory.EarnedMoney;
			int waitingCargoCount = factory.WaitingCargo.Count;

			// Assert
			Assert.AreEqual(1500m, earnedMoney, "Factory earned different amount of money");
			Assert.AreEqual(1, waitingCargoCount, "Factory still has some cargo");
		}

		// TODO: MSZ: sell -> invalid values
		// TODO: MSZ: sell -> valid values
		// TODO: MSZ: sell -> 
		// TODO: MSZ: sell -> ok
		// TODO: MSZ: sell -> ok
		// TODO: MSZ: sell -> ok
		// TODO: MSZ: sell -> ok
		// TODO: MSZ: sell -> ok

		// TODO: MSZ: sell -> ok

	}
}