﻿using System;

namespace MC.ChangeSetterFunctionalityAttribute
{
	class Program
	{
		static void Main(string[] args)
		{
			var code = "AADS 809 sd AAA";
			var someObject = new SomeObject { NormalizedText = code, RealText = code };
			Console.WriteLine(someObject.NormalizedText + ", " + someObject.RealText);

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}
