﻿using System;

namespace MC.ChangeSetterFunctionalityAttribute
{
	internal abstract class BaseObject
	{
		private string someText = default(string);

		public string NormalizedText
		{
			get { return someText; }
			set
			{
				someText = value.ToLower().Replace(" ", String.Empty);
			}
		}
	}
}
