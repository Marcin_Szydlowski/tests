﻿using System;
using System.Diagnostics;

namespace MC.Instrumentation
{
	class Program
	{
		static void Main(string[] args)
		{
			var source = "MC.Instrumentation";
			var log = "Application";
			var message = "Application started";


			if (!EventLog.SourceExists(source))
			{
				EventLog.CreateEventSource(source, "log");
			}

			EventLog.WriteEntry(source, message, EventLogEntryType.Information, 8964);
			Console.WriteLine("An event had been logged");

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}
