﻿using System;

namespace MC.RegexExamples
{
	class Program
	{
		static void Main(string[] args)
		{
			var text = "An example to show how to use regular expressions in .NET v. 4.5.1 to getting results quickly";
			Console.WriteLine("Sample text: {0}", text);

			text.WriteMatches(@"\w{2,}\b", "All words:");
			text.WriteMatches(@"\d+\b", "All numbers:");
			text.WriteMatches(@"[\a\s\.]\w{3}\b", "All three-letter words");
#warning Check this case
			text.WriteMatches(@"(\w{3}).+\1", "All three-letter patterns");

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}
