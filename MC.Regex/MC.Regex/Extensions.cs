﻿using System;
using System.Text.RegularExpressions;

namespace MC.RegexExamples
{
	public static class Extensions
	{
		public static void WriteMatches(this string text, string pattern, string description, RegexOptions options = RegexOptions.IgnoreCase)
		{
			Console.WriteLine(description);
			var matches = Regex.Matches(text, pattern, options);
			foreach (var match in matches)
			{
				Console.WriteLine(match);
			}
		}
	}
}
